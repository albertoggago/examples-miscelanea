# Java Spring template project

Part to test any idea in java

## Process of List

Each class is related with an exercise:
+ ListDiff Instance of elements  
+ QueueList. List object saved next element. Not ordered
+ OrderList. List ordered using QueueList Ordered.
+ TreeList. Based in Right and Left branch. 
+ BalancedTreeList. based in TreeList including rules balanced the branches.
+ Coding Dojo https://codingdojo.org/kata/ Bank OCR
+ Bowling https://codingdojo.org/kata/Bowling/
+ Santa Delivering https://code.joejag.com/coding-dojo/christmas-delivery/
+ Santa Delivering II https://codingdojo.org/kata/christmas-delivery/
+ Java Coding https://www.cloudtechtwitter.com/p/java.html

## Interesting Information

### Use of List and Maps in Java

https://stackoverflow.com/questions/48442/rule-of-thumb-for-choosing-an-implementation-of-a-java-collection

Images key important to decide which are more important:
![Image1.png](images/GfpyN.png)
![Image2.png](images/0g5VZ.png)
