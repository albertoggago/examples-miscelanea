package com.example.demo.magicbox;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.List;

import static com.example.demo.magicbox.MagicBoxBuilder.*;
import static org.junit.jupiter.api.Assertions.*;

class MagicBoxTest {

    @Test
    void equalsCloneTest(){
        MagicBox magicBox = createFirstBox(3);
        MagicBox magicBoxNew = MagicBoxBuilder.clone(magicBox);

        assertEquals(magicBox, magicBox);
        MagicBox magicBoxNull = null;
        assertNotEquals(magicBox, magicBoxNull);
        assertNotEquals(magicBox, new Object());
        assertEquals(magicBoxNew, magicBox);
        assertEquals(magicBoxNew.hashCode(), magicBox.hashCode());
        assertEquals(0,magicBoxNew.compareTo(null));

        try {
            Field privateSizeField = MagicBox.class.getDeclaredField("size");
            privateSizeField.setAccessible(true);
            privateSizeField.setInt(magicBoxNew,2);

            assertNotEquals(magicBoxNew, magicBox);

            privateSizeField.setInt(magicBoxNew,3);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            fail();
        }

        magicBoxNew.setMatrix(new int[]{4,3,2,1});
        assertNotEquals(magicBoxNew, magicBox);

        magicBoxNew.setMatrix(null);
        assertNotEquals(magicBoxNew, magicBox);
    }

    @Test
    void createErrorMagicBox(){
        assertThrows(IllegalStateException.class, ()-> createFirstBox(-1) );
    }

    @Test
    void createMagicBoxOneSize() {
        MagicBox magicBox = createFirstBox(1);

        assertEquals("[[1]]", magicBox.toString());
        assertTrue(isMagicBox(magicBox));
        assertThrows(IndexOutOfBoundsException.class, () -> nextBox(magicBox));
    }

    @Test
    void createMagicBoxTwoSizePhaseOne() {
        MagicBox magicBox = createFirstBox(2);

        assertEquals("[[1,2],[3,4]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[1,3],[2,4]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[1,4],[2,3]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[1,4],[3,2]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));
    }

    @Test
    void createMagicBoxTwoSizePhaseTwo() {
        MagicBox magicBox = createFirstBox(2);
        magicBox.setMatrix(new int[]{1, 4, 3, 2});

        nextBox(magicBox);

        assertEquals("[[2,1],[3,4]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[2,3],[1,4]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[2,3],[4,1]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[2,4],[1,3]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[2,4],[3,1]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));
    }

    @Test
    void createMagicBoxTwoSizePhaseThree() {
        MagicBox magicBox = createFirstBox(2);
        magicBox.setMatrix(new int[]{2, 4, 3, 1});

        nextBox(magicBox);

        assertEquals("[[3,1],[2,4]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[3,2],[1,4]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[3,2],[4,1]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));


        nextBox(magicBox);

        assertEquals("[[3,4],[1,2]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[3,4],[2,1]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);
    }

    @Test
    void createMagicBoxTwoSizePhaseFour() {
        MagicBox magicBox = createFirstBox(2);
        magicBox.setMatrix(new int[]{3,4,2,1});

        nextBox(magicBox);

        assertEquals("[[4,1],[2,3]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[4,1],[3,2]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[4,2],[1,3]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));

        nextBox(magicBox);

        assertEquals("[[4,3],[1,2]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertTrue(hasNext(magicBox));
        nextBox(magicBox);

        assertEquals("[[4,3],[2,1]]", magicBox.toString());
        assertFalse(isMagicBox(magicBox));
        assertFalse(hasNext(magicBox));
        assertThrows(IndexOutOfBoundsException.class, () -> nextBox(magicBox));
    }

    @Test
    void testNoMagicBoxIn2(){
        List<MagicBox> result = printAllMagicBoxed(2);

        assertEquals(0, result.size());
    }

    @Test
    void testNoMagicBox3OneOfThem(){
        MagicBox result = createFirstBox(3);
        result.setMatrix(new int[]{8,3,4,1,5,9,6,7,2});

        assertFalse(isMagicBox(result));
    }
    @Test
    void testNoMagicBoxIn3(){
        List<MagicBox> result = printAllMagicBoxed(3, Integer.MAX_VALUE, true);

        assertEquals(8, result.size());
        assertTrue(isOriginal(result.get(0)));
        assertFalse(isOriginal(result.get(1)));
        assertFalse(isOriginal(result.get(2)));
        assertFalse(isOriginal(result.get(3)));
        assertTrue(isOriginal(result.get(4)));
        assertFalse(isOriginal(result.get(5)));
        assertFalse(isOriginal(result.get(6)));
        assertFalse(isOriginal(result.get(7)));

        MagicBox magicBox1 = createFirstBox(3);
        magicBox1.setMatrix(new int[]{2,7,6,9,5,1,4,3,8});
        MagicBox magicBox2 = createFirstBox(3);
        magicBox2.setMatrix(new int[]{2,9,4,7,5,3,6,1,8});

        assertEquals(1, magicBox2.compareTo(magicBox1));

        assertEquals(magicBox1, result.get(0));
        assertEquals(magicBox2, result.get(4));
    }

    @Test
    void testNoMagicBoxIn3Limited(){
        List<MagicBox> result = printAllMagicBoxed(3, 8, true);

        assertEquals(8, result.size());

        MagicBox magicBox1 = createFirstBox(3);
        magicBox1.setMatrix(new int[]{2,7,6,9,5,1,4,3,8});
        MagicBox magicBox2 = createFirstBox(3);
        magicBox2.setMatrix(new int[]{2,9,4,7,5,3,6,1,8});

        assertEquals(magicBox1, result.get(0));
        assertEquals(magicBox2, result.get(4));
    }

    @Test
    void testNoMagicBoxIn3LimitedWithOutClones(){
        List<MagicBox> result = printAllMagicBoxed(3, 10, false);

        assertEquals(2, result.size());

        MagicBox magicBox1 = createFirstBox(3);
        magicBox1.setMatrix(new int[]{2,7,6,9,5,1,4,3,8});
        MagicBox magicBox2 = createFirstBox(3);
        magicBox2.setMatrix(new int[]{2,9,4,7,5,3,6,1,8});

        assertEquals(magicBox1, result.get(0));
        assertEquals(magicBox2, result.get(1));
    }

    @Test
    void testNoMagicBoxCheck294753618(){
        MagicBox result = createFirstBox(3);
        result.setMatrix(new int[]{2,9,4,7,1,3,5,6,8});

        nextBox(result);


        MagicBox magicBox1 = createFirstBox(3);
        magicBox1.setMatrix(new int[]{2,9,4,7,1,5,3,6,8});

        assertEquals(magicBox1, result);
    }
}
