package com.example.demo.javacoding;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class StringExamplesTest {

    @Test
    void reverseStringVoid() {
        assertNull(StringExamples.reverseString(null));
    }
    @Test
    void reverseStringAnyString() {
        assertEquals("4321 edcba",StringExamples.reverseString("abcde 1234"));
    }

    @Test
    void isAnagramNull() {
        assertFalse(StringExamples.isAnagram(null, null));
        assertFalse(StringExamples.isAnagram("a", null));
        assertFalse(StringExamples.isAnagram(null, "b"));
    }

    @Test
    void isAnagramTrue() {
        assertTrue(StringExamples.isAnagram("home is home", "is meho home"));
    }

    @Test
    void isAnagramFalse() {
        assertFalse(StringExamples.isAnagram("home ", "meho"));
        assertFalse(StringExamples.isAnagram("home", "meho "));
        assertFalse(StringExamples.isAnagram("homex", "meho"));
        assertFalse(StringExamples.isAnagram("home", "mehx"));
    }

    @Test
    void allIsUniqueNull() {
        assertFalse(StringExamples.allIsUnique(null));
    }

    @Test
    void allIsUniqueFalse() {
        assertFalse(StringExamples.allIsUnique("apple"));
        assertFalse(StringExamples.allIsUnique("abc  x"));
        assertFalse(StringExamples.allIsUnique("papi"));
        assertFalse(StringExamples.allIsUnique("caseron caseron"));
    }

    @Test
    void allIsUniqueTrue() {
        assertTrue(StringExamples.allIsUnique("home "));
        assertTrue(StringExamples.allIsUnique("patibulo"));
    }

    @Test
    void hasDuplicatedNull(){
        assertEquals(0, StringExamples.hasDuplicated(null).size());
    }

    @Test
    void hasDuplicatedNo(){
        assertEquals(0, StringExamples.hasDuplicated("abcdefghi").size());
        assertEquals(0, StringExamples.hasDuplicated("").size());
    }


    @Test
    void hasDuplicatedYes(){
        Map<String, Integer> duplicated = StringExamples.hasDuplicated("the house is big and red");
        assertEquals(6, duplicated.size());
        assertEquals(5, duplicated.get(" "));
        assertEquals(2, duplicated.get("s"));
        assertEquals(2, duplicated.get("d"));
        assertEquals(3, duplicated.get("e"));
        assertEquals(2, duplicated.get("h"));
        assertEquals(2, duplicated.get("i"));
    }

    @Test
    void hasDuplicated3TimesYes(){
        Map<String, Integer> duplicated = StringExamples.hasDuplicated("aaa");
        assertEquals(1, duplicated.size());
        assertEquals(3, duplicated.get("a"));
    }


    @Test
    void findFirstNoDuplicatedNull(){
        assertNull(StringExamples.findFirstNoDuplicated(null));
    }


    @Test
    void findFirstNoDuplicated(){
        assertEquals("n", StringExamples.findFirstNoDuplicated("null"));
        assertEquals("b", StringExamples.findFirstNoDuplicated("aab"));
        assertNull( StringExamples.findFirstNoDuplicated("aabb"));
    }

    @Test
    void allSubStringsNull() {
        assertArrayEquals(new String[]{}, StringExamples.allSubstrings(null).toArray());
    }
    @Test
    void allSubStrings4Elements() {
        assertArrayEquals(new String[]{}, StringExamples.allSubstrings("a").toArray());
        assertArrayEquals(new String[]{"a", "b"}, StringExamples.allSubstrings("ab").toArray());
        assertArrayEquals(new String[]{"a", "b", "c", "ab", "bc"}, StringExamples.allSubstrings("abc").toArray());
        assertArrayEquals(new String[]{"a", "b", "c", "d", "ab", "bc", "cd", "abc", "bcd"}, StringExamples.allSubstrings("abcd").toArray());
    }

    @Test
    void getLengthTest(){
        assertEquals(0, StringExamples.getLength(null));
        assertEquals(4, StringExamples.getLength("null"));
        assertEquals(0, StringExamples.getLength(""));
        assertEquals(16, StringExamples.getLength("abcdefghijkl sss"));
    }

    @Test
    void getPermutationsTest() {
        assertArrayEquals(new String[]{}, StringExamples.getPermutations(null).toArray());
        assertArrayEquals(new String[]{}, StringExamples.getPermutations("").toArray());
        assertArrayEquals(new String[]{"a"}, StringExamples.getPermutations("a").toArray());
        assertArrayEquals(new String[]{"ab","ba"}, StringExamples.getPermutations("ab").toArray());
        assertArrayEquals(new String[]{"abc","acb","bac","bca","cab","cba"}, StringExamples.getPermutations("abc").toArray());
    }
}
