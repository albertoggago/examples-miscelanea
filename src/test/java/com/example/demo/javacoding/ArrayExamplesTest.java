package com.example.demo.javacoding;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ArrayExamplesTest {

    @Test
    void finMaxAndMin() {
        List<Integer> result = ArrayExamples.finMaxAndMin(new Integer[]{1, 2});
        assertEquals(2,result.size());
        assertEquals(1,result.get(0));
        assertEquals(2,result.get(1));
    }

    @Test
    void finMaxAndMinReverse() {
        List<Integer> result = ArrayExamples.finMaxAndMin(new Integer[]{2, 1});
        assertEquals(2,result.size());
        assertEquals(1,result.get(0));
        assertEquals(2,result.get(1));
    }

    @Test
    void finMaxAndMinComplex() {
        List<Integer> result = ArrayExamples.finMaxAndMin(new Integer[]{0, -1, 1, 0, 9999, 1, 2, 1, 3 , -1});
        assertEquals(2,result.size());
        assertEquals(-1,result.get(0));
        assertEquals(9999,result.get(1));
    }

    @Test
    void finMaxAndMinNull() {
        List<Integer> result = ArrayExamples.finMaxAndMin(null);
        assertEquals(2,result.size());
        assertNull(result.get(0));
        assertNull(result.get(1));
    }

    @Test
    void findMissingNumberOk() {
        assertEquals(3, ArrayExamples.findMissingNumber(new int[]{7,5,6,1,4,2}));
        assertEquals(4, ArrayExamples.findMissingNumber(new int[]{5,3,1,2}));
    }

    @Test
    void findMissingNumberError() {
        assertNull(ArrayExamples.findMissingNumber(new int[]{1,2,5,6}));
        assertNull(ArrayExamples.findMissingNumber(new int[]{5,6}, 3, 6));
        assertNull(ArrayExamples.findMissingNumber(new int[]{1,2}, 2, 3));
        assertNull(ArrayExamples.findMissingNumber(new int[]{1,1}, 1, 2));
    }

}
