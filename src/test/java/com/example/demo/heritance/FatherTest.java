package com.example.demo.heritance;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FatherTest {

    public static final String FATHER = "Father";
    Father father = new Father();

    @Test
    void withoutThisTest(){
        assertEquals(FATHER, father.withoutThis());
    }

    @Test
    void withThisTest(){
        assertEquals(FATHER, father.withThis());
    }

    @Test
    void directAccessTest(){
        assertEquals(FATHER, father.directAccess());
    }



}
