package com.example.demo.heritance;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SonTest {

    Son son = new Son();

    @Test
    void withoutThisTest(){
        assertEquals("Son", son.withoutThis());
    }

    @Test
    void withThisTest(){
        assertEquals("Son", son.withThis());
    }

    @Test
    void directAccessTest(){
        assertEquals("Father", son.directAccess());
    }

}
