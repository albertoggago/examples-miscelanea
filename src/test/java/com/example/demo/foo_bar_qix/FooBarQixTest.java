package com.example.demo.foo_bar_qix;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class FooBarQixTest {


    @Test
    void userWrongConstructor() throws NoSuchMethodException {
        Constructor<FooBarQix> constructor = FooBarQix.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);

        assertThrows(InvocationTargetException.class, constructor::newInstance);
    }
    @Test
    void oneTest(){
        String result = FooBarQix.compute(1);
        assertEquals("1",result);
    }

    @Test
    void twoTest(){
        String result = FooBarQix.compute(2);
        assertEquals("2",result);
    }

    @Test
    void threeTest(){
        String result = FooBarQix.compute(3);
        assertEquals("FooFoo",result);
    }

    @Test
    void fourTest(){
        String result = FooBarQix.compute(4);
        assertEquals("4",result);
    }

    @Test
    void FiveTest(){
        String result = FooBarQix.compute(5);
        assertEquals("BarBar",result);
    }

    @Test
    void SixTest(){
        String result = FooBarQix.compute(6);
        assertEquals("Foo",result);
    }

    @Test
    void SevenTest(){
        String result = FooBarQix.compute(7);
        assertEquals("QixQix",result);
    }

    @Test
    void EightTest(){
        String result = FooBarQix.compute(8);
        assertEquals("8",result);
    }

    @Test
    void NineTest(){
        String result = FooBarQix.compute(9);
        assertEquals("Foo",result);
    }

    @Test
    void TenTest(){
        String result = FooBarQix.compute(10);
        assertEquals("Bar*",result);
    }

    @Test
    void ThirteenTest(){
        String result = FooBarQix.compute(13);
        assertEquals("Foo",result);
    }

    @Test
    void FifteenTest(){
        String result = FooBarQix.compute(15);
        assertEquals("FooBarBar",result);
    }

    @Test
    void TwentyOneTest(){
        String result = FooBarQix.compute(21);
        assertEquals("FooQix",result);
    }

    @Test
    void ThirtyThreeTest(){
        String result = FooBarQix.compute(33);
        assertEquals("FooFooFoo",result);
    }

    @Test
    void FiftyOneTest(){
        String result = FooBarQix.compute(51);
        assertEquals("FooBar",result);
    }

    @Test
    void FiftyThreeTest(){
        String result = FooBarQix.compute(53);
        assertEquals("BarFoo",result);
    }

    @Test
    void OneHundredOneTest(){
        String result = FooBarQix.compute(101);
        assertEquals("1*1",result);
    }

    @Test
    void ThreeHundredThreeTest(){
        String result = FooBarQix.compute(303);
        assertEquals("FooFoo*Foo",result);
    }

    @Test
    void OneHundredFiveTest(){
        String result = FooBarQix.compute(105);
        assertEquals("FooBarQix*Bar",result);
    }

    @Test
    void Number10101Test(){
        String result = FooBarQix.compute(10101);
        assertEquals("FooQix**",result);
    }




}
