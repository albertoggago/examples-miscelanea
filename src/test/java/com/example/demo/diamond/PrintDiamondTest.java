package com.example.demo.diamond;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class PrintDiamondTest {

    @Test
    void printDiamondNulls(){
        assertNull(PrintDiamond.printDiamond('0'));
        assertNull(PrintDiamond.printDiamond('a'));
        assertNull(PrintDiamond.printDiamond(null));
    }
    @Test
    void printDiamondA(){
        String result = PrintDiamond.printDiamond('A');

        assertEquals("A", result);
    }

    @Test
    void printDiamondB(){
        String result = PrintDiamond.printDiamond('B');

        assertEquals(" A \nB B\n A ", result);
    }

    @Test
    void printDiamondC(){
        String result = PrintDiamond.printDiamond('C');

        assertEquals("  A  \n B B \nC   C\n B B \n  A  ", result);
    }

    @Test
    void printDiamondD(){
        String result = PrintDiamond.printDiamond('D');

        assertEquals("   A   \n  B B  \n C   C \nD     D\n C   C \n  B B  \n   A   ", result);
    }
}
