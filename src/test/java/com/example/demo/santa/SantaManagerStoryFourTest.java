package com.example.demo.santa;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SantaManagerStoryFourTest {
    SantaManagerStoryFour santaManagerStoryFour = new SantaManagerStoryFour();
    String expectedStringPresentOne = "Toy Machine ";
    String expectedStringPresentTwo = "--> (Gives Present) --> MrsClaus --> (Gives Present) --> Elf ";
    String expectedStringPresentTree = "--> (Packs onto) --> SantaSleigh";

    public static class PairTest
    {
        public String key;
        public int value;

        public PairTest(String key, int value)
        {
            this.key = key;
            this.value = value;
        }

    }

    @Test
    void testingUserStory1() {


        List<String> badFamiliesToTest = Arrays.asList("Family2", "Family3");
        santaManagerStoryFour.setBadFamilies(badFamiliesToTest);
        santaManagerStoryFour.run();
        assertTrue(SantaManagerStoryTwo.NUMBER_PRESENTS*SantaManagerStoryTree.NUM_TOY_MACHINES > santaManagerStoryFour.getSantaSleigh().getPresents().size());
        assertNull(santaManagerStoryFour.getMrsClaus().getNextPresent());


        santaManagerStoryFour.getSantaSleigh().getPresents().forEach(present->assertTrue(present.toString().contains(expectedStringPresentOne)));
        santaManagerStoryFour.getSantaSleigh().getPresents().forEach(present->assertTrue(present.toString().contains(expectedStringPresentTwo)));
        santaManagerStoryFour.getSantaSleigh().getPresents().forEach(present->assertTrue(present.toString().contains(expectedStringPresentTree)));
        santaManagerStoryFour.getSantaSleigh().getPresents().forEach(present->assertEquals(7, present.getHistoricActions().size()));

        assertEquals(0,santaManagerStoryFour.getSantaSleigh().getPresents()
                .stream()
                .filter(a -> badFamiliesToTest.contains(a.getFamily())).count());

        List<PairTest> groupFamilies = new ArrayList<>();
        for (Present present: santaManagerStoryFour.getSantaSleigh().getPresents()) {
            if (groupFamilies.isEmpty() || !groupFamilies.getLast().key.equals(present.getFamily())) {
                groupFamilies.add(new PairTest(present.getFamily(), 1));
            } else {
                groupFamilies.getLast().value = groupFamilies.getLast().value+1;
            }
        }
        assertTrue(groupFamilies.size()>10);
        assertTrue(groupFamilies.stream().filter(pairTest->pairTest.value>10).count()<10);
    }

}

