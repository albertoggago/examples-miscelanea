package com.example.demo.santa;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SantaManagerStoryTwoTest {
    SantaManagerStoryTwo santaManagerStoryTwo = new SantaManagerStoryTwo();
    String expectedStringPresentOne = "Toy Machine ";
    String expectedStringPresentTwo = "--> (Gives Present) --> MrsClaus --> (Gives Present) --> Elf ";
    String expectedStringPresentTree = "--> (Packs onto) --> SantaSleigh";

    @Test
    void testingUserStory1()  {
        santaManagerStoryTwo.run();
        assertEquals(SantaManagerStoryTwo.NUMBER_PRESENTS*SantaManagerStoryTwo.NUM_TOY_MACHINES,santaManagerStoryTwo.getSantaSleigh().getPresents().size());
        assertNull(santaManagerStoryTwo.getMrsClaus().getNextPresent());


        santaManagerStoryTwo.getSantaSleigh().getPresents().forEach(presentX->assertTrue(presentX.toString().contains(expectedStringPresentOne)));
        santaManagerStoryTwo.getSantaSleigh().getPresents().forEach(presentX->assertTrue(presentX.toString().contains(expectedStringPresentTwo)));
        santaManagerStoryTwo.getSantaSleigh().getPresents().forEach(presentX->assertTrue(presentX.toString().contains(expectedStringPresentTree)));
        santaManagerStoryTwo.getSantaSleigh().getPresents().forEach(presentX->assertEquals(7, presentX.getHistoricActions().size()));
    }

}

