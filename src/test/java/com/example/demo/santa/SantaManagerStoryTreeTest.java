package com.example.demo.santa;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SantaManagerStoryTreeTest {
    SantaManagerStoryTree santaManagerStoryTree = new SantaManagerStoryTree();
    String expectedStringPresentOne = "Toy Machine ";
    String expectedStringPresentTwo = "--> (Gives Present) --> MrsClaus --> (Gives Present) --> Elf ";
    String expectedStringPresentTree = "--> (Packs onto) --> SantaSleigh";

    public static class PairTest
    {
        public String key;
        public int value;

        public PairTest(String key, int value)
        {
            this.key = key;
            this.value = value;
        }

    }

    @Test
    void testingUserStory1() {


        santaManagerStoryTree.run();
        assertEquals(SantaManagerStoryTwo.NUMBER_PRESENTS*SantaManagerStoryTwo.NUM_TOY_MACHINES, santaManagerStoryTree.getSantaSleigh().getPresents().size());
        assertNull(santaManagerStoryTree.getMrsClaus().getNextPresent());


        santaManagerStoryTree.getSantaSleigh().getPresents().forEach(present->assertTrue(present.toString().contains(expectedStringPresentOne)));
        santaManagerStoryTree.getSantaSleigh().getPresents().forEach(present->assertTrue(present.toString().contains(expectedStringPresentTwo)));
        santaManagerStoryTree.getSantaSleigh().getPresents().forEach(present->assertTrue(present.toString().contains(expectedStringPresentTree)));
        santaManagerStoryTree.getSantaSleigh().getPresents().forEach(present->assertEquals(7, present.getHistoricActions().size()));

        List<PairTest> groupFamilies = new ArrayList<>();
        for (Present present: santaManagerStoryTree.getSantaSleigh().getPresents()) {
            if (groupFamilies.isEmpty() || !groupFamilies.getLast().key.equals(present.getFamily())) {
                groupFamilies.add(new PairTest(present.getFamily(), 1));
            } else {
                groupFamilies.getLast().value = groupFamilies.getLast().value+1;
            }
        }
        assertTrue(groupFamilies.size()>10);
        assertTrue(groupFamilies.stream().filter(pairTest->pairTest.value>10).count()<10);
    }

}

