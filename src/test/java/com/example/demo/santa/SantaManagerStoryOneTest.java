package com.example.demo.santa;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SantaManagerStoryOneTest {
    SantaManagerStoryOne santaManagerStoryOne = new SantaManagerStoryOne();
    @Test
    void testingUserStory1()  {
        santaManagerStoryOne.run();
        assertEquals(1, santaManagerStoryOne.getSantaSleigh().getPresents().size());
        Present present = santaManagerStoryOne.getSantaSleigh().getPresents().getFirst();
        assertEquals("ToyMachine --> (Gives Present) --> Sole Elf --> (Packs onto) --> SantaSleigh",
                      present.toString());
        assertEquals("New Present", present.getName());
        assertEquals(5, present.getHistoricActions().size());
        assertEquals("Sole Elf", santaManagerStoryOne.getElf().getName());
        assertNull(santaManagerStoryOne.getElf().getPresent());
    }

    @Test
    void testingUserStory1ErrorElfOverChange()  {
        santaManagerStoryOne.getElf().packing(santaManagerStoryOne.getToyMachine().createPresent("Other Present"));

        ElfException elfException = assertThrows(ElfException.class, santaManagerStoryOne::run);
        assertEquals("Error the Elf is busy", elfException.getMessage());
    }

}

