package com.example.demo.list;

import org.junit.jupiter.api.Test;

import jakarta.validation.constraints.NotNull;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static com.example.demo.list.BalancedTreeBuilder.*;
import static org.junit.jupiter.api.Assertions.*;

class BalancedTreeListTest {
    private static final String OBJECT_1 = "A-One";
    private static final String OBJECT_2 = "B-Two";
    private static final String OBJECT_3 = "C-tree";
    private static final String OBJECT_4 = "D-Four";
    private static final String OBJECT_5 = "E-Five";
    private static final String OBJECT_6 = "F-Six";
    private static final String OBJECT_7 = "G-Seven";
    private static final String OBJECT_8 = "H-Eight";
    private static final String OBJECT_9 = "I-Nine";
    private static final String OBJECT_10 = "J-Ten";

    @Test
    void testVoidComponent(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        assertTrue(balancedTreeList.isEmpty());

        assertEquals(0, balancedTreeList.getLength());
        assertEquals(1, balancedTreeList.getDeep());
        assertNull(get(balancedTreeList, 1));
        assertNull(getFirst(balancedTreeList));
        assertNull(getLast(balancedTreeList));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
        assertEquals("// * (1)null(0) * \\\\",balancedTreeList.toString());
        assertEquals(ListDiff.Typist.BALANCED_TREE_LIST, balancedTreeList.getType());
    }


    @Test
    void testVoidOtherConstructor(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>(null);

        assertTrue(balancedTreeList.isEmpty());
    }

    @Test
    void testOneComponentOk(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>(null, null, OBJECT_1);

        assertFalse(balancedTreeList.isEmpty());
        assertEquals(1, balancedTreeList.getLength());
        assertEquals(1, balancedTreeList.getDeep());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertNull(get(balancedTreeList, 2));
        assertEquals(OBJECT_1,getFirst(balancedTreeList));
        assertEquals(OBJECT_1,getLast(balancedTreeList));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
        assertEquals("// * (1)A-One(1) * \\\\",balancedTreeList.toString());
    }

    @Test
    void testTwoComponentLeft(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_1);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(balancedTreeList1, null, OBJECT_2);

        assertEquals(2, balancedTreeList2.getLength());
        assertEquals(2, balancedTreeList2.getDeep());
        assertFalse(balancedTreeList2.isEmpty());
        assertEquals(OBJECT_1, get(balancedTreeList2, 1));
        assertEquals(OBJECT_2, get(balancedTreeList2, 2));
        assertNull(get(balancedTreeList2, 3));
        assertEquals(OBJECT_1,getFirst(balancedTreeList2));
        assertEquals(OBJECT_2,getLast(balancedTreeList2));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList2.verifyQuality());
    }
    @Test

    void testTwoComponentRight(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_2);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(null, balancedTreeList1, OBJECT_1);

        assertEquals(2, balancedTreeList2.getLength());
        assertEquals(2, balancedTreeList2.getDeep());
        assertFalse(balancedTreeList2.isEmpty());
        assertEquals(OBJECT_1, get(balancedTreeList2, 1));
        assertEquals(OBJECT_2, get(balancedTreeList2, 2));
        assertNull(get(balancedTreeList2, 3));
        assertEquals(OBJECT_1,getFirst(balancedTreeList2));
        assertEquals(OBJECT_2,getLast(balancedTreeList2));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList2.verifyQuality());
    }

    @Test
    void testTreeComponentS(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_1);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(null, null, OBJECT_3);
        BalancedTreeList<String> balancedTreeList3 = new BalancedTreeList<>(balancedTreeList1, balancedTreeList2, OBJECT_2);

        assertEquals(3, balancedTreeList3.getLength());
        assertEquals(2, balancedTreeList3.getDeep());
        assertEquals(OBJECT_1, get(balancedTreeList3, 1));
        assertEquals(OBJECT_2, get(balancedTreeList3, 2));
        assertEquals(OBJECT_3, get(balancedTreeList3, 3));
        assertNull(get(balancedTreeList3, 4));
        assertEquals(OBJECT_1,getFirst(balancedTreeList3));
        assertEquals(OBJECT_3,getLast(balancedTreeList3));
        assertFalse(balancedTreeList3.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList3.verifyQuality());
        assertEquals("// // * (1)A-One(1) * \\\\ (2)B-Two(3) // * (1)C-tree(1) * \\\\ \\\\",balancedTreeList3.toString());
    }

    @Test
    void addOrderedOneComponentInVoid(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        BalancedTreeList<String> result = addFirst(balancedTreeList, OBJECT_1);

        assertEquals(1, result.getLength());
        assertEquals(1, result.getDeep());
        assertEquals(OBJECT_1, get(result, 1));
        assertEquals(ListDiff.TypeQuality.OK, result.verifyQuality());
    }

    @Test
    void addTwoComponentsAscending(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addLast(balancedTreeList, OBJECT_1), OBJECT_2);

        assertEquals(2, balancedTreeList.getLength());
        assertEquals(2, balancedTreeList.getDeep());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addTwoComponentsDescending(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1);

        assertEquals(2, balancedTreeList.getLength());
        assertEquals(2, balancedTreeList.getDeep());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals123(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addLast(balancedTreeList, OBJECT_1), OBJECT_2), OBJECT_3);

        assertEquals(3, balancedTreeList.getLength());
        assertEquals(2, balancedTreeList.getDeep());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(ListDiff.TypeQuality.OK, balancedTreeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals132(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addLast(balancedTreeList, OBJECT_1), OBJECT_3), OBJECT_2);

        assertEquals(3, balancedTreeList.getLength());
        assertEquals(2, balancedTreeList.getDeep());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(ListDiff.TypeQuality.OK, balancedTreeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals213(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addLast(balancedTreeList, OBJECT_2), OBJECT_1), OBJECT_3);

        assertEquals(3,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(ListDiff.TypeQuality.OK, balancedTreeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals231(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addLast(balancedTreeList, OBJECT_2), OBJECT_3), OBJECT_1);

        assertEquals(3, balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(ListDiff.TypeQuality.OK, balancedTreeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals312(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addLast(balancedTreeList, OBJECT_3), OBJECT_1), OBJECT_2);

        assertEquals(3, balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(ListDiff.TypeQuality.OK, balancedTreeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals321(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addLast(balancedTreeList, OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(3, balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(ListDiff.TypeQuality.OK, balancedTreeList.verifyQuality());
    }

    @Test
    void addFourComponentsEquals12345(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(
                balancedTreeList, OBJECT_1), OBJECT_2), OBJECT_3), OBJECT_4), OBJECT_5);

        assertEquals(5, balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(ListDiff.TypeQuality.OK, balancedTreeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals12354(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(
                balancedTreeList, OBJECT_1), OBJECT_2), OBJECT_3), OBJECT_5), OBJECT_4);

        assertEquals(5, balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals12435(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(
                balancedTreeList, OBJECT_1), OBJECT_2), OBJECT_4), OBJECT_3), OBJECT_5);

        assertEquals(5, balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals12534(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(
                balancedTreeList, OBJECT_1), OBJECT_2), OBJECT_5), OBJECT_3), OBJECT_4);

        assertEquals(5,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals12543(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(
                balancedTreeList, OBJECT_1), OBJECT_2), OBJECT_5), OBJECT_4), OBJECT_3);

        assertEquals(5,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals32415(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(
                balancedTreeList, OBJECT_3), OBJECT_2), OBJECT_4), OBJECT_1), OBJECT_5);

        assertEquals(5,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }







    @Test
    void testVoidComponentBadDiff(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        balancedTreeList.setDeep(4);

        assertEquals(0,balancedTreeList.getLength());
        assertEquals(4,balancedTreeList.getDeep());
        assertTrue(balancedTreeList.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_DEEP,balancedTreeList.verifyQuality());
    }

    @Test
    void testOneElementOkFalseDeep(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>(null, null, OBJECT_1);
        balancedTreeList.setDeep(0);

        assertEquals(0,balancedTreeList.getDeep());
        assertFalse(balancedTreeList.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_DEEP,balancedTreeList.verifyQuality());
    }

    @Test
    void testOneElementOkFalseLength(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>(null, null, OBJECT_1);
        balancedTreeList.setLength(0);

        assertEquals(0,balancedTreeList.getLength());
        assertFalse(balancedTreeList.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_LENGTH,balancedTreeList.verifyQuality());
    }

    @Test
    void testBadComponentLeft(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_1);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(balancedTreeList1, null, null);

        assertEquals(2,balancedTreeList2.getDeep());
        assertEquals(0,balancedTreeList2.getLength());
        assertFalse(balancedTreeList2.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_ROOT,balancedTreeList2.verifyQuality());
    }

    @Test
    void testBadComponentRight(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_1);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(null, balancedTreeList1, null);

        assertEquals(0,balancedTreeList2.getLength());
        assertEquals(2,balancedTreeList2.getDeep());
        assertFalse(balancedTreeList2.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_ROOT,balancedTreeList2.verifyQuality());
    }

    @Test
    void testBadComponentLeftAndRight(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_1);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(null, null, OBJECT_2);
        BalancedTreeList<String> balancedTreeList3 = new BalancedTreeList<>(balancedTreeList1, balancedTreeList2, null);

        assertEquals(0,balancedTreeList3.getLength());
        assertEquals(2,balancedTreeList3.getDeep());
        assertFalse(balancedTreeList3.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_ROOT,balancedTreeList3.verifyQuality());
    }

    @Test
    void testBadBalanceLeft(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_1);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(balancedTreeList1, null, OBJECT_2);
        BalancedTreeList<String> balancedTreeList3 = new BalancedTreeList<>(balancedTreeList2, null, OBJECT_3);

        assertEquals(ListDiff.TypeQuality.ERROR_BALANCED,balancedTreeList3.verifyQuality());
    }

    @Test
    void testBadBalanceRight(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_3);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(null, balancedTreeList1, OBJECT_2);
        BalancedTreeList<String> balancedTreeList3 = new BalancedTreeList<>(null, balancedTreeList2, OBJECT_1);

        assertEquals(ListDiff.TypeQuality.ERROR_BALANCED,balancedTreeList3.verifyQuality());
    }

    @Test
    void testBadBalanceRightLeft(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_1);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(balancedTreeList1, null, OBJECT_2);
        BalancedTreeList<String> balancedTreeList3 = new BalancedTreeList<>(null, null, OBJECT_4);
        BalancedTreeList<String> balancedTreeList4 = new BalancedTreeList<>(balancedTreeList2, balancedTreeList3, OBJECT_3);
        BalancedTreeList<String> balancedTreeList5 = new BalancedTreeList<>(null, null, OBJECT_6);
        BalancedTreeList<String> balancedTreeList6 = new BalancedTreeList<>(balancedTreeList4, balancedTreeList5, OBJECT_5);

        assertEquals(ListDiff.TypeQuality.ERROR_BALANCED,balancedTreeList6.verifyQuality());
    }

    @Test
    void testBadOrderLeft(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_2);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>(balancedTreeList1, null, OBJECT_1);

        assertEquals(ListDiff.TypeQuality.ERROR_ORDER,balancedTreeList2.verifyQuality());
    }

    @Test
    void testBadOrderRight(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, OBJECT_1);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>( null, balancedTreeList1, OBJECT_2);

        assertEquals(ListDiff.TypeQuality.ERROR_ORDER,balancedTreeList2.verifyQuality());
    }

    @Test
    void testQualityLeftListBad(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, null);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>( balancedTreeList1, null, OBJECT_2);
        balancedTreeList1.setLength(1);
        balancedTreeList2.setLength(2);

        assertEquals(ListDiff.TypeQuality.ERROR_LEFT_BRANCH,balancedTreeList2.verifyQuality());
    }

    @Test
    void testQualityRightListBad(){
        BalancedTreeList<String> balancedTreeList1 = new BalancedTreeList<>(null, null, null);
        BalancedTreeList<String> balancedTreeList2 = new BalancedTreeList<>( null, balancedTreeList1, OBJECT_2);
        balancedTreeList1.setLength(1);
        balancedTreeList2.setLength(2);

        assertEquals(ListDiff.TypeQuality.ERROR_RIGHT_BRANCH,balancedTreeList2.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals54321(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(5,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addSixComponentsEquals654321(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_6), OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(6,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(OBJECT_6, get(balancedTreeList, 6));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addSevenComponentsEquals0987654321(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_7), OBJECT_6), OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);
        
        assertEquals(7,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(OBJECT_6, get(balancedTreeList, 6));
        assertEquals(OBJECT_7, get(balancedTreeList, 7));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addEightComponentsEquals0987654321(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_8), OBJECT_7), OBJECT_6), OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(8,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(OBJECT_6, get(balancedTreeList, 6));
        assertEquals(OBJECT_7, get(balancedTreeList, 7));
        assertEquals(OBJECT_8, get(balancedTreeList, 8));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }


    @Test
    void addNineComponentsEquals0987654321(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_9), OBJECT_8), OBJECT_7), OBJECT_6), OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(9,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(OBJECT_6, get(balancedTreeList, 6));
        assertEquals(OBJECT_7, get(balancedTreeList, 7));
        assertEquals(OBJECT_8, get(balancedTreeList, 8));
        assertEquals(OBJECT_9, get(balancedTreeList, 9));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }


    @Test
    void addTenComponentsEquals0987654321(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_8), OBJECT_5), OBJECT_10), OBJECT_3), OBJECT_7), OBJECT_9), OBJECT_2), OBJECT_4), OBJECT_6), OBJECT_1);

        assertEquals(10,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(OBJECT_6, get(balancedTreeList, 6));
        assertEquals(OBJECT_7, get(balancedTreeList, 7));
        assertEquals(OBJECT_8, get(balancedTreeList, 8));
        assertEquals(OBJECT_9, get(balancedTreeList, 9));
        assertEquals(OBJECT_10, get(balancedTreeList, 10));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void addTenComponentsEquals1234567890(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_1), OBJECT_2), OBJECT_3), OBJECT_4), OBJECT_5), OBJECT_6), OBJECT_7), OBJECT_8), OBJECT_9), OBJECT_10);

        assertEquals(10,balancedTreeList.getLength());
        assertEquals(OBJECT_1, get(balancedTreeList, 1));
        assertEquals(OBJECT_2, get(balancedTreeList, 2));
        assertEquals(OBJECT_3, get(balancedTreeList, 3));
        assertEquals(OBJECT_4, get(balancedTreeList, 4));
        assertEquals(OBJECT_5, get(balancedTreeList, 5));
        assertEquals(OBJECT_6, get(balancedTreeList, 6));
        assertEquals(OBJECT_7, get(balancedTreeList, 7));
        assertEquals(OBJECT_8, get(balancedTreeList, 8));
        assertEquals(OBJECT_9, get(balancedTreeList, 9));
        assertEquals(OBJECT_10, get(balancedTreeList, 10));
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveFirstNullTree(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        String removed = removeFirst(balancedTreeList);

        assertTrue(balancedTreeList.isEmpty());
        assertNull(removed);
        assertEquals(0,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveFirstOneElement(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(balancedTreeList, OBJECT_1);

        String removed = BalancedTreeBuilder.removeFirst(balancedTreeList);

        assertTrue(balancedTreeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(0,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveFirstTwoElementsBalancedLeft(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1);

        String removed = BalancedTreeBuilder.removeFirst(balancedTreeList);

        assertFalse(balancedTreeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveFirstTwoElementsBalancedRight(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_1), OBJECT_2);

        String removed = BalancedTreeBuilder.removeFirst(balancedTreeList);

        assertFalse(balancedTreeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveFirstSevenElementsOrdered(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_4), OBJECT_2), OBJECT_6), OBJECT_1), OBJECT_3), OBJECT_5), OBJECT_7);

        String removed = BalancedTreeBuilder.removeFirst(balancedTreeList);

        assertFalse(balancedTreeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(6,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveLastNullTree(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        String removed = BalancedTreeBuilder.removeLast(balancedTreeList);

        assertTrue(balancedTreeList.isEmpty());
        assertNull(removed);
        assertEquals(0,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveLastOneElement(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(balancedTreeList, OBJECT_1);

        String removed = BalancedTreeBuilder.removeLast(balancedTreeList);

        assertTrue(balancedTreeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(0,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveLastTwoElementsBalancedLeft(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1);

        String removed = BalancedTreeBuilder.removeLast(balancedTreeList);

        assertFalse(balancedTreeList.isEmpty());
        assertEquals(OBJECT_2, removed);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveLastTwoElementsBalancedRight(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_1), OBJECT_2);

        String removed = BalancedTreeBuilder.removeLast(balancedTreeList);

        assertFalse(balancedTreeList.isEmpty());
        assertEquals(OBJECT_2, removed);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveLastSevenElementsLevelNotOrdered(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_5), OBJECT_2), OBJECT_6), OBJECT_1), OBJECT_3), OBJECT_4), OBJECT_7);

        String removed = BalancedTreeBuilder.removeLast(balancedTreeList);

        assertFalse(balancedTreeList.isEmpty());
        assertEquals(OBJECT_7, removed);
        assertEquals(6,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveLastSevenElementsOrdered(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                balancedTreeList, OBJECT_4), OBJECT_2), OBJECT_6), OBJECT_1), OBJECT_3), OBJECT_5), OBJECT_7);

        String removed = BalancedTreeBuilder.removeLast(balancedTreeList);

        assertFalse(balancedTreeList.isEmpty());
        assertEquals(OBJECT_7, removed);
        assertEquals(6,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectNotFoundNullComponent(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_1);

        assertFalse(isRemoved);
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectFoundOneComponent(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(balancedTreeList, OBJECT_1);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_1);

        assertTrue(isRemoved);
        assertEquals(0,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectNoFoundOneComponentGreater(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(balancedTreeList, OBJECT_1);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_2);

        assertFalse(isRemoved);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectNoFoundOneComponentLower(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(balancedTreeList, OBJECT_2);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_1);

        assertFalse(isRemoved);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectTwoComponentsFoundOneComponentHead(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_1), OBJECT_2);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_1);

        assertTrue(isRemoved);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectTwoComponentsFoundOneComponentHeadOtherBalance(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_2);

        assertTrue(isRemoved);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectTwoComponentsFoundOneComponentLeftBranch(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_1);

        assertTrue(isRemoved);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectTwoComponentsFoundOneComponentRightBranch(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_1), OBJECT_2);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_2);

        assertTrue(isRemoved);
        assertEquals(1,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsNotFoundToHighLeft(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_3), OBJECT_2), OBJECT_4);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_1);

        assertFalse(isRemoved);
        assertEquals(3,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsNotFoundToLeft(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_3), OBJECT_1), OBJECT_4);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_2);

        assertFalse(isRemoved);
        assertEquals(3,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsNotFoundToRight(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1), OBJECT_4);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_3);

        assertFalse(isRemoved);
        assertEquals(3,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsNotFoundToHighRight(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1), OBJECT_3);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_4);

        assertFalse(isRemoved);
        assertEquals(3,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsFoundMiddle(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1), OBJECT_3);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_2);

        assertTrue(isRemoved);
        assertEquals(2,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsFoundOnTheLeft(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1), OBJECT_3);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_1);

        assertTrue(isRemoved);
        assertEquals(2,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsFoundOnTheRight(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1), OBJECT_3);

        boolean isRemoved = removeObject(balancedTreeList, OBJECT_3);

        assertTrue(isRemoved);
        assertEquals(2,balancedTreeList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void equalsCloneTest(){
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_4), OBJECT_2), OBJECT_6);
        BalancedTreeList<String> balancedTreeListNew = (BalancedTreeList<String>) ListBuilder.clone(balancedTreeList);

        assertEquals(balancedTreeList, balancedTreeList);
        BalancedTreeList<String> balancedTreeListNull = null;
        assertNotEquals(balancedTreeListNull, balancedTreeList);
        assertNotEquals(balancedTreeList, balancedTreeListNull);
        assertNotEquals(balancedTreeList, new Object());
        assertEquals(balancedTreeListNew, balancedTreeList);
        assertEquals(balancedTreeListNew.hashCode(), balancedTreeList.hashCode());

        balancedTreeListNew.setDeep(balancedTreeList.getDeep()+1);
        assertNotEquals(balancedTreeListNew, balancedTreeList);
        balancedTreeListNew.setDeep(balancedTreeList.getDeep());

        balancedTreeListNew.setLength(balancedTreeList.getLength()+1);
        assertNotEquals(balancedTreeListNew, balancedTreeList);
        balancedTreeListNew.setLength(balancedTreeList.getLength());

        balancedTreeListNew.setElement(null);
        assertNotEquals(balancedTreeListNew, balancedTreeList);
        assertNotEquals(balancedTreeList, balancedTreeListNew);
        balancedTreeListNew.setElement(balancedTreeList.getElement());

        assertNotEquals(addOrdered(addOrdered(addOrdered(new BalancedTreeList<>(), OBJECT_5), OBJECT_2), OBJECT_6), balancedTreeList);
        assertNotEquals(addOrdered(addOrdered(addOrdered(new BalancedTreeList<>(), OBJECT_4), OBJECT_3), OBJECT_6), balancedTreeList);
        assertNotEquals(addOrdered(addOrdered(addOrdered(new BalancedTreeList<>(), OBJECT_4), OBJECT_2), OBJECT_7), balancedTreeList);

        assertEquals(new BalancedTreeList<>(), new BalancedTreeList<>());
    }

    @Test
    void equalsDifferentT(){
        BalancedTreeList<Integer> balancedTreeListInteger = new BalancedTreeList<>();
        balancedTreeListInteger.setElement(4);
        BalancedTreeList<String> balancedTreeListString = new BalancedTreeList<>();
        balancedTreeListString.setElement("4");

        assertNotEquals(balancedTreeListString, balancedTreeListInteger);
        assertNotEquals(balancedTreeListInteger, balancedTreeListString);
    }


    @Test
    void createIteratorNull() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        @NotNull TreeIterator<String> treeIteratorIterator = (TreeIterator<String>) balancedTreeList.iterator();

        assertFalse(treeIteratorIterator.hasNext());
        assertThrows(NoSuchElementException.class, treeIteratorIterator::next);
        assertFalse(treeIteratorIterator.hasPrev());
        assertThrows(NoSuchElementException.class, treeIteratorIterator::prev);
    }
    @Test
    void createIterator() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(balancedTreeList, OBJECT_1), OBJECT_2);

        @NotNull Iterator<String> iterator = balancedTreeList.iterator();

        assertTrue(iterator.hasNext());
        assertEquals(OBJECT_1, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(OBJECT_2, iterator.next());
        assertFalse(iterator.hasNext());
    }

    @Test
    void createIteratorBackup() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_1), OBJECT_2);
        @NotNull TreeIterator<String> treeIterator = (TreeIterator<String>) balancedTreeList.iterator();

        assertFalse(treeIterator.hasPrev());
        assertThrows(NoSuchElementException.class, treeIterator::prev);

        assertTrue(treeIterator.hasNext());
        assertEquals(OBJECT_1, treeIterator.next());

        assertTrue(treeIterator.hasPrev());
        assertEquals(OBJECT_1, treeIterator.prev());

        assertFalse(treeIterator.hasPrev());
        assertThrows(NoSuchElementException.class, treeIterator::prev);

        assertTrue(treeIterator.hasNext());
        assertEquals(OBJECT_1, treeIterator.next());
    }

    @Test
    void createIteratorStream() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_3), OBJECT_1), OBJECT_2), OBJECT_4), OBJECT_5);
        @NotNull TreeIterator<String> treeIterator = (TreeIterator<String>) balancedTreeList.iterator();

        List<String> list = Stream.generate(() -> null).takeWhile(x -> treeIterator.hasNext()).map(z -> treeIterator.next()).toList();
        List<String> listPrev = Stream.generate(() -> null).takeWhile(x -> treeIterator.hasPrev()).map(z -> treeIterator.prev()).toList();

        assertEquals(5, list.size());
        assertEquals(OBJECT_1, list.get(0));
        assertEquals(OBJECT_2, list.get(1));
        assertEquals(OBJECT_3, list.get(2));
        assertEquals(OBJECT_4, list.get(3));
        assertEquals(OBJECT_5, list.get(4));
        assertEquals(5, listPrev.size());
        assertEquals(OBJECT_5, listPrev.get(0));
        assertEquals(OBJECT_4, listPrev.get(1));
        assertEquals(OBJECT_3, listPrev.get(2));
        assertEquals(OBJECT_2, listPrev.get(3));
        assertEquals(OBJECT_1, listPrev.get(4));
    }

    @Test
    void createIteratorSoftNull() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) balancedTreeList.iteratorSoft();

        assertFalse(treeIteratorSoft.hasNext());
        assertThrows(NoSuchElementException.class, treeIteratorSoft::next);
        assertFalse(treeIteratorSoft.hasPrev());
        assertThrows(NoSuchElementException.class, treeIteratorSoft::prev);
    }
    @Test
    void createIteratorSoft() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(balancedTreeList, OBJECT_1), OBJECT_2);

        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) balancedTreeList.iteratorSoft();

        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_1, treeIteratorSoft.next());
        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_2, treeIteratorSoft.next());
        assertFalse(treeIteratorSoft.hasNext());
    }

    @Test
    void createIteratorOtherOrderSoft() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();

        addOrdered(addOrdered(balancedTreeList, OBJECT_2), OBJECT_1);

        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) balancedTreeList.iteratorSoft();

        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_2, treeIteratorSoft.next());
        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_1, treeIteratorSoft.next());
        assertFalse(treeIteratorSoft.hasNext());
    }

    @Test
    void createIteratorSoftBackup() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(balancedTreeList, OBJECT_1), OBJECT_2);
        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) balancedTreeList.iteratorSoft();

        assertFalse(treeIteratorSoft.hasPrev());
        assertThrows(NoSuchElementException.class, treeIteratorSoft::prev);

        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_1, treeIteratorSoft.next());

        assertTrue(treeIteratorSoft.hasPrev());
        assertEquals(OBJECT_1, treeIteratorSoft.prev());

        assertFalse(treeIteratorSoft.hasPrev());
        assertThrows(NoSuchElementException.class, treeIteratorSoft::prev);

        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_1, treeIteratorSoft.next());
    }

    @Test
    void createIteratorSoftStream() {
        BalancedTreeList<String> balancedTreeList = new BalancedTreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(balancedTreeList, OBJECT_3), OBJECT_1), OBJECT_2), OBJECT_4), OBJECT_5);
        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) balancedTreeList.iteratorSoft();

        List<String> list = Stream.generate(() -> null).takeWhile(x -> treeIteratorSoft.hasNext()).map(z -> treeIteratorSoft.next()).toList();
        List<String> listPrev = Stream.generate(() -> null).takeWhile(x -> treeIteratorSoft.hasPrev()).map(z -> treeIteratorSoft.prev()).toList();

        assertEquals(5, list.size());
        assertEquals(OBJECT_2, list.get(0));
        assertEquals(OBJECT_1, list.get(1));
        assertEquals(OBJECT_4, list.get(2));
        assertEquals(OBJECT_3, list.get(3));
        assertEquals(OBJECT_5, list.get(4));
        assertEquals(5, listPrev.size());
        assertEquals(OBJECT_5, listPrev.get(0));
        assertEquals(OBJECT_3, listPrev.get(1));
        assertEquals(OBJECT_4, listPrev.get(2));
        assertEquals(OBJECT_1, listPrev.get(3));
        assertEquals(OBJECT_2, listPrev.get(4));
    }
}