package com.example.demo.list;

public class ListDiffMock<T extends Comparable<T>> extends ListBuilderTest implements ListDiff<T> {

	@Override
	public Typist getType() {
		return Typist.ND;
	}

	@Override
	public long getLength() {
		return 0;
	}

	@Override
	public long getDeep() {
		return 0;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public TypeQuality verifyQuality() {
		return null;
	}

	@Override
	public void setRightElement(ListDiff<T> leftElement) { // Noncompliant - method is empty
	}

	@Override
	public void setLeftElement(ListDiff<T> leftElement) { // Noncompliant - method is empty
	}
}
