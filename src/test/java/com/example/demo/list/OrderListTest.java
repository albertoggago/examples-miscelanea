package com.example.demo.list;


import org.junit.jupiter.api.Test;

import jakarta.validation.constraints.NotNull;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static com.example.demo.list.OrderBuilder.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderListTest {
    private static final String OBJECT_1 = "A-One";
    private static final String OBJECT_2 = "B-Two";
    private static final String OBJECT_3 = "C-tree";
    private static final String OBJECT_4 = "D-Four";
    private static final String OBJECT_5 = "E-Five";
    private static final String OBJECT_6 = "F-Six";
    private static final String OBJECT_7 = "G-Seven";

    @Test
    void testCreateOrderList (){
        OrderList<String> orderList = new OrderList<>();

        assertEquals(0L,orderList.getLength());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertTrue(orderList.isEmpty());
        assertEquals(ListDiff.Typist.ORDERED_LIST, orderList.getType());
    }

    @Test
    void testAddOrdered() {
        OrderList<String> orderList = new OrderList<>();

        addOrdered(orderList, OBJECT_1);

        assertEquals(1L,orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1,getFirst(orderList));
        assertEquals(OBJECT_1,getLast(orderList));
    }

    @Test
    void testAddTwoAscending() {
        OrderList<String> orderList = new OrderList<>();

        addOrdered(addOrdered(orderList, OBJECT_1), OBJECT_2);

        assertEquals(2L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1, get(orderList, 1));
        assertEquals(OBJECT_2, get(orderList, 2));
    }

    @Test
    void testAddTwoDescending() {
        OrderList<String> orderList = new OrderList<>();

        addOrdered(addOrdered(orderList, OBJECT_2), OBJECT_1);

        assertEquals(2L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1, get(orderList, 1));
        assertEquals(OBJECT_2, get(orderList, 2));

    }

    @Test
    void testAddTreeAscending() {
        OrderList<String> orderList = new OrderList<>();

        addOrdered(addOrdered(addOrdered(orderList, OBJECT_1), OBJECT_2), OBJECT_3);

        assertEquals(3L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1, get(orderList, 1));
        assertEquals(OBJECT_2, get(orderList, 2));
        assertEquals(OBJECT_3, get(orderList, 3));
    }

    @Test
    void testAddTreeDescending() {
        OrderList<String> orderList = new OrderList<>();

        addOrdered(addOrdered(addOrdered(orderList, OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(3L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1, get(orderList, 1));
        assertEquals(OBJECT_2, get(orderList, 2));
        assertEquals(OBJECT_3, get(orderList, 3));
    }

    @Test
    void testAddTreeRandom() {
        OrderList<String> orderList = new OrderList<>();

        addOrdered(addOrdered(addOrdered(orderList, OBJECT_2), OBJECT_1), OBJECT_3);

        assertEquals(3L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1, get(orderList, 1));
        assertEquals(OBJECT_2, get(orderList, 2));
        assertEquals(OBJECT_3, get(orderList, 3));
    }


    @Test
    void testAddNothingRemoveFirst() {
        OrderList<String> orderList = new OrderList<>();

        Object removed = OrderBuilder.removeFirst(orderList);

        assertEquals(0L, orderList.getLength());
        assertTrue(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertNull(removed);
    }

    @Test
    void testAddOneRemoveFirst() {
        OrderList<String> orderList = new OrderList<>();

        Object removed = removeFirst(addOrdered(orderList, OBJECT_1));

        assertEquals(0L, orderList.getLength());
        assertTrue(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1,removed);
    }

    @Test
    void testAddTwoRemoveFirst() {
        OrderList<String> orderList = new OrderList<>();

        Object removed = removeFirst(addOrdered(addOrdered(orderList, OBJECT_1), OBJECT_2));

        assertEquals(1L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_2, get(orderList, 1));
        assertEquals(OBJECT_1,removed);
    }

    @Test
    void testAddTreeRemoveFirst() {
        OrderList<String> orderList = new OrderList<>();

        Object removed = removeFirst(addOrdered(addOrdered(addOrdered(orderList, OBJECT_1), OBJECT_2), OBJECT_3));

        assertEquals(2L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_2, get(orderList, 1));
        assertEquals(OBJECT_3, get(orderList, 2));
        assertEquals(OBJECT_1,removed);
    }


    @Test
    void testAddNothingRemoveLast() {
        OrderList<String> orderList = new OrderList<>();

        Object removed = OrderBuilder.removeLast(orderList);

        assertEquals(0L, orderList.getLength());
        assertTrue(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertNull(removed);
    }

    @Test
    void testAddOneRemoveLast() {
        OrderList<String> orderList = new OrderList<>();

        Object removed = removeLast(addLast(orderList, OBJECT_1));

        assertEquals(0L, orderList.getLength());
        assertTrue(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1,removed);
    }

    @Test
    void testAddTwoRemoveLast() {
        OrderList<String> orderList = new OrderList<>();

        Object removed = removeLast(addLast(addLast(orderList, OBJECT_1), OBJECT_2));

        assertEquals(1L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1, get(orderList, 1));
        assertEquals(OBJECT_2,removed);
    }

    @Test
    void testAddTreeRemoveLast() {
        OrderList<String> orderList = new OrderList<>();

        Object removed = removeLast(addLast(addLast(addLast(orderList, OBJECT_1), OBJECT_2), OBJECT_3));

        assertEquals(2L, orderList.getLength());
        assertFalse(orderList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(OBJECT_1, get(orderList, 1));
        assertEquals(OBJECT_2, get(orderList, 2));
        assertEquals(OBJECT_3,removed);
    }

    @Test
    void testAddLast() {
        OrderList<String> orderList1 = new OrderList<>();
        OrderList<String> orderList2 = new OrderList<>();
        String element = "Element";

        OrderList<String> orderListX = addFirst(orderList1, element);
        addLast(orderList2, element);

        assertEquals(orderListX.toString(), orderList2.toString());
    }

    @Test
    void testUnordered() {
        OrderList<String> orderList1 = new OrderList<>(null, OBJECT_1);
        OrderList<String> orderList2 = new OrderList<>(orderList1, OBJECT_2);

        assertEquals(ListDiff.TypeQuality.ERROR_ORDER,orderList2.verifyQuality());
    }

    @Test
    void testNullFirstElement() {
        OrderList<String> orderList1 = new OrderList<>(null, OBJECT_1);
        OrderList<String> orderList2 = new OrderList<>(orderList1, null);

        assertEquals(ListDiff.TypeQuality.ERROR_DEEP,orderList2.verifyQuality());
    }

    @Test
    void equalsCloneTest(){
        OrderList<String> orderList = new OrderList<>();
        addOrdered(addOrdered(addOrdered(orderList, OBJECT_4), OBJECT_2), OBJECT_6);
        OrderList<String> orderListNew = (OrderList<String>) ListBuilder.clone(orderList);

        assertEquals(orderList, orderList);
        OrderList<String> orderListNull = null;
        assertNotEquals(orderListNull, orderList);
        assertNotEquals(orderList, orderListNull);
        assertNotEquals(orderList, new Object());
        assertEquals(orderListNew, orderList);
        assertEquals(orderListNew.hashCode(), orderList.hashCode());

        orderListNew.setDeep(orderList.getDeep()+1);
        assertNotEquals(orderListNew, orderList);
        orderListNew.setDeep(orderList.getDeep());

        orderListNew.setElement(null);
        assertNotEquals(orderListNew, orderList);
        assertNotEquals(orderList, orderListNew);
        orderListNew.setElement(orderList.getElement());

        assertNotEquals(addOrdered(addOrdered(addOrdered(new OrderList<>(), OBJECT_5), OBJECT_2), OBJECT_6), orderList);
        assertNotEquals(addOrdered(addOrdered(addOrdered(new OrderList<>(), OBJECT_4), OBJECT_3), OBJECT_6), orderList);
        assertNotEquals(addOrdered(addOrdered(addOrdered(new OrderList<>(), OBJECT_4), OBJECT_2), OBJECT_7), orderList);

        assertEquals(new OrderList<>(), new OrderList<>());
    }

    @Test
    void equalsDifferentT(){
        OrderList<Integer> orderListInteger = new OrderList<>();
        orderListInteger.setElement(4);
        OrderList<String> orderListString = new OrderList<>();
        orderListString.setElement("4");

        assertNotEquals(orderListString, orderListInteger);
        assertNotEquals(orderListInteger, orderListString);
    }

    @Test
    void createIteratorNull() {
        OrderList<String> orderList = new OrderList<>();

        @NotNull QueueIterator<String> queueIterator = (QueueIterator<String>) orderList.iterator();

        assertFalse(queueIterator.hasNext());
        assertThrows(NoSuchElementException.class, queueIterator::next);
        assertFalse(queueIterator.hasPrev());
        assertThrows(NoSuchElementException.class, queueIterator::prev);
    }
    @Test
    void createIterator() {
        OrderList<String> orderList = new OrderList<>();

        addOrdered(addOrdered(orderList, OBJECT_1), OBJECT_2);

        @NotNull Iterator<String> iterator = orderList.iterator();

        assertTrue(iterator.hasNext());
        assertEquals(OBJECT_1, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(OBJECT_2, iterator.next());
        assertFalse(iterator.hasNext());
    }

    @Test
    void createIteratorBackup() {
        OrderList<String> orderList = new OrderList<>();
        addOrdered(addOrdered(orderList, OBJECT_1), OBJECT_2);
        @NotNull QueueIterator<String> queueIterator = (QueueIterator<String>) orderList.iterator();

        assertFalse(queueIterator.hasPrev());
        assertThrows(NoSuchElementException.class, queueIterator::prev);

        assertTrue(queueIterator.hasNext());
        assertEquals(OBJECT_1, queueIterator.next());

        assertTrue(queueIterator.hasPrev());
        assertEquals(OBJECT_1, queueIterator.prev());

        assertFalse(queueIterator.hasPrev());
        assertThrows(NoSuchElementException.class, queueIterator::prev);

        assertTrue(queueIterator.hasNext());
        assertEquals(OBJECT_1, queueIterator.next());
    }

    @Test
    void createIteratorStream() {
        OrderList<String> orderList = new OrderList<>();
        addOrdered(addOrdered(addOrdered(orderList, OBJECT_1), OBJECT_2), OBJECT_3);
        @NotNull QueueIterator<String> queueIterator = (QueueIterator<String>) orderList.iterator();

        List<String> list = Stream.generate(() -> null).takeWhile(x -> queueIterator.hasNext()).map(z -> queueIterator.next()).toList();
        List<String> listPrev = Stream.generate(() -> null).takeWhile(x -> queueIterator.hasPrev()).map(z -> queueIterator.prev()).toList();

        assertEquals(3, list.size());
        assertEquals(OBJECT_1, list.get(0));
        assertEquals(OBJECT_2, list.get(1));
        assertEquals(OBJECT_3, list.get(2));
        assertEquals(3, listPrev.size());
        assertEquals(OBJECT_3, listPrev.get(0));
        assertEquals(OBJECT_2, listPrev.get(1));
        assertEquals(OBJECT_1, listPrev.get(2));
    }
}

