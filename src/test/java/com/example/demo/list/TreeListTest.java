package com.example.demo.list;

import org.junit.jupiter.api.Test;

import jakarta.validation.constraints.NotNull;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static com.example.demo.list.TreeBuilder.*;
import static org.junit.jupiter.api.Assertions.*;

class TreeListTest {

    private static final String OBJECT_1 = "A-One";
    private static final String OBJECT_2 = "B-Two";
    private static final String OBJECT_3 = "C-tree";
    private static final String OBJECT_4 = "D-Four";
    private static final String OBJECT_5 = "E-Five";
    private static final String OBJECT_6 = "F-Six";
    private static final String OBJECT_7 = "G-Seven";
    private static final String OBJECT_8 = "H-Eight";
    private static final String OBJECT_9 = "I-Nine";
    private static final String OBJECT_10 = "J-Ten";

    @Test
    void testVoidComponent(){
        TreeList<String> treeList = new TreeList<>();

        assertTrue(treeList.isEmpty());
        assertEquals(0,treeList.getLength());
        assertEquals(1,treeList.getDeep());
        assertNull(get(treeList, 1));
        assertNull(getFirst(treeList));
        assertNull(getLast(treeList));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
        assertEquals("// * (1)null(0) * \\\\",treeList.toString());
        assertEquals(ListDiff.Typist.TREE_LIST, treeList.getType());
    }


    @Test
    void testVoidOtherConstructor(){
        TreeList<String> treeList = new TreeList<>(null, null, null);

        assertTrue(treeList.isEmpty());
    }

    @Test
    void testOneComponentOk(){
        TreeList<String> treeList = new TreeList<>(null, null, OBJECT_1);

        assertFalse(treeList.isEmpty());
        assertEquals(1,treeList.getLength());
        assertEquals(1,treeList.getDeep());
        assertEquals(OBJECT_1,get(treeList, 1));
        assertNull(get(treeList, 2));
        assertEquals(OBJECT_1,getFirst(treeList));
        assertEquals(OBJECT_1,getLast(treeList));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
        assertEquals("// * (1)A-One(1) * \\\\",treeList.toString());
    }

    @Test
    void testTwoComponentLeft(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_1);
        TreeList<String> treeList2 = new TreeList<>(treeList1, null, OBJECT_2);

        assertEquals(2,treeList2.getLength());
        assertEquals(2,treeList2.getDeep());
        assertFalse(treeList2.isEmpty());
        assertEquals(OBJECT_1, get(treeList2, 1));
        assertEquals(OBJECT_2, get(treeList2, 2));
        assertNull(get(treeList2, 3));
        assertEquals(OBJECT_1,getFirst(treeList2));
        assertEquals(OBJECT_2,getLast(treeList2));
        assertEquals(TreeList.TypeQuality.OK,treeList2.verifyQuality());
    }
    @Test

    void testTwoComponentRight(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_2);
        TreeList<String> treeList2 = new TreeList<>(null, treeList1, OBJECT_1);

        assertEquals(2,treeList2.getLength());
        assertEquals(2,treeList2.getDeep());
        assertFalse(treeList2.isEmpty());
        assertEquals(OBJECT_1, get(treeList2, 1));
        assertEquals(OBJECT_2, get(treeList2, 2));
        assertNull(get(treeList2, 3));
        assertEquals(OBJECT_1,getFirst(treeList2));
        assertEquals(OBJECT_2,getLast(treeList2));
        assertEquals(TreeList.TypeQuality.OK,treeList2.verifyQuality());
    }

    @Test
    void testTreeComponentS(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_1);
        TreeList<String> treeList2 = new TreeList<>(null, null, OBJECT_3);
        TreeList<String> treeList3 = new TreeList<>(treeList1, treeList2, OBJECT_2);

        assertEquals(3,treeList3.getLength());
        assertEquals(2,treeList3.getDeep());
        assertEquals(OBJECT_1, get(treeList3, 1));
        assertEquals(OBJECT_2, get(treeList3, 2));
        assertEquals(OBJECT_3, get(treeList3, 3));
        assertNull(get(treeList3, 4));
        assertEquals(OBJECT_1,getFirst(treeList3));
        assertEquals(OBJECT_3,getLast(treeList3));
        assertFalse(treeList3.isEmpty());
        assertEquals(TreeList.TypeQuality.OK,treeList3.verifyQuality());
        assertEquals("// // * (1)A-One(1) * \\\\ (2)B-Two(3) // * (1)C-tree(1) * \\\\ \\\\",treeList3.toString());
    }

    @Test
    void addOrderedOneComponentInVoid(){
        TreeList<String> treeList = new TreeList<>();

        TreeList<String>  result = addFirst(treeList, OBJECT_1);

        assertEquals(1, result.getLength());
        assertEquals(1, result.getDeep());
        assertEquals(OBJECT_1, get(result, 1));
        assertEquals(TreeList.TypeQuality.OK,result.verifyQuality());
    }

    @Test
    void addTwoComponentsAscending(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addLast(treeList, OBJECT_1), OBJECT_2);

        assertEquals(2,treeList.getLength());
        assertEquals(2,treeList.getDeep());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addTwoComponentsDescending(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1);

        assertEquals(2,treeList.getLength());
        assertEquals(2,treeList.getDeep());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals123(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addLast(treeList, OBJECT_1), OBJECT_2), OBJECT_3);

        assertEquals(3,treeList.getLength());
        assertEquals(3,treeList.getDeep());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals132(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addLast(treeList, OBJECT_1), OBJECT_3), OBJECT_2);

        assertEquals(3,treeList.getLength());
        assertEquals(3,treeList.getDeep());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals213(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addLast(treeList, OBJECT_2), OBJECT_1), OBJECT_3);

        assertEquals(3,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals231(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addLast(treeList, OBJECT_2), OBJECT_3), OBJECT_1);

        assertEquals(3,treeList.getLength());
        assertEquals(OBJECT_1,get(treeList, 1));
        assertEquals(OBJECT_2,get(treeList, 2));
        assertEquals(OBJECT_3,get(treeList, 3));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals312(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addLast(treeList, OBJECT_3), OBJECT_1), OBJECT_2);

        assertEquals(3,treeList.getLength());
        assertEquals(OBJECT_1,get(treeList, 1));
        assertEquals(OBJECT_2,get(treeList, 2));
        assertEquals(OBJECT_3,get(treeList, 3));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addTreeComponentsEquals321(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addLast(treeList, OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(3,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addFourComponentsEquals12345(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(treeList, OBJECT_1), OBJECT_2), OBJECT_3), OBJECT_4), OBJECT_5);

        assertEquals(5,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals12354(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(treeList, OBJECT_1), OBJECT_2), OBJECT_3), OBJECT_5), OBJECT_4);

        assertEquals(5,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals12435(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(treeList, OBJECT_1), OBJECT_2), OBJECT_4), OBJECT_3), OBJECT_5);

        assertEquals(5,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals12534(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(treeList, OBJECT_1), OBJECT_2), OBJECT_5), OBJECT_3), OBJECT_4);

        assertEquals(5,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals12543(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(treeList, OBJECT_1), OBJECT_2), OBJECT_5), OBJECT_4), OBJECT_3);

        assertEquals(5,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals32415(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(treeList, OBJECT_3), OBJECT_2), OBJECT_4), OBJECT_1), OBJECT_5);

        assertEquals(5,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }







    @Test
    void testVoidComponentBadDiff(){
        TreeList<String> treeList = new TreeList<>();

        treeList.setDeep(4);

        assertEquals(0,treeList.getLength());
        assertEquals(4,treeList.getDeep());
        assertTrue(treeList.isEmpty());
        assertEquals(TreeList.TypeQuality.ERROR_DEEP,treeList.verifyQuality());
    }

    @Test
    void testOneElementOkFalseDeep(){
        TreeList<String> treeList = new TreeList<>(null, null, OBJECT_1);

        treeList.setDeep(0);

        assertEquals(0,treeList.getDeep());
        assertFalse(treeList.isEmpty());
        assertEquals(TreeList.TypeQuality.ERROR_DEEP,treeList.verifyQuality());
    }

    @Test
    void testOneElementOkFalseLength(){
        TreeList<String> treeList = new TreeList<>(null, null, OBJECT_1);

        treeList.setLength(0);

        assertEquals(0,treeList.getLength());
        assertFalse(treeList.isEmpty());
        assertEquals(TreeList.TypeQuality.ERROR_LENGTH,treeList.verifyQuality());
    }

    @Test
    void testBadComponentLeft(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_1);
        TreeList<String> treeList2 = new TreeList<>(treeList1, null, null);

        assertEquals(2,treeList2.getDeep());
        assertEquals(0,treeList2.getLength());
        assertFalse(treeList2.isEmpty());
        assertEquals(TreeList.TypeQuality.ERROR_ROOT,treeList2.verifyQuality());
    }

    @Test
    void testBadComponentRight(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_1);
        TreeList<String> treeList2 = new TreeList<>(null, treeList1, null);

        assertEquals(0,treeList2.getLength());
        assertEquals(2,treeList2.getDeep());
        assertFalse(treeList2.isEmpty());
        assertEquals(TreeList.TypeQuality.ERROR_ROOT,treeList2.verifyQuality());
    }

    @Test
    void testBadComponentLeftAndRight(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_1);
        TreeList<String> treeList2 = new TreeList<>(null, null, OBJECT_2);
        TreeList<String> treeList3 = new TreeList<>(treeList1, treeList2, null);

        assertEquals(0,treeList3.getLength());
        assertEquals(2,treeList3.getDeep());
        assertFalse(treeList3.isEmpty());
        assertEquals(TreeList.TypeQuality.ERROR_ROOT,treeList3.verifyQuality());
    }

    @Test
    void testStructureLeft(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_1);
        TreeList<String> treeList2 = new TreeList<>(treeList1, null, OBJECT_2);
        TreeList<String> treeList3 = new TreeList<>(treeList2, null, OBJECT_3);

        assertEquals(TreeList.TypeQuality.OK,treeList3.verifyQuality());
    }

    @Test
    void testDeepRight(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_3);
        TreeList<String> treeList2 = new TreeList<>(null, treeList1, OBJECT_2);
        TreeList<String> treeList3 = new TreeList<>(null, treeList2, OBJECT_1);

        assertEquals(TreeList.TypeQuality.OK,treeList3.verifyQuality());
    }

    @Test
    void testStructureRightLeft(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_1);
        TreeList<String> treeList2 = new TreeList<>(treeList1, null, OBJECT_2);
        TreeList<String> treeList3 = new TreeList<>(treeList2, null, OBJECT_3);
        TreeList<String> treeList4 = new TreeList<>(null, null, OBJECT_5);
        TreeList<String> treeList5 = new TreeList<>(treeList3, treeList4, OBJECT_4);

        assertEquals(TreeList.TypeQuality.OK,treeList5.verifyQuality());
    }

    @Test
    void testBadOrderLeft(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_2);
        TreeList<String> treeList2 = new TreeList<>(treeList1, null, OBJECT_1);

        assertEquals(TreeList.TypeQuality.ERROR_ORDER,treeList2.verifyQuality());
    }

    @Test
    void testBadOrderRight(){
        TreeList<String> treeList1 = new TreeList<>(null, null, OBJECT_1);
        TreeList<String> treeList2 = new TreeList<>( null, treeList1, OBJECT_2);

        assertEquals(TreeList.TypeQuality.ERROR_ORDER,treeList2.verifyQuality());
    }

    @Test
    void testQualityLeftListBad(){
        TreeList<String> treeList1 = new TreeList<>(null, null, null);
        TreeList<String> treeList2 = new TreeList<>( treeList1, null, OBJECT_2);

        treeList1.setLength(1);
        treeList2.setLength(2);

        assertEquals(TreeList.TypeQuality.ERROR_LEFT_BRANCH,treeList2.verifyQuality());
    }

    @Test
    void testQualityRightListBad(){
        TreeList<String> treeList1 = new TreeList<>(null, null, null);
        TreeList<String> treeList2 = new TreeList<>( null, treeList1, OBJECT_2);

        treeList1.setLength(1);
        treeList2.setLength(2);

        assertEquals(TreeList.TypeQuality.ERROR_RIGHT_BRANCH,treeList2.verifyQuality());
    }

    @Test
    void addFiveComponentsEquals54321(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addLast(treeList, OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(5,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addSixComponentsEquals654321(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_6), OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(6,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(OBJECT_6, get(treeList, 6));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addSevenComponentsEquals0987654321(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_7), OBJECT_6), OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(7,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(OBJECT_6, get(treeList, 6));
        assertEquals(OBJECT_7, get(treeList, 7));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addEightComponentsEquals0987654321(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_8), OBJECT_7), OBJECT_6), OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);


        assertEquals(8,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(OBJECT_6, get(treeList, 6));
        assertEquals(OBJECT_7, get(treeList, 7));
        assertEquals(OBJECT_8, get(treeList, 8));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }


    @Test
    void addNineComponentsEquals0987654321(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_9), OBJECT_8), OBJECT_7), OBJECT_6), OBJECT_5), OBJECT_4), OBJECT_3), OBJECT_2), OBJECT_1);

        assertEquals(9,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(OBJECT_6, get(treeList, 6));
        assertEquals(OBJECT_7, get(treeList, 7));
        assertEquals(OBJECT_8, get(treeList, 8));
        assertEquals(OBJECT_9, get(treeList, 9));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }


    @Test
    void addTenComponentsEquals0987654321(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_8), OBJECT_5), OBJECT_10), OBJECT_3), OBJECT_7), OBJECT_9), OBJECT_2), OBJECT_4), OBJECT_6), OBJECT_1);

        assertEquals(10,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(OBJECT_6, get(treeList, 6));
        assertEquals(OBJECT_7, get(treeList, 7));
        assertEquals(OBJECT_8, get(treeList, 8));
        assertEquals(OBJECT_9, get(treeList, 9));
        assertEquals(OBJECT_10, get(treeList, 10));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void addTenComponentsEquals1234567890(){
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_1), OBJECT_2), OBJECT_3), OBJECT_4), OBJECT_5), OBJECT_6), OBJECT_7), OBJECT_8), OBJECT_9), OBJECT_10);

        assertEquals(10,treeList.getLength());
        assertEquals(OBJECT_1, get(treeList, 1));
        assertEquals(OBJECT_2, get(treeList, 2));
        assertEquals(OBJECT_3, get(treeList, 3));
        assertEquals(OBJECT_4, get(treeList, 4));
        assertEquals(OBJECT_5, get(treeList, 5));
        assertEquals(OBJECT_6, get(treeList, 6));
        assertEquals(OBJECT_7, get(treeList, 7));
        assertEquals(OBJECT_8, get(treeList, 8));
        assertEquals(OBJECT_9, get(treeList, 9));
        assertEquals(OBJECT_10, get(treeList, 10));
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveFirstNullTree(){
        TreeList<String> treeList = new TreeList<>();

        String removed = TreeBuilder.removeFirst(treeList);

        assertTrue(treeList.isEmpty());
        assertNull(removed);
        assertEquals(0,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveFirstOneElement(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(treeList, OBJECT_1);

        String removed = TreeBuilder.removeFirst(treeList);

        assertTrue(treeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(0,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveFirstTwoElementsBalancedLeft(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1);

        String removed = TreeBuilder.removeFirst(treeList);

        assertFalse(treeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveFirstTwoElementsBalancedRight(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_1), OBJECT_2);

        String removed = TreeBuilder.removeFirst(treeList);

        assertFalse(treeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveFirstSevenElementsOrdered(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_4), OBJECT_2), OBJECT_6), OBJECT_1), OBJECT_3), OBJECT_5), OBJECT_7);

        String removed = TreeBuilder.removeFirst(treeList);

        assertFalse(treeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(6,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveLastNullTree(){
        TreeList<String> treeList = new TreeList<>();

        String removed = TreeBuilder.removeLast(treeList);

        assertTrue(treeList.isEmpty());
        assertNull(removed);
        assertEquals(0,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveLastOneElement(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(treeList, OBJECT_1);

        String removed = TreeBuilder.removeLast(treeList);

        assertTrue(treeList.isEmpty());
        assertEquals(OBJECT_1, removed);
        assertEquals(0,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveLastTwoElementsBalancedLeft(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1);

        String removed = TreeBuilder.removeLast(treeList);

        assertFalse(treeList.isEmpty());
        assertEquals(OBJECT_2, removed);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveLastTwoElementsBalancedRight(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_1), OBJECT_2);

        String removed = TreeBuilder.removeLast(treeList);

        assertFalse(treeList.isEmpty());
        assertEquals(OBJECT_2, removed);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveLastSevenElementsLevelNotOrdered(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_5), OBJECT_2), OBJECT_6), OBJECT_1), OBJECT_3), OBJECT_4), OBJECT_7);

        String removed = TreeBuilder.removeLast(treeList);

        assertFalse(treeList.isEmpty());
        assertEquals(OBJECT_7, removed);
        assertEquals(6,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveLastSevenElementsOrdered(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(
                treeList, OBJECT_4), OBJECT_2), OBJECT_6), OBJECT_1), OBJECT_3), OBJECT_5), OBJECT_7);

        String removed = TreeBuilder.removeLast(treeList);

        assertFalse(treeList.isEmpty());
        assertEquals(OBJECT_7, removed);
        assertEquals(6,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectNotFoundNullComponent(){
        TreeList<String> treeList = new TreeList<>();

        boolean isRemoved = removeObject(treeList, OBJECT_1);

        assertFalse(isRemoved);
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectFoundOneComponent(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(treeList, OBJECT_1);

        boolean isRemoved = removeObject(treeList, OBJECT_1);

        assertTrue(isRemoved);
        assertEquals(0,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectNoFoundOneComponentGreater(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(treeList, OBJECT_1);

        boolean isRemoved = removeObject(treeList, OBJECT_2);

        assertFalse(isRemoved);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectNoFoundOneComponentLower(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(treeList, OBJECT_2);

        boolean isRemoved = removeObject(treeList, OBJECT_1);

        assertFalse(isRemoved);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectTwoComponentsFoundOneComponentHead(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_1), OBJECT_2);

        boolean isRemoved = removeObject(treeList, OBJECT_1);

        assertTrue(isRemoved);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectTwoComponentsFoundOneComponentHeadOtherBalance(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1);

        boolean isRemoved = removeObject(treeList, OBJECT_2);

        assertTrue(isRemoved);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectTwoComponentsFoundOneComponentLeftBranch(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1);

        boolean isRemoved = removeObject(treeList, OBJECT_1);

        assertTrue(isRemoved);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectTwoComponentsFoundOneComponentRightBranch(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_1), OBJECT_2);

        boolean isRemoved = removeObject(treeList, OBJECT_2);

        assertTrue(isRemoved);
        assertEquals(1,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsNotFoundToHighLeft(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(treeList, OBJECT_3), OBJECT_2), OBJECT_4);

        boolean isRemoved = removeObject(treeList, OBJECT_1);

        assertFalse(isRemoved);
        assertEquals(3,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsNotFoundToLeft(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(treeList, OBJECT_3), OBJECT_1), OBJECT_4);

        boolean isRemoved = removeObject(treeList, OBJECT_2);

        assertFalse(isRemoved);
        assertEquals(3,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsNotFoundToRight(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1), OBJECT_4);

        boolean isRemoved = removeObject(treeList, OBJECT_3);

        assertFalse(isRemoved);
        assertEquals(3,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsNotFoundToHighRight(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1), OBJECT_3);

        boolean isRemoved = removeObject(treeList, OBJECT_4);

        assertFalse(isRemoved);
        assertEquals(3,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsFoundMiddle(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1), OBJECT_3);

        boolean isRemoved = removeObject(treeList, OBJECT_2);

        assertTrue(isRemoved);
        assertEquals(2,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsFoundOnTheLeft(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1), OBJECT_3);

        boolean isRemoved = removeObject(treeList, OBJECT_1);

        assertTrue(isRemoved);
        assertEquals(2,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void testRemoveObjectThreeComponentsFoundOnTheRight(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1), OBJECT_3);

        boolean isRemoved = removeObject(treeList, OBJECT_3);

        assertTrue(isRemoved);
        assertEquals(2,treeList.getLength());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
    }

    @Test
    void equalsCloneTest(){
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(treeList, OBJECT_4), OBJECT_2), OBJECT_6);
        TreeList<String> treeListNew = (TreeList<String>) ListBuilder.clone(treeList);

        assertEquals(treeList, treeList);
        TreeList<String> treeListNull = null;
        assertNotEquals(treeListNull, treeList);
        assertNotEquals(treeList, treeListNull);
        assertNotEquals(treeList, new Object());
        assertEquals(treeListNew, treeList);
        assertEquals(treeListNew.hashCode(), treeList.hashCode());

        treeListNew.setDeep(treeList.getDeep()+1);
        assertNotEquals(treeListNew, treeList);
        treeListNew.setDeep(treeList.getDeep());

        treeListNew.setLength(treeList.getLength()+1);
        assertNotEquals(treeListNew, treeList);
        treeListNew.setLength(treeList.getLength());

        treeListNew.setElement(null);
        assertNotEquals(treeListNew, treeList);
        assertNotEquals(treeList, treeListNew);
        treeListNew.setElement(treeList.getElement());

        assertNotEquals(addOrdered(addOrdered(addOrdered(new TreeList<>(), OBJECT_5), OBJECT_2), OBJECT_6), treeList);
        assertNotEquals(addOrdered(addOrdered(addOrdered(new TreeList<>(), OBJECT_4), OBJECT_3), OBJECT_6), treeList);
        assertNotEquals(addOrdered(addOrdered(addOrdered(new TreeList<>(), OBJECT_4), OBJECT_2), OBJECT_7), treeList);

        assertEquals(new TreeList<>(), new TreeList<>());
    }

    @Test
    void equalsDifferentT(){
        TreeList<Integer> treeListInteger = new TreeList<>();
        treeListInteger.setElement(4);
        TreeList<String> treeListString = new TreeList<>();
        treeListString.setElement("4");

        assertNotEquals(treeListString, treeListInteger);
        assertNotEquals(treeListInteger, treeListString);
    }

    @Test
    void createIteratorNull() {
        TreeList<String> treeList = new TreeList<>();

        @NotNull TreeIterator<String> treeIterator = (TreeIterator<String>) treeList.iterator();

        assertFalse(treeIterator.hasNext());
        assertThrows(NoSuchElementException.class, treeIterator::next);
        assertFalse(treeIterator.hasPrev());
        assertThrows(NoSuchElementException.class, treeIterator::prev);
    }
    @Test
    void createIterator() {
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(treeList, OBJECT_1), OBJECT_2);

        @NotNull Iterator<String> iterator = treeList.iterator();

        assertTrue(iterator.hasNext());
        assertEquals(OBJECT_1, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(OBJECT_2, iterator.next());
        assertFalse(iterator.hasNext());
    }

    @Test
    void createIteratorBackup() {
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_1), OBJECT_2);
        @NotNull TreeIterator<String> treeIterator = (TreeIterator<String>) treeList.iterator();

        assertFalse(treeIterator.hasPrev());
        assertThrows(NoSuchElementException.class, treeIterator::prev);

        assertTrue(treeIterator.hasNext());
        assertEquals(OBJECT_1, treeIterator.next());

        assertTrue(treeIterator.hasPrev());
        assertEquals(OBJECT_1, treeIterator.prev());

        assertFalse(treeIterator.hasPrev());
        assertThrows(NoSuchElementException.class, treeIterator::prev);

        assertTrue(treeIterator.hasNext());
        assertEquals(OBJECT_1, treeIterator.next());
    }

    @Test
    void createIteratorStream() {
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(treeList, OBJECT_3), OBJECT_1), OBJECT_2), OBJECT_4), OBJECT_5);
        @NotNull TreeIterator<String> treeIterator = (TreeIterator<String>) treeList.iterator();

        List<String> list = Stream.generate(() -> null).takeWhile(x -> treeIterator.hasNext()).map(z -> treeIterator.next()).toList();
        List<String> listPrev = Stream.generate(() -> null).takeWhile(x -> treeIterator.hasPrev()).map(z -> treeIterator.prev()).toList();

        assertEquals(5, list.size());
        assertEquals(OBJECT_1, list.get(0));
        assertEquals(OBJECT_2, list.get(1));
        assertEquals(OBJECT_3, list.get(2));
        assertEquals(OBJECT_4, list.get(3));
        assertEquals(OBJECT_5, list.get(4));
        assertEquals(5, listPrev.size());
        assertEquals(OBJECT_5, listPrev.get(0));
        assertEquals(OBJECT_4, listPrev.get(1));
        assertEquals(OBJECT_3, listPrev.get(2));
        assertEquals(OBJECT_2, listPrev.get(3));
        assertEquals(OBJECT_1, listPrev.get(4));
    }

    @Test
    void createIteratorSoftNull() {
        TreeList<String> treeList = new TreeList<>();

        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) treeList.iteratorSoft();

        assertFalse(treeIteratorSoft.hasNext());
        assertThrows(NoSuchElementException.class, treeIteratorSoft::next);
        assertFalse(treeIteratorSoft.hasPrev());
        assertThrows(NoSuchElementException.class, treeIteratorSoft::prev);
    }
    @Test
    void createIteratorSoft() {
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(treeList, OBJECT_1), OBJECT_2);

        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) treeList.iteratorSoft();

        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_1, treeIteratorSoft.next());
        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_2, treeIteratorSoft.next());
        assertFalse(treeIteratorSoft.hasNext());
    }

    @Test
    void createIteratorOtherOrderSoft() {
        TreeList<String> treeList = new TreeList<>();

        addOrdered(addOrdered(treeList, OBJECT_2), OBJECT_1);

        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) treeList.iteratorSoft();

        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_2, treeIteratorSoft.next());
        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_1, treeIteratorSoft.next());
        assertFalse(treeIteratorSoft.hasNext());
    }

    @Test
    void createIteratorSoftBackup() {
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(treeList, OBJECT_1), OBJECT_2);
        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) treeList.iteratorSoft();

        assertFalse(treeIteratorSoft.hasPrev());
        assertThrows(NoSuchElementException.class, treeIteratorSoft::prev);

        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_1, treeIteratorSoft.next());

        assertTrue(treeIteratorSoft.hasPrev());
        assertEquals(OBJECT_1, treeIteratorSoft.prev());

        assertFalse(treeIteratorSoft.hasPrev());
        assertThrows(NoSuchElementException.class, treeIteratorSoft::prev);

        assertTrue(treeIteratorSoft.hasNext());
        assertEquals(OBJECT_1, treeIteratorSoft.next());
    }

    @Test
    void createIteratorSoftStream() {
        TreeList<String> treeList = new TreeList<>();
        addOrdered(addOrdered(addOrdered(addOrdered(addOrdered(treeList, OBJECT_3), OBJECT_1), OBJECT_2), OBJECT_4), OBJECT_5);
        @NotNull TreeIteratorSoft<String> treeIteratorSoft = (TreeIteratorSoft<String>) treeList.iteratorSoft();

        List<String> list = Stream.generate(() -> null).takeWhile(x -> treeIteratorSoft.hasNext()).map(z -> treeIteratorSoft.next()).toList();
        List<String> listPrev = Stream.generate(() -> null).takeWhile(x -> treeIteratorSoft.hasPrev()).map(z -> treeIteratorSoft.prev()).toList();

        assertEquals(5, list.size());
        assertEquals(OBJECT_3, list.get(0));
        assertEquals(OBJECT_1, list.get(1));
        assertEquals(OBJECT_4, list.get(2));
        assertEquals(OBJECT_2, list.get(3));
        assertEquals(OBJECT_5, list.get(4));
        assertEquals(5, listPrev.size());
        assertEquals(OBJECT_5, listPrev.get(0));
        assertEquals(OBJECT_2, listPrev.get(1));
        assertEquals(OBJECT_4, listPrev.get(2));
        assertEquals(OBJECT_1, listPrev.get(3));
        assertEquals(OBJECT_3, listPrev.get(4));
    }
}



