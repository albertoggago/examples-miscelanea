package com.example.demo.list;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TimeControlTest {
    private final int times = 300;
    private final int max = 1000000;

    @Test
    void TimeControlInOrderTest() {
        Date dateStart = new Date();
        OrderList<Integer> orderList = new OrderList<>();
        IntStream.rangeClosed(1,times).forEach(ele -> OrderBuilder.addOrdered(orderList, ele));
        Date dateEnd = new Date();

        long diff1 = dateEnd.getTime() - dateStart.getTime();
        long millisecondsOrderList = TimeUnit.MILLISECONDS.toMillis(diff1);

        Date dateStart2 = new Date();
        TreeList<Integer> treeList = new TreeList<>();
        IntStream.rangeClosed(1,times).forEach(ele -> TreeBuilder.addOrdered(treeList, ele));
        Date dateEnd2 = new Date();

        long diff2 = dateEnd2.getTime() - dateStart2.getTime();
        long millisecondsTreeList = TimeUnit.MILLISECONDS.toMillis(diff2);

        Date dateStart3 = new Date();
        BalancedTreeList<Integer> balancedTreeList = new BalancedTreeList<>();
        IntStream.rangeClosed(1,times).forEach(ele -> BalancedTreeBuilder.addOrdered(balancedTreeList, ele));
        Date dateEnd3 = new Date();

        long diff3 = dateEnd3.getTime() - dateStart3.getTime();
        long millisecondsBalancedTreeList = TimeUnit.MILLISECONDS.toMillis(diff3);

        System.out.println("======================================================");
        System.out.println("========= SEQUENTIAL PROCESS =======");
        System.out.println("Milliseconds used OrderedList:      "+millisecondsOrderList);
        System.out.println("Milliseconds used TreeList:         "+millisecondsTreeList);
        System.out.println("Milliseconds used BalancedTreeList: "+millisecondsBalancedTreeList);

        assertEquals(times, orderList.getLength());
        assertEquals(times, treeList.getLength());
        assertEquals(times, balancedTreeList.getLength());
        assertEquals(OrderList.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
        assertEquals(TreeList.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void TimeControlInUnorderedTest() {

        Date dateStart1 = new Date();
        OrderList<Integer> orderList = new OrderList<>();
        Random r = new Random();

        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> OrderBuilder.addOrdered(orderList, ele));
        Date dateEnd1 = new Date();

        long diff1 = dateEnd1.getTime() - dateStart1.getTime();
        long milliSecondsOrderList = TimeUnit.MILLISECONDS.toMillis(diff1);

        Date dateStart2 = new Date();
        TreeList<Integer> treeList = new TreeList<>();
        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> TreeBuilder.addOrdered(treeList, ele));
        Date dateEnd2 = new Date();

        long diff2 = dateEnd2.getTime() - dateStart2.getTime();
        long millisecondsTreeList = TimeUnit.MILLISECONDS.toMillis(diff2);

        Date dateStart3 = new Date();
        BalancedTreeList<Integer> balancedTreeList = new BalancedTreeList<>();
        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> BalancedTreeBuilder.addOrdered(balancedTreeList, ele));
        Date dateEnd3 = new Date();

        long diff3 = dateEnd3.getTime() - dateStart3.getTime();
        long millisecondsBalancedTreeList = TimeUnit.MILLISECONDS.toMillis(diff3);

        System.out.println("======================================================");
        System.out.println("========= RANDOM PROCESS =======");
        System.out.println("Milliseconds used OrderedList:      "+milliSecondsOrderList);
        System.out.println("Milliseconds used TreeList:         "+millisecondsTreeList);
        System.out.println("Milliseconds used BalancedTreeList: "+millisecondsBalancedTreeList);

        assertEquals(times, orderList.getLength());
        assertEquals(times, treeList.getLength());
        assertEquals(times, balancedTreeList.getLength());
        assertEquals(OrderList.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
        assertEquals(TreeList.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void TimeControlSearch() {

        long timesSearch = times * 10;
        Random r = new Random();

        OrderList<Integer> orderList = new OrderList<>();
        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> OrderBuilder.addOrdered(orderList, ele));

        Date dateStart1 = new Date();
        IntStream.generate(() -> r.nextInt(max)).limit(timesSearch).forEach(ele -> OrderBuilder.get(orderList, ele));
        Date dateEnd1 = new Date();

        long diff1 = dateEnd1.getTime() - dateStart1.getTime();
        long milliSecondsOrderList = TimeUnit.MILLISECONDS.toMillis(diff1);

        TreeList<Integer> treeList = new TreeList<>();
        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> TreeBuilder.addOrdered(treeList, ele));

        Date dateStart2 = new Date();
        IntStream.generate(() -> r.nextInt(max)).limit(timesSearch).forEach(ele -> TreeBuilder.get(treeList, ele));
        Date dateEnd2 = new Date();

        long diff2 = dateEnd2.getTime() - dateStart2.getTime();
        long millisecondsTreeList = TimeUnit.MILLISECONDS.toMillis(diff2);

        BalancedTreeList<Integer> balancedTreeList = new BalancedTreeList<>();
        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> BalancedTreeBuilder.addOrdered(balancedTreeList, ele));

        Date dateStart3 = new Date();
        IntStream.generate(() -> r.nextInt(max)).limit(timesSearch).forEach(ele -> BalancedTreeBuilder.get(balancedTreeList, ele));
        Date dateEnd3 = new Date();

        long diff3 = dateEnd3.getTime() - dateStart3.getTime();
        long millisecondsBalancedTreeList = TimeUnit.MILLISECONDS.toMillis(diff3);

        System.out.println("======================================================");
        System.out.println("========= SEARCH PROCESS =======");
        System.out.println("Milliseconds used OrderedList:      "+milliSecondsOrderList);
        System.out.println("Milliseconds used TreeList:         "+millisecondsTreeList);
        System.out.println("Milliseconds used BalancedTreeList: "+millisecondsBalancedTreeList);

        assertEquals(times, orderList.getLength());
        assertEquals(times, treeList.getLength());
        assertEquals(times, balancedTreeList.getLength());
        assertEquals(OrderList.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
        assertEquals(TreeList.TypeQuality.OK,balancedTreeList.verifyQuality());
    }

    @Test
    void TimeControlRemovePosition() {

        long removeElements = times / 2;
        Random r = new Random();

        OrderList<Integer> orderList = new OrderList<>();
        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> OrderBuilder.addOrdered(orderList, ele));

        Date dateStart1 = new Date();
        IntStream.generate(() -> r.nextInt(max)).limit(removeElements).forEach(orderList::removeObject);
        Date dateEnd1 = new Date();

        long diff1 = dateEnd1.getTime() - dateStart1.getTime();
        long milliSecondsOrderList = TimeUnit.MILLISECONDS.toMillis(diff1);

        TreeList<Integer> treeList = new TreeList<>();
        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> TreeBuilder.addOrdered(treeList, ele));

        Date dateStart2 = new Date();
        IntStream.generate(() -> r.nextInt(max)).limit(removeElements).forEach(ele -> TreeBuilder.removeObject(treeList, ele));
        Date dateEnd2 = new Date();

        long diff2 = dateEnd2.getTime() - dateStart2.getTime();
        long millisecondsTreeList = TimeUnit.MILLISECONDS.toMillis(diff2);

        BalancedTreeList<Integer> balancedTreeList = new BalancedTreeList<>();
        IntStream.generate(() -> r.nextInt(max)).limit(times).forEach(ele -> BalancedTreeBuilder.addOrdered(balancedTreeList, ele));

        Date dateStart3 = new Date();
        IntStream.generate(() -> r.nextInt(max)).limit(removeElements).forEach(ele -> BalancedTreeBuilder.removeObject(balancedTreeList, ele));
        Date dateEnd3 = new Date();

        long diff3 = dateEnd3.getTime() - dateStart3.getTime();
        long millisecondsBalancedTreeList = TimeUnit.MILLISECONDS.toMillis(diff3);

        System.out.println("======================================================");
        System.out.println("========= REMOVE PROCESS =======");
        System.out.println("Milliseconds used OrderedList:      "+milliSecondsOrderList);
        System.out.println("Milliseconds used TreeList:         "+millisecondsTreeList);
        System.out.println("Milliseconds used BalancedTreeList: "+millisecondsBalancedTreeList);

        assertEquals(OrderList.TypeQuality.OK,orderList.verifyQuality());
        assertEquals(TreeList.TypeQuality.OK,treeList.verifyQuality());
        assertEquals(TreeList.TypeQuality.OK,balancedTreeList.verifyQuality());
    }


}

