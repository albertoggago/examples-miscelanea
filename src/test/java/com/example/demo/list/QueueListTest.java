package com.example.demo.list;


import org.junit.jupiter.api.Test;

import jakarta.validation.constraints.NotNull;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static com.example.demo.list.QueueBuilder.*;
import static org.junit.jupiter.api.Assertions.*;

class QueueListTest {

    private static final String OBJECT_1 = "A-One";
    private static final String OBJECT_2 = "B-Two";
    private static final String OBJECT_3 = "C-tree";
    private static final String OBJECT_4 = "D-Four";
    private static final String OBJECT_5 = "E-Five";
    private static final String OBJECT_6 = "F-Six";
    private static final String OBJECT_7 = "G-Seven";

    @Test
    void testCreateQueue (){
        QueueList<String> queueList = new QueueList<>();

        assertEquals(0L,queueList.getLength());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertTrue(queueList.isEmpty());
        assertEquals(ListDiff.Typist.QUEUE_LIST, queueList.getType());
    }

    @Test
    void testAddFirstElement() {
        QueueList<String> queueList = new QueueList<>();

        addFirst(queueList , OBJECT_1);

        System.out.println(queueList);

        assertEquals(1L,queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,queueList.verifyQuality());
        assertEquals(OBJECT_1, getFirst(queueList));

    }

    @Test
    void testAddFirstFirstElement() {
        QueueList<String> queueList = new QueueList<>();

        addFirst(addFirst(queueList, OBJECT_1), OBJECT_2);

        assertEquals(2L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_2, getFirst(queueList));
        assertEquals(OBJECT_1, getLast(queueList));

    }

    @Test
    void testAddFirstFirstFirstElement() {
        QueueList<String> queueList = new QueueList<>();

        addFirst(addFirst(addFirst(queueList, OBJECT_1), OBJECT_2), OBJECT_3);

        assertEquals(3L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_3, getFirst(queueList));
        assertEquals(OBJECT_2, get(queueList, 2));
        assertEquals(OBJECT_1, get(queueList, 3));
        assertNull(get(queueList, -1));
        assertNull(get(queueList, 4));
        assertEquals(OBJECT_1, getLast(queueList));

    }

    @Test
    void testAddLastElement() {
        QueueList<String> queueList = new QueueList<>();

        addLast(queueList, OBJECT_1);

        assertEquals(1L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,queueList.verifyQuality());
        assertEquals(OBJECT_1, getFirst(queueList));

    }

    @Test
    void testAddLastLastElement() {
        QueueList<String> queueList = new QueueList<>();

        addLast(addLast(queueList, OBJECT_1), OBJECT_2);

        assertEquals(2L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,queueList.verifyQuality());
        assertEquals(OBJECT_1, getFirst(queueList));
        assertEquals(OBJECT_2, get(queueList, 2));
    }

    @Test
    void testAddLastLastLastElement() {
        QueueList<String> queueList = new QueueList<>();

        addLast(addLast(addLast(queueList, OBJECT_1), OBJECT_2), OBJECT_3);

        assertEquals(3L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_1, getFirst(queueList));
        assertEquals(OBJECT_2, get(queueList, 2));
        assertEquals(OBJECT_3, get(queueList, 3));
    }

    @Test
    void testAddLastFirstLastElement() {
        QueueList<String> queueList = new QueueList<>();

        addLast(addFirst(addLast(queueList, OBJECT_1), OBJECT_2), OBJECT_3);

        assertEquals(3L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_2, getFirst(queueList));
        assertEquals(OBJECT_1, get(queueList, 2));
        assertEquals(OBJECT_3, get(queueList, 3));
    }

    @Test
    void testAddFirstLastFirstElement() {
        QueueList<String> queueList = new QueueList<>();

        addFirst(addLast(addFirst(queueList, OBJECT_1), OBJECT_2), OBJECT_3);


        assertEquals(3L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_3, getFirst(queueList));
        assertEquals(OBJECT_1, get(queueList, 2));
        assertEquals(OBJECT_2, get(queueList, 3));
    }

    @Test
    void testAddNothingRemoveFirst() {
        QueueList<String> queueList = new QueueList<>();

        Object removed = QueueBuilder.removeFirst(queueList);

        assertEquals(0L, queueList.getLength());
        assertTrue(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertNull(removed);
    }

    @Test
    void testAddOneRemoveFirst() {
        QueueList<String> queueList = new QueueList<>();

        Object removed = removeLast(addLast(queueList, OBJECT_1));

        assertEquals(0L, queueList.getLength());
        assertTrue(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_1, removed);
    }

    @Test
    void testAddTwoRemoveFirst() {
        QueueList<String> queueList = new QueueList<>();

        Object removed = removeFirst(addLast(addLast(queueList, OBJECT_1), OBJECT_2));

        assertEquals(1L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_2, get(queueList, 1));
        assertEquals(OBJECT_1, removed);
    }

    @Test
    void testAddTreeRemoveFirst() {
        QueueList<String> queueList = new QueueList<>();

        Object removed = removeFirst(addLast(addLast(addLast(queueList, OBJECT_1), OBJECT_2), OBJECT_3));

        assertEquals(2L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_2, get(queueList, 1));
        assertEquals(OBJECT_3, get(queueList, 2));
        assertEquals(OBJECT_1, removed);
    }


    @Test
    void testAddNothingRemoveLast() {
        QueueList<String> queueList = new QueueList<>();

        Object removed = removeLast(queueList);

        assertEquals(0L, queueList.getLength());
        assertTrue(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertNull(removed);
    }
    

    @Test
    void testAddTwoRemoveLast() {
        QueueList<String> queueList = new QueueList<>();

        Object removed = removeLast(addLast(addLast(queueList, OBJECT_1), OBJECT_2));

        assertEquals(1L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_1, get(queueList, 1));
        assertEquals(OBJECT_2, removed);
    }

    @Test
    void testAddTreeRemoveLast() {
        QueueList<String> queueList = new QueueList<>();

        Object removed = removeLast(addLast(addLast(addLast(queueList, OBJECT_1), OBJECT_2), OBJECT_3));

        assertEquals(2L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_1, get(queueList, 1));
        assertEquals(OBJECT_2, get(queueList, 2));
        assertEquals(OBJECT_3, removed);
    }

    @Test
    void testAddNothingRemoveObject() {
        QueueList<String> queueList = new QueueList<>();


        boolean removed = queueList.removeObject(OBJECT_1);

        assertEquals(0L, queueList.getLength());
        assertTrue(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK,queueList.verifyQuality());
        assertFalse(removed);
    }

    @Test
    void addOneRemoveObject() {
        QueueList<String> queueList = new QueueList<>();

        boolean removed = addLast(queueList, OBJECT_1).removeObject(OBJECT_1);

        assertEquals(0L, queueList.getLength());
        assertTrue(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertTrue(removed);
    }

    @Test
    void testAddOneRemoveOtherObject() {
        QueueList<String> queueList = new QueueList<>();

        boolean removed = addLast(queueList, OBJECT_1).removeObject(OBJECT_2);

        assertEquals(1L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_1, get(queueList, 1));
        assertFalse(removed);
    }

    @Test
    void testAddTwoRemoveObject() {
        QueueList<String> queueList = new QueueList<>();

        boolean removed = addLast(addLast(queueList, OBJECT_1), OBJECT_2).removeObject(OBJECT_1);

        assertEquals(1L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_2, get(queueList, 1));
        assertTrue(removed);
    }

    @Test
    void testAddTwoRemoveOtherObject() {
        QueueList<String> queueList = new QueueList<>();

        boolean removed = addLast(addLast(queueList, OBJECT_1), OBJECT_2).removeObject(OBJECT_3);

        assertEquals(2L, queueList.getLength());
        assertFalse(queueList.isEmpty());
        assertEquals(ListDiff.TypeQuality.OK, queueList.verifyQuality());
        assertEquals(OBJECT_1, get(queueList, 1));
        assertEquals(OBJECT_2, get(queueList, 2));
        assertFalse(removed);
    }

    @Test
    void testCreationVoid(){
        QueueList<String> queueList1 = new QueueList<>(null, null);

        assertEquals(0L,queueList1.getDeep());
        assertEquals(ListDiff.TypeQuality.OK, queueList1.verifyQuality());
        assertTrue(queueList1.isEmpty());
        assertNull(get(queueList1, 2));
        assertEquals("Number 0, No Element",queueList1.toString());
    }

    @Test
    void testCreationLength1(){
        QueueList<String> queueList1 = new QueueList<>(null, OBJECT_1);

        assertEquals(1L,queueList1.getDeep());
        assertEquals(ListDiff.TypeQuality.OK,queueList1.verifyQuality());
        assertFalse(queueList1.isEmpty());
        assertEquals("Number 1, Element A-One",queueList1.toString());
    }

    @Test
    void testCreationLength2(){
        QueueList<String> queueList1 = new QueueList<>(null, OBJECT_1);
        QueueList<String> queueList2 = new QueueList<>(queueList1, OBJECT_2);

        assertEquals(2L,queueList2.getDeep());
        assertEquals(ListDiff.TypeQuality.OK,queueList1.verifyQuality());
        assertFalse(queueList1.isEmpty());
        assertFalse(queueList2.isEmpty());
        assertEquals("Number 2, Element B-Two; Number 1, Element A-One",queueList2.toString());
    }

    @Test
    void  testCreationWeird(){
        QueueList<String> queueList1 = new QueueList<>(null, OBJECT_1);
        QueueList<String> queueList2 = new QueueList<>(queueList1, null);

        assertFalse(queueList2.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_DEEP,queueList2.verifyQuality());

    }

    @Test
    void  testCreationWrongDeepOnlyOneElement(){
        QueueList<String> queueList1 = new QueueList<>(null, OBJECT_1);
        queueList1.setDeep(2);

        assertFalse(queueList1.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_DEEP,queueList1.verifyQuality());
    }

    @Test
    void  testCreationWrongDeepOnlyTwoElements(){
        QueueList<String> queueList1 = new QueueList<>(null, OBJECT_1);
        QueueList<String> queueList2 = new QueueList<>(queueList1, OBJECT_2);
        queueList2.setDeep(99);

        assertEquals(ListDiff.TypeQuality.ERROR_DEEP,queueList2.verifyQuality());
    }

    @Test
    void  testCreationTreeElementsWrongDeepErrorThird(){
        QueueList<String> queueList1 = new QueueList<>(null, OBJECT_1);
        QueueList<String> queueList2 = new QueueList<>(queueList1, OBJECT_2);
        QueueList<String> queueList3 = new QueueList<>(queueList2, OBJECT_3);
        queueList2.setDeep(99);

        assertEquals(ListDiff.TypeQuality.ERROR_DEEP,queueList3.verifyQuality());
    }

    @Test
    void  testCreationWrongTwoElements(){
        QueueList<String> queueList1 = new QueueList<>(null, OBJECT_1);
        QueueList<String> queueList2 = new QueueList<>(queueList1, null);
        queueList2.setDeep(2);

        assertEquals(ListDiff.TypeQuality.OK,queueList2.verifyQuality());
    }

    @Test
    void testAddOrdered(){
        QueueList<String> queueList1 = new QueueList<>();

        QueueList<String> result = addOrdered(queueList1, OBJECT_1);

        assertFalse(result.isEmpty());
    }

    @Test
    void testQualityNextElementElementNull(){
        QueueList<String> queueList1 = new QueueList<>(null, null);
        QueueList<String> queueList2 = new QueueList<>(queueList1, OBJECT_1);
        queueList1.setDeep(0);
        queueList2.setDeep(1);

        assertEquals(ListDiff.TypeQuality.OK,queueList2.verifyQuality());

    }

    @Test
    void testRemoveFailureWhenLongComponent(){
        QueueList<String> queueList = new QueueList<>();
        addLast(addLast(addLast(queueList, OBJECT_1), OBJECT_2), OBJECT_3);

        assertFalse(queueList.removeObject(OBJECT_4));

    }

    @Test
    void testRemoveOKWhenLongComponent(){
        QueueList<String> queueList = new QueueList<>();
        addLast(addLast(addLast(queueList, OBJECT_1), OBJECT_2), OBJECT_3);

        assertTrue(queueList.removeObject(OBJECT_3));

    }

    @Test
    void testLeftElement() {
        QueueList<String> queueList1 = new QueueList<>();
        QueueList<String> queueList2 = new QueueList<>();


        addFirst(queueList1, OBJECT_1);
        queueList2.setLeftElement(queueList1);
        queueList2.setElement(OBJECT_2);

        assertEquals(0L, queueList2.getLength());
        assertFalse(queueList2.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_DEEP, queueList2.verifyQuality());
        assertEquals(OBJECT_2, getFirst(queueList2));
        assertEquals(OBJECT_1, get(queueList2, 2L));
    }

    @Test
    void testRightElement() {
        QueueList<String> queueList1 = new QueueList<>();
        QueueList<String> queueList2 = new QueueList<>();


        addFirst(queueList1, OBJECT_1);
        queueList2.setRightElement(queueList1);
        queueList2.setElement(OBJECT_2);

        assertEquals(0L, queueList2.getLength());
        assertFalse(queueList2.isEmpty());
        assertEquals(ListDiff.TypeQuality.ERROR_DEEP, queueList2.verifyQuality());
        assertEquals(OBJECT_2, getFirst(queueList2));
        assertEquals(OBJECT_1, get(queueList2, 2L));
    }

    @Test
    void equalsCloneTest(){
        QueueList<String> queueList = new QueueList<>();
        addOrdered(addOrdered(addOrdered(queueList, OBJECT_4), OBJECT_2), OBJECT_6);
        QueueList<String> queueListNew = (QueueList<String>) ListBuilder.clone(queueList);

        assertEquals(queueList, queueList);
        QueueList<String> queueListNull = null;
        assertNotEquals(queueListNull, queueList);
        assertNotEquals(queueList, queueListNull);
        assertNotEquals(queueList, new Object());
        assertEquals(queueListNew, queueList);
        assertEquals(queueListNew.hashCode(), queueList.hashCode());

        queueListNew.setDeep(queueList.getDeep()+1);
        assertNotEquals(queueListNew, queueList);
        queueListNew.setDeep(queueList.getDeep());

        queueListNew.setElement(null);
        assertNotEquals(queueListNew, queueList);
        assertNotEquals(queueList, queueListNew);
        queueListNew.setElement(queueList.getElement());

        assertNotEquals(addOrdered(addOrdered(addOrdered(new QueueList<>(), OBJECT_5), OBJECT_2), OBJECT_6), queueList);
        assertNotEquals(addOrdered(addOrdered(addOrdered(new QueueList<>(), OBJECT_4), OBJECT_3), OBJECT_6), queueList);
        assertNotEquals(addOrdered(addOrdered(addOrdered(new QueueList<>(), OBJECT_4), OBJECT_2), OBJECT_7), queueList);

        assertEquals(new QueueList<>(), new QueueList<>());
    }

    @Test
    void equalsDifferentT(){
        QueueList<Integer> queueListInteger = new QueueList<>();
        queueListInteger.setElement(4);
        QueueList<String> queueListString = new QueueList<>();
        queueListString.setElement("4");

        assertNotEquals(queueListString, queueListInteger);
        assertNotEquals(queueListInteger, queueListString);
    }

    @Test
    void equalsCompareNull(){
        ListDiff<String> queueList = ListBuilder.clone(null);

        assertNull(queueList);
    }

    @Test
    void createIteratorNull() {
        QueueList<String> queueList = new QueueList<>();

        @NotNull QueueIterator<String> queueListIterator = (QueueIterator<String>) queueList.iterator();

        assertFalse(queueListIterator.hasNext());
        assertThrows(NoSuchElementException.class, queueListIterator::next);
        assertFalse(queueListIterator.hasPrev());
        assertThrows(NoSuchElementException.class, queueListIterator::prev);
    }
    @Test
    void createIterator() {
        QueueList<String> queueList = new QueueList<>();

        addOrdered(addOrdered(queueList, OBJECT_1), OBJECT_2);

        @NotNull Iterator<String> queueListIterator = queueList.iterator();

        assertTrue(queueListIterator.hasNext());
        assertEquals(OBJECT_1, queueListIterator.next());
        assertTrue(queueListIterator.hasNext());
        assertEquals(OBJECT_2, queueListIterator.next());
        assertFalse(queueListIterator.hasNext());
    }

    @Test
    void createIteratorBackup() {
        QueueList<String> queueList = new QueueList<>();

        addOrdered(addOrdered(queueList, OBJECT_1), OBJECT_2);

        @NotNull QueueIterator<String> queueListIterator = (QueueIterator<String>) queueList.iterator();

        assertFalse(queueListIterator.hasPrev());
        assertThrows(NoSuchElementException.class, queueListIterator::prev);

        assertTrue(queueListIterator.hasNext());
        assertEquals(OBJECT_1, queueListIterator.next());

        assertTrue(queueListIterator.hasPrev());
        assertEquals(OBJECT_1, queueListIterator.prev());

        assertFalse(queueListIterator.hasPrev());
        assertThrows(NoSuchElementException.class, queueListIterator::prev);

        assertTrue(queueListIterator.hasNext());
        assertEquals(OBJECT_1, queueListIterator.next());
    }

    @Test
    void createIteratorStream() {
        QueueList<String> queueList = new QueueList<>();

        addOrdered(addOrdered(addOrdered(queueList, OBJECT_1), OBJECT_2), OBJECT_3);

        @NotNull QueueIterator<String> iterator = (QueueIterator<String>) queueList.iterator();

        List<String> list = Stream.generate(() -> null).takeWhile(x -> iterator.hasNext()).map(z -> iterator.next()).toList();
        List<String> listPrev = Stream.generate(() -> null).takeWhile(x -> iterator.hasPrev()).map(z -> iterator.prev()).toList();

        assertEquals(3, list.size());
        assertEquals(OBJECT_1, list.get(0));
        assertEquals(OBJECT_2, list.get(1));
        assertEquals(OBJECT_3, list.get(2));
        assertEquals(3, listPrev.size());
        assertEquals(OBJECT_3, listPrev.get(0));
        assertEquals(OBJECT_2, listPrev.get(1));
        assertEquals(OBJECT_1, listPrev.get(2));
    }
}

