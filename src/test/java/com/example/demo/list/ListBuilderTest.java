package com.example.demo.list;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
class ListBuilderTest {

    @Test
    void cloneCreateNull() {

        ListDiff<String> result = ListBuilder.clone(null);
        assertNull(result);
    }

    @Test
    void cloneCreateTypeNull() {
    	ListDiff<String> anyList = new ListDiffMock<>();

        ListDiff<String> result = ListBuilder.clone(anyList);
        assertNull(result);
    }

    @Test
    void cloneCreateTypeOrderedList() {
    	ListDiff<String> anyList = new OrderList<>();

        ListDiff<String> result = ListBuilder.clone(anyList);
        assertEquals("OrderList", result.getClass().getSimpleName());
    }

    @Test
    void cloneCreateTypeQueueList() {
    	ListDiff<String> anyList = new QueueList<String>();        

        ListDiff<String> result = ListBuilder.clone(anyList);
        assertEquals("QueueList", result.getClass().getSimpleName());
    }

    @Test
    void cloneCreateTypeTreeList() {
    	ListDiff<String> anyList = new TreeList<String>();        

        ListDiff<String> result = ListBuilder.clone(anyList);
        assertEquals("TreeList", result.getClass().getSimpleName());
    }

    @Test
    void cloneCreateTypeBalancedTreeList() {
    	ListDiff<String> anyList = new BalancedTreeList<String>();        

        ListDiff<String> result = ListBuilder.clone(anyList);
        assertEquals("BalancedTreeList", result.getClass().getSimpleName());
    }
}