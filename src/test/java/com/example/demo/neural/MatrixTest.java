package com.example.demo.neural;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {
    private static final int NUM_ROWS = 9;
    private static final int NUM_COLS = 10;
    private static final double SCALAR = 4.0;


    @Test
    void creationTest() throws DataException {
        Matrix matrix = new Matrix(NUM_ROWS, NUM_COLS);

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertTrue(between(matrix.getData()[i][j]));
            }
        }
        assertEquals(NUM_ROWS, matrix.getRows());
        assertEquals(NUM_COLS, matrix.getCols());


    }

    private boolean between(double v) {
        double minTable = -1;
        double maxTable = 1;
        return (v > minTable && v < maxTable);
    }

    @Test
    void creationErrorTest() {
        assertThrows(DataException.class, () -> new Matrix(0, 0));
        assertThrows(DataException.class, () -> new Matrix(0, 1));
        assertThrows(DataException.class, () -> new Matrix(1, 0));
    }

    @Test
    void cloneTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(matrix1);

        assertEquals(matrix1.getCols(), matrix2.getCols());
        assertEquals(matrix1.getRows(), matrix2.getRows());

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertEquals(matrix1.getData()[i][j], matrix2.getData()[i][j]);
            }
        }
    }

    @Test
    void addTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix3 = new Matrix(matrix1);

        matrix3.add(matrix2);

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertEquals(matrix3.getData()[i][j], matrix1.getData()[i][j] + matrix2.getData()[i][j]);
            }
        }
    }

    @Test
    void addStaticTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(NUM_ROWS, NUM_COLS);

        Matrix matrix3 = Matrix.add(matrix1, matrix2);

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertEquals(matrix3.getData()[i][j], matrix1.getData()[i][j] + matrix2.getData()[i][j]);
            }
        }
    }

    @Test
    void addScalarTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(matrix1);


        matrix2.add(4.0);

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertEquals(matrix2.getData()[i][j], matrix1.getData()[i][j] + SCALAR);
            }
        }
    }

    @Test
    void addErrorSizeTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(NUM_ROWS, NUM_COLS + 1);
        Matrix matrix3 = new Matrix(NUM_ROWS + 1, NUM_COLS);

        assertThrows(DataException.class, () -> matrix1.add(matrix2));
        assertThrows(DataException.class, () -> matrix1.add(matrix3));
    }


    @Test
    void subtractTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix3 = new Matrix(matrix1);

        matrix3.subtract(matrix2);

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertEquals(matrix3.getData()[i][j], matrix1.getData()[i][j] - matrix2.getData()[i][j]);
            }
        }
    }

    @Test
    void subtractStaticTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(NUM_ROWS, NUM_COLS);

        Matrix matrix3 = Matrix.subtract(matrix1, matrix2);

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertEquals(matrix3.getData()[i][j], matrix1.getData()[i][j] - matrix2.getData()[i][j]);
            }
        }
    }

    @Test
    void subtractScalarTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(matrix1);
        var scalar = 4.0;

        matrix2.subtract(4.0);

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertEquals(matrix2.getData()[i][j], matrix1.getData()[i][j] - scalar);
            }
        }
    }


    @Test
    void transposeTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);

        Matrix matrix2 = Matrix.transpose(matrix1);

        assertEquals(matrix1.getRows(), matrix2.getCols());
        assertEquals(matrix2.getRows(), matrix1.getCols());

        for (var i = 0; i < NUM_ROWS; i++) {
            for (var j = 0; j < NUM_COLS; j++) {
                assertEquals(matrix1.getData()[i][j], matrix2.getData()[j][i]);
            }
        }
    }

    @Test
    void multiplyStaticTest() throws DataException {
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(2, 4);

        Matrix matrix3 = Matrix.multiply(matrix1, matrix2);

        assertEquals(3, matrix3.getRows());
        assertEquals(4, matrix3.getCols());
        for (var i = 0; i < 3; i++) {
            for (var j = 0; j < 4; j++) {
                assertEquals(matrix1.getData()[i][0] * matrix2.getData()[0][j] + matrix1.getData()[i][1] * matrix2.getData()[1][j], matrix3.getData()[i][j]);
            }
        }
    }

    @Test
    void multiplyFailure() throws DataException {
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(5, 4);

        assertThrows(DataException.class, () -> Matrix.multiply(matrix1, matrix2));

    }

    @Test
    void multiplyTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix3 = new Matrix(matrix1);

        matrix3.multiply(matrix2);

        assertEquals(matrix1.getRows(), matrix3.getRows());
        assertEquals(matrix1.getCols(), matrix3.getCols());

        for (var i = 0; i < matrix3.getRows(); i++) {
            for (var j = 0; j < matrix3.getCols(); j++) {
                assertEquals(matrix1.getData()[i][j] * matrix2.getData()[i][j], matrix3.getData()[i][j]);
            }
        }
    }

    @Test
    void multiplyScalarTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(matrix1);

        matrix2.multiply(SCALAR);

        for (var i = 0; i < matrix2.getRows(); i++) {
            for (var j = 0; j < matrix2.getCols(); j++) {
                assertEquals(matrix1.getData()[i][j] * SCALAR, matrix2.getData()[i][j]);
            }
        }
    }

    @Test
    void sigmoidTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);
        Matrix matrix2 = new Matrix(matrix1);

        matrix2.sigmoid();

        for (var i = 0; i < matrix2.getRows(); i++) {
            for (var j = 0; j < matrix2.getCols(); j++) {
                assertEquals(matrix2.getData()[i][j], 1 / (1 + Math.exp(-matrix1.getData()[i][j])));
            }
        }
    }

    @Test
    void dSigmoidTest() throws DataException {
        Matrix matrix1 = new Matrix(NUM_ROWS, NUM_COLS);

        Matrix matrix2 = matrix1.dSigmoid();

        for (var i = 0; i < matrix2.getRows(); i++) {
            for (var j = 0; j < matrix2.getCols(); j++) {
                assertEquals(matrix2.getData()[i][j], matrix1.getData()[i][j] * (1 - matrix1.getData()[i][j]));
            }
        }
    }

    @Test
    void fromArrayTest() throws DataException {
        double[] x = {1.0, 2.2, 3.3, 4.5};

        Matrix matrix = Matrix.fromArray(x);

        assertEquals(4, matrix.getRows());
        assertEquals(1, matrix.getCols());
        for (var i = 0; i < matrix.getRows(); i++) {
            assertEquals(x[i], matrix.getData()[i][0]);
        }
    }

    @Test
    void toArrayTest() throws DataException {

        Matrix matrix = new Matrix(NUM_ROWS, NUM_COLS);

        List<Double> result = matrix.toArray();

        for (var i = 0; i < matrix.getRows(); i++) {
            for (var j = 0; j < matrix.getCols(); j++) {
                assertEquals(matrix.getData()[i][j],result.get(i*matrix.getCols()+j));
            }
        }
    }
}


