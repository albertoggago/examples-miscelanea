package com.example.demo.neural;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NeuralNetworkTest {

    private final int input = 2;
    private final int hidden = 3;
    private final int output = 4;
    private final double delta = 0.20;//binary decisions it is Ok for the method to predict.
    private final int epochs = 50000;


    @Test
    void createNeuralNetworkTest() throws DataException, IllegalAccessException {

        Map<String,int[]> resultWanted = Stream.of(
                new SimpleEntry<>("weightsIh", new int[]{hidden, input}),
                new SimpleEntry<>("weightsHo", new int[]{output, hidden}),
                new SimpleEntry<>("biasH", new int[]{hidden, 1}),
                new SimpleEntry<>("biasO", new int[]{output, 1})
        ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));

        Field[] fields = NeuralNetwork.class.getDeclaredFields();

        NeuralNetwork neuralNetwork = new NeuralNetwork(input, hidden, output);

        int i = 0;
        for (Field field : fields){
            field.setAccessible(true);
            if (field.getType().isAssignableFrom(Matrix.class)) {
                Matrix matrix = (Matrix) field.get(neuralNetwork);
                assertEquals(matrix.getRows(), resultWanted.get(field.getName())[0], "Error: "+field.getName()+", rows");
                assertEquals(matrix.getCols(), resultWanted.get(field.getName())[1], "Error: "+field.getName()+", cols");
                i++;
            }
        }
        assertEquals(4,i);
    }

    @Test
    void predictTest() throws DataException {
        NeuralNetwork neuralNetwork = new NeuralNetwork(input, hidden, output);
        double[] enterData = new Random().doubles(input).toArray();

        List<Double> result = neuralNetwork.predict(enterData);

        assertEquals(output,result.size());
    }

    @Test
    void predictLogicalGateNotTest() throws DataException {
        double [][] sampleInput = {{0},{1}};
        double [][] sampleOutPut = {{1},{0}};
        NeuralNetwork neuralNetwork = new NeuralNetwork(1,1,1);


        neuralNetwork.fit(sampleInput,sampleOutPut,epochs, delta);

        assertEquals(1.0, Math.round(neuralNetwork.predict(new double[]{0.0}).getFirst()));
        assertEquals(0.0, Math.round(neuralNetwork.predict(new double[]{1.0}).getFirst()));
    }

    @Test
    void predictLogicalGateXAND() throws DataException {
        double [][] sampleInput = {{0,0},{0,1},{1,0},{1,1}};
        double [][] sampleOutPut = {{1},{0},{0},{1}};
        NeuralNetwork neuralNetwork = new NeuralNetwork(2,10,1);

        neuralNetwork.fit(sampleInput,sampleOutPut,epochs, delta);

        for (var i=0;i<sampleInput.length;i++) {
            assertEquals(sampleOutPut[i][0], Math.round(neuralNetwork.predict(sampleInput[i]).getFirst()));
        }
    }

    @Test
    void smallCall() throws DataException {
        double [][] sampleInput = {{0,0},{0,1},{1,0},{1,1}};
        double [][] sampleOutPut = {{1},{0},{0},{1}};
        NeuralNetwork neuralNetwork = new NeuralNetwork(2,4,1,0.3);

        neuralNetwork.fit(sampleInput,sampleOutPut,1, 0);

        assertTrue(true);
    }

}
