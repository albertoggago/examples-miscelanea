package com.example.demo.bowling;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScoreTest {

    @Test
    void initTest(){
        Score score = new Score();
        assertEquals(TypeScore.FAIL, score.getType());
        assertEquals(0, score.getPoints());
    }

    @Test
    void cloneTest(){
        Score score = new Score(TypeScore.FAIL, 1,2,3);
        Score scoreCloned = BowlsManager.clone(score);
        assertEquals(score, scoreCloned);
        assertNotSame(score, scoreCloned);
    }

    @Test
    void equalsSameObject(){
        Score score = new Score(TypeScore.FAIL, 1,2,3);
        Score scoreOther = new Score(TypeScore.FAIL, 1,2,3);
        assertNotEquals(null, score);
        assertNotEquals(new Object(), score);
        assertNotEquals(score, new Object());
        assertEquals(scoreOther, score);
        assertNotEquals(new Score(TypeScore.FAIL, 2,2,3), score);
        assertNotEquals(new Score(TypeScore.STRIKE, 1,2,3), score);
        assertNotEquals(new Score(TypeScore.FAIL, 1,3,3), score);
        assertNotEquals(new Score(TypeScore.FAIL, 1,2,4), score);
        assertEquals(scoreOther.hashCode(), score.hashCode());

    }
}

