package com.example.demo.bowling;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TurnBowlingTest {


    @Test
    void twoTriesError(){
        TurnBowling turnBowling = new TurnBowling();
        assertEquals(BowlsManager.NUMBER_TRIES, turnBowling.getTries().size());
    }

    @Test
    void twoTriesValidateError(){
        TurnBowling turnBowling = new TurnBowling();
        turnBowling.setTries(Collections.singletonList(new Integer[]{}));
        assertFalse(turnBowling.validate());
        assertEquals("ERROR", turnBowling.toString());
    }

    @Test
    void twoTriesChangeTriesOk(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{-1,2,3,11}, new Integer[]{4,5,6});
        List<Integer[]> newTries = new ArrayList<>();
        newTries.add(new Integer[]{2,3});
        newTries.add(new Integer[]{4,5});
        turnBowling.setTries(newTries);
        assertTrue(turnBowling.validate());
        assertEquals(4L, turnBowling.getScore().getPoints());
    }


    @Test
    void cleanBowls(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{-1,2,3,11}, new Integer[]{4,5});
        assertTrue(turnBowling.validate());
    }

    @Test
    void validateOk(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{2,3}, new Integer[]{4,5});
        assertTrue(turnBowling.validate());
    }

    @Test
    void validateErrorInFirstTry(){
        TurnBowling turnBowling = new TurnBowling();
        turnBowling.setTries(Arrays.asList(new Integer[]{2,3,3}, new Integer[]{4,5}));
        assertFalse(turnBowling.validate());
    }

    @Test
    void validateErrorInSecondTry(){
        TurnBowling turnBowling = new TurnBowling();
        turnBowling.setTries(Arrays.asList(new Integer[]{2,3}, new Integer[]{4,4,5}));
        assertFalse(turnBowling.validate());
    }

    @Test
    void validateScoreZero(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{}, new Integer[]{});
        assertEquals(0, turnBowling.getScore().getPoints());
        assertEquals("0-", turnBowling.toString());
    }

    @Test
    void validateScore3(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3}, new Integer[]{});
        assertEquals(3, turnBowling.getScore().getPoints());
        assertEquals(TypeScore.FAIL, turnBowling.getScore().getType());
        assertEquals("3-", turnBowling.toString());
    }

    @Test
    void validateScore4(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{}, new Integer[]{1,2,3,4});
        assertEquals(4, turnBowling.getScore().getPoints());
        assertEquals(TypeScore.FAIL, turnBowling.getScore().getType());
        assertEquals("4-", turnBowling.toString());
    }

    @Test
    void validateScore5(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3}, new Integer[]{4,5});
        assertEquals(5, turnBowling.getScore().getPoints());
        assertEquals(TypeScore.FAIL, turnBowling.getScore().getType());
        assertEquals("5-", turnBowling.toString());
    }

    @Test
    void validateScore10Spare(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5}, new Integer[]{6,7,8,9,10});
        TurnBowling turnBowlingNext = new TurnBowling(new Integer[]{1,2,3,4}, new Integer[]{5,6});
        turnBowling.scoringExtra(List.of(turnBowlingNext));
        assertEquals(10+turnBowlingNext.score.getFirstPoints(), turnBowling.getScore().getPoints());
        assertEquals(turnBowlingNext.score.getFirstPoints(), turnBowling.getScore().getExtraPoints());
        assertEquals(TypeScore.SPARE, turnBowling.getScore().getType());
        assertEquals("5/", turnBowling.toString());
    }

    @Test
    void validateScore10Strike(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9,10}, new Integer[]{});
        TurnBowling turnBowlingNext = new TurnBowling(new Integer[]{1,2,3,4}, new Integer[]{5,6});
        turnBowling.scoringExtra(List.of(turnBowlingNext));
        assertEquals(10+turnBowlingNext.score.getFirstPoints()+turnBowlingNext.score.getSecondPoints(),
                      turnBowling.getScore().getPoints());
        assertEquals(turnBowlingNext.score.getFirstPoints()+turnBowlingNext.score.getSecondPoints(),
                turnBowling.getScore().getExtraPoints());
        assertEquals(TypeScore.STRIKE, turnBowling.getScore().getType());
        assertEquals("X", turnBowling.toString());
    }

    @Test
    void validateScore10Fail(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9}, new Integer[]{});
        TurnBowling turnBowlingNext = new TurnBowling(new Integer[]{1,2,3,4}, new Integer[]{5,6});
        turnBowling.scoringExtra(List.of(turnBowlingNext));
        assertEquals(turnBowling.score.getFirstPoints()+turnBowling.score.getSecondPoints(),
                turnBowling.getScore().getPoints());
        assertEquals(0, turnBowling.getScore().getExtraPoints());
        assertEquals(TypeScore.FAIL, turnBowling.getScore().getType());
    }

    @Test
    void testCloneableObject(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4}, new Integer[]{5,6});
        TurnBowling turnBowlingClone = BowlsManager.clone(turnBowling);
        assertEquals(turnBowling, turnBowlingClone);
        assertNotSame(turnBowling, turnBowlingClone);
    }

    @Test
    void equalsSameObject(){
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2}, new Integer[]{5,6});
        TurnBowling turnBowlingOther = new TurnBowling(new Integer[]{1,2}, new Integer[]{5,6});
        assertEquals(turnBowling, turnBowling);
        assertNotEquals(null, turnBowling);
        TurnBowling turnBowlingNull = null;
        assertNotEquals(turnBowling, turnBowlingNull);
        assertNotEquals(new Object(), turnBowling);
        assertNotEquals(turnBowling, new Object());
        assertEquals(turnBowlingOther, turnBowling);
        assertNotEquals(new TurnBowling(new Integer[]{3,4}, new Integer[]{5,6}), turnBowling);
        assertNotEquals(new TurnBowling(new Integer[]{1,2}, new Integer[]{7,8}), turnBowling);
        assertNotEquals(new TurnBowling(new Integer[]{1,2,3,4}, new Integer[]{5,6,7,8}), turnBowling);
        assertEquals(turnBowlingOther.hashCode(), turnBowling.hashCode());
    }

}
