package com.example.demo.bowling;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class BowlsManagerTest {

    @Test
    void validateNull(){
        Integer[] base = new Integer[]{};
        Integer[] result = BowlsManager.clean(base);
        assertArrayEquals(new Integer[]{}, result);
        assertTrue(BowlsManager.validate(base));
    }
    @Test
    void validateOk(){
        Integer[] base = new Integer[]{1,2,3};
        Integer[] result = BowlsManager.clean(base);
        assertArrayEquals(new Integer[]{1,2,3}, result);
        assertTrue(BowlsManager.validate(base));
    }

    @Test
    void validateOutRange(){
        Integer[] base = new Integer[]{-1,0,3,10,-1};
        Integer[] result = BowlsManager.clean(base);
        assertArrayEquals(new Integer[]{3,10}, result);
        assertFalse(BowlsManager.validate(base));
    }
    @Test
    void validateUnordered(){
        Integer[] base = new Integer[]{3,1};
        Integer[] result = BowlsManager.clean(base);
        assertArrayEquals(new Integer[]{1,3}, result);
        assertThat(result).isNotEqualTo(new Integer[]{3,1});
        assertFalse(BowlsManager.validate(base));
    }

    @Test
    void validateCleanWithPrevNoNull(){
        Integer[] prev = new Integer[]{};
        Integer[] base = new Integer[]{1,3};
        Integer[] result = BowlsManager.cleanWithPrev(base, prev);
        assertArrayEquals(new Integer[]{1,3}, result);
    }

    @Test
    void validateCleanWithPrevNoMatch(){
        Integer[] prev = new Integer[]{2,4};
        Integer[] base = new Integer[]{1,3};
        Integer[] result = BowlsManager.cleanWithPrev(base, prev);
        assertArrayEquals(new Integer[]{1,3}, result);
    }

    @Test
    void validateCleanWithPrevMatch(){
        Integer[] prev = new Integer[]{2,4};
        Integer[] base = new Integer[]{1,2,3};
        Integer[] result = BowlsManager.cleanWithPrev(base, prev);
        assertArrayEquals(new Integer[]{1,3}, result);
    }

    @Test
    void validateFailScoreNull(){
        Integer[] first = new Integer[]{};
        Integer[] second = new Integer[]{};
        Score result = BowlsManager.score(first, second);
        assertNotNull(result);
        assertEquals(0, result.getPoints());
        assertEquals(TypeScore.FAIL, result.getType());
    }

    @Test
    void validateFailScore(){
        Integer[] first = new Integer[]{2,4};
        Integer[] second = new Integer[]{1,2,3};
        Score result = BowlsManager.score(first, second);
        assertNotNull(result);
        assertEquals(first.length+second.length, result.getPoints());
        assertEquals(TypeScore.FAIL, result.getType());
    }
}
