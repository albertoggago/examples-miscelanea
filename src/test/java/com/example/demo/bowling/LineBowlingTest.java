package com.example.demo.bowling;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class LineBowlingTest {
    private LineBowling lineBowling;

    @BeforeEach
    void init(){
        lineBowling = new LineBowling();
    }
    @Test
    void tenTurns(){
       assertEquals(BowlsManager.NUMBER_LINES, lineBowling.getTurns().size());
    }

    @Test
    void ValidateOk(){
        assertTrue(lineBowling.validate());
    }

    @Test
    void tenTurnsValidateFailure(){
        lineBowling.setTurns(List.of(new TurnBowling()));
        assertFalse(lineBowling.validate());
    }

    @Test
    void equalsSameObject(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9,10}, new Integer[]{});
        TurnBowling turnBowlingExtra = new TurnBowling(new Integer[]{1,2,3,4,5}, new Integer[]{6,7,8,9,10});
        IntStream.rangeClosed(1,BowlsManager.NUMBER_LINES).forEach(a->turns.add(BowlsManager.clone(turnBowling)));
        turns.add(turnBowlingExtra);
        LineBowling lineBowlingSameObject = new LineBowling(turns);
        LineBowling lineBowlingOther = BowlsManager.clone(lineBowlingSameObject);
        assertNotEquals(null, lineBowlingSameObject);
        assertNotEquals(new Object(), lineBowlingSameObject);
        assertNotEquals(lineBowlingSameObject, new Object());
        assertEquals(lineBowlingOther, lineBowlingSameObject);
        assertEquals(lineBowlingOther.hashCode(), lineBowlingSameObject.hashCode());
    }

    @Test
    void tenTurnsAllStrikes(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9,10}, new Integer[]{});
        IntStream.rangeClosed(1,BowlsManager.NUMBER_LINES+1).forEach(a->turns.add(BowlsManager.clone(turnBowling)));
        LineBowling lineBowlingAllStrikes = new LineBowling(turns);
        assertTrue(lineBowlingAllStrikes.validate());
        assertEquals("""
                X X X X X X X X X X X
                Total Points: 300
                """,lineBowlingAllStrikes.toString());
    }

    @Test
    void tenTurnsEnoughElements(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9,10}, new Integer[]{});
        IntStream.rangeClosed(1,BowlsManager.NUMBER_LINES+2).forEach(a->turns.add(BowlsManager.clone(turnBowling)));
        LineBowling lineBowling10Enough = new LineBowling(turns);
        assertFalse(lineBowling10Enough.validate());
    }

    @Test
    void extraTurnNotValid(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9}, new Integer[]{});
        IntStream.rangeClosed(1,BowlsManager.NUMBER_LINES+1).forEach(a->turns.add(BowlsManager.clone(turnBowling)));
        LineBowling lineBowlingNotValid = new LineBowling(turns);
        assertFalse(lineBowlingNotValid.validate());
        assertEquals("""
                
                Total Points: 0
                """,lineBowlingNotValid.toString());
    }

    @Test
    void extraTurnNotValidForceUpdate(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9}, new Integer[]{});
        IntStream.rangeClosed(1,BowlsManager.NUMBER_LINES).forEach(a->turns.add(BowlsManager.clone(turnBowling)));
        LineBowling lineBowlingExtraTurn = new LineBowling(turns);
        try {
            Field turnsField = lineBowlingExtraTurn.getClass().getDeclaredField("turns");
            turnsField.setAccessible(true);
            turns.add(new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9,10}, new Integer[]{}));
            turnsField.set(lineBowlingExtraTurn, turns);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        assertFalse(lineBowlingExtraTurn.validate());
    }

    @Test
    void tenTurnsAllStrikesExtraASpare(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9,10}, new Integer[]{});
        TurnBowling turnBowlingExtra = new TurnBowling(new Integer[]{1,2,3,4,5}, new Integer[]{6,7,8,9,10});
        IntStream.rangeClosed(1,BowlsManager.NUMBER_LINES).forEach(a->turns.add(BowlsManager.clone(turnBowling)));
        turns.add(turnBowlingExtra);
        LineBowling lineBowlingAllStrikesExtra = new LineBowling(turns);
        assertTrue(lineBowlingAllStrikesExtra.validate());
        assertEquals("""
                X X X X X X X X X X 5/
                Total Points: 285
                """,lineBowlingAllStrikesExtra.toString());
    }
    @Test
    void tenTurnsAllFailures(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9}, new Integer[]{});
        IntStream.rangeClosed(1,BowlsManager.NUMBER_LINES).forEach(a->turns.add(BowlsManager.clone(turnBowling)));
        LineBowling lineBowlingAllFailures = new LineBowling(turns);
        assertTrue(lineBowlingAllFailures.validate());
        assertEquals("""
                9- 9- 9- 9- 9- 9- 9- 9- 9- 9-
                Total Points: 90
                """,lineBowlingAllFailures.toString());
    }
    @Test
    void tenTurnsAllSpares(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling = new TurnBowling(new Integer[]{1,2,3,4,5}, new Integer[]{6,7,8,9,10});
        TurnBowling turnBowlingExtra = new TurnBowling(new Integer[]{1,2,3,4,5}, new Integer[]{});
        IntStream.rangeClosed(1,BowlsManager.NUMBER_LINES).forEach(a->turns.add(BowlsManager.clone(turnBowling)));
        turns.add(turnBowlingExtra);
        LineBowling lineBowlingTenTurns = new LineBowling(turns);
        assertTrue(lineBowlingTenTurns.validate());
        assertEquals("""
                5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5
                Total Points: 150
                """,lineBowlingTenTurns.toString());
    }

    @Test
    void tenTurnsMixed(){
        List<TurnBowling> turns = new ArrayList<>();
        TurnBowling turnBowling1 = new TurnBowling(new Integer[]{1,2,3,4,5}, new Integer[]{6,7,8,9,10});
        TurnBowling turnBowling2 = new TurnBowling(new Integer[]{1,2,3,4,5}, new Integer[]{});
        TurnBowling turnBowling3 = new TurnBowling(new Integer[]{}, new Integer[]{6,7,8,9,10});
        TurnBowling turnBowling4 = new TurnBowling(new Integer[]{1,2,3,4,5,6,7,8,9,10}, new Integer[]{});
        TurnBowling turnBowlingExtra = new TurnBowling(new Integer[]{}, new Integer[]{1,2,3,4,5});
        turns.add(turnBowling1);
        turns.add(turnBowling2);
        turns.add(turnBowling3);
        turns.add(turnBowling4);
        turns.add(BowlsManager.clone(turnBowling1));
        turns.add(BowlsManager.clone(turnBowling2));
        turns.add(BowlsManager.clone(turnBowling3));
        turns.add(BowlsManager.clone(turnBowling4));
        turns.add(BowlsManager.clone(turnBowling3));
        turns.add(BowlsManager.clone(turnBowling4));
        turns.add(turnBowlingExtra);
        LineBowling lineBowling10Mixed = new LineBowling(turns);
        assertTrue(lineBowling10Mixed.validate());
        assertEquals("""
                5/ 5- 5- X 5/ 5- 5- X 5- X 5
                Total Points: 105
                """, lineBowling10Mixed.toString());
        assertEquals(15, lineBowling10Mixed.getTurns().get(0).getScore().getPoints());
        assertEquals(5, lineBowling10Mixed.getTurns().get(1).getScore().getPoints());
        assertEquals(5, lineBowling10Mixed.getTurns().get(2).getScore().getPoints());
        assertEquals(20, lineBowling10Mixed.getTurns().get(3).getScore().getPoints());
        assertEquals(15, lineBowling10Mixed.getTurns().get(4).getScore().getPoints());
        assertEquals(5, lineBowling10Mixed.getTurns().get(5).getScore().getPoints());
        assertEquals(5, lineBowling10Mixed.getTurns().get(6).getScore().getPoints());
        assertEquals(15, lineBowling10Mixed.getTurns().get(7).getScore().getPoints());
        assertEquals(5, lineBowling10Mixed.getTurns().get(8).getScore().getPoints());
        assertEquals(15, lineBowling10Mixed.getTurns().get(9).getScore().getPoints());
        assertEquals(5, lineBowling10Mixed.getTurns().get(10).getScore().getPoints());
    }
}
