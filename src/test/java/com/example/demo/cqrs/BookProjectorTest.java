package com.example.demo.cqrs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class BookProjectorTest {

    BookProjector bookProjector;
    RoomReadRepository roomReadRepository;

    @BeforeEach
    void init(){
        roomReadRepository = mock(RoomReadRepository.class);
        bookProjector = new BookProjector(roomReadRepository);
    }

    @Test
    void projectorBook(){
        when(roomReadRepository.addBook(any(Book.class))).thenReturn(true);

        boolean result = bookProjector.projector(MockBookManager.createBookWrite());

        assertTrue(result);
        verify(roomReadRepository, times(1)).addBook(any(Book.class));
    }

    @Test
    void projectorBookFailed(){
        when(roomReadRepository.addBook(any(Book.class))).thenReturn(false);

        boolean result = bookProjector.projector(MockBookManager.createBookWrite());

        assertFalse(result);
        verify(roomReadRepository, times(1)).addBook(any(Book.class));
    }

    @Test
    void projectRoom(){
        when(roomReadRepository.addRoom(any(Room.class))).thenReturn(true);

        boolean result = bookProjector.projector(MockBookManager.createRoomWrite());

        assertTrue(result);
        verify(roomReadRepository, times(1)).addRoom(any(Room.class));
    }

    @Test
    void projectRoomFailed(){
        when(roomReadRepository.addRoom(any(Room.class))).thenReturn(false);

        boolean result = bookProjector.projector(MockBookManager.createRoomWrite());

        assertFalse(result);
        verify(roomReadRepository, times(1)).addRoom(any(Room.class));
    }

}
