package com.example.demo.cqrs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class RoomWriteTest {
    static final String ROOM_NAME = "roomName";


    @Test
    void equalsSameObject(){
        RoomWrite roomWrite = new RoomWrite(ROOM_NAME);
        RoomWrite roomWriteOther = new RoomWrite(ROOM_NAME);
        assertEquals(roomWrite, roomWrite);
        RoomWrite roomWriteNull = null;
        assertNotEquals(roomWrite, roomWriteNull);
        assertNotEquals(roomWrite, new Object());
        assertEquals(roomWriteOther, roomWrite);
        assertNotEquals(roomWrite, new RoomWrite("ROOM_OTHER"));

        assertEquals(roomWriteOther.hashCode(), roomWrite.hashCode());

        roomWriteOther.setMessageSent(true);
        assertNotEquals(roomWriteOther, roomWrite);

        assertEquals("Room(name=roomName, messageSent: false)",roomWrite.toString());
    }

}
