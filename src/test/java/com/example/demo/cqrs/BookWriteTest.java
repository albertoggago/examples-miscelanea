package com.example.demo.cqrs;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class BookWriteTest {

    @Test
    void equalsSameObject(){
        BookWrite bookWrite = MockBookManager.createBookWrite();
        BookWrite bookWriteOther = MockBookManager.createBookWrite();
        assertEquals(bookWrite, bookWrite);
        BookWrite bookWriteNull = null;
        assertNotEquals(bookWrite, bookWriteNull);
        assertNotEquals(bookWrite, new Object());
        assertEquals(bookWriteOther, bookWrite);
        assertNotEquals(new BookWrite("CLIENT_ID", MockBookManager.ROOM_NAME, MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE), bookWrite);
        assertNotEquals(new BookWrite(MockBookManager.CLIENT_ID, "ROOM_NAME_OTHER", MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE), bookWrite);
        assertNotEquals(new BookWrite(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, LocalDate.now(), MockBookManager.DEPARTURE_DATE), bookWrite);
        assertNotEquals(new BookWrite(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, MockBookManager.ARRIVE_DATE, LocalDate.now()), bookWrite);

        assertEquals(bookWriteOther.hashCode(), bookWrite.hashCode());

        bookWriteOther.setMessageSent(true);
        assertNotEquals(bookWriteOther, bookWrite);

        assertEquals("Book(clientID=clientID, roomName=roomName, arrivalDate=2022-12-08, departureDate=2022-12-09, messageSent: false)",bookWrite.toString());
    }

}
