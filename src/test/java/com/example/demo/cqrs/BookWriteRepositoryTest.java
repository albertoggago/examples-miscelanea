package com.example.demo.cqrs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertFalse;

class BookWriteRepositoryTest {

    BookWriteRepository bookWriteRepository;

    @BeforeEach
    void init(){
        bookWriteRepository = new BookWriteRepository();
    }
    @Test
    void addBook(){
        bookWriteRepository.addBook(MockBookManager.createBookWrite());

        assertEquals(1,bookWriteRepository.getStoreBook().size());

        BookWrite bookRepo = bookWriteRepository.getStoreBook().get(MockBookManager.CLIENT_ID);
        assertEquals(MockBookManager.CLIENT_ID,bookRepo.getClientID());
        assertEquals(MockBookManager.ROOM_NAME,bookRepo.getRoomName());
        assertFalse(MockBookManager.ROOM_NAME,bookRepo.isMessageSent());
    }

    @Test
    void addRoom(){
        bookWriteRepository.addRoom(MockBookManager.createRoomWrite());

        assertEquals(1,bookWriteRepository.getStoreRoom().size());

        RoomWrite roomRepo = bookWriteRepository.getStoreRoom().get(MockBookManager.ROOM_NAME);
        assertEquals(MockBookManager.ROOM_NAME,roomRepo.getName());
        assertFalse(MockBookManager.ROOM_NAME,roomRepo.isMessageSent());
    }

}
