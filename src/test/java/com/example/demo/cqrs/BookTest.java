package com.example.demo.cqrs;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class BookTest {

    @Test
    void equalsSameObject(){
        Book book = MockBookManager.createBook();
        Book bookOther = MockBookManager.createBook();
        assertEquals(book, book);
        assertNotEquals(book, new Object());
        assertEquals(bookOther, book);
        assertNotEquals(new Book("CLIENT_ID", MockBookManager.ROOM_NAME, MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE), book);
        assertNotEquals(new Book(MockBookManager.CLIENT_ID, "ROOM_NAME", MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE), book);
        assertNotEquals(new Book(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, LocalDate.now(), MockBookManager.DEPARTURE_DATE), book);
        assertNotEquals(new Book(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, MockBookManager.ARRIVE_DATE, LocalDate.now()), book);

        assertEquals(bookOther.hashCode(), book.hashCode());

        assertEquals("Book(clientID=clientID, roomName=roomName, arrivalDate=2022-12-08, departureDate=2022-12-09)", book.toString());
    }

}
