package com.example.demo.cqrs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class RoomReadServiceTest {

    RoomReadService roomReadService;
    RoomReadRepository roomReadRepository;

    @BeforeEach
    void setup(){
        roomReadRepository = mock(RoomReadRepository.class);
        roomReadService = new RoomReadService(roomReadRepository);
    }

    @Test
    void freeRooms(){
        List<Room> roomsReturned = new ArrayList<>();
        roomsReturned.add(MockBookManager.createRoom());

        when(roomReadService.freeRooms(any(LocalDate.class), any(LocalDate.class))).thenReturn(roomsReturned);

        List<Room> rooms = roomReadService.freeRooms(MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE);

        assertEquals(1 ,rooms.size());
        assertEquals(MockBookManager.ROOM_NAME ,rooms.getFirst().getName());
    }
}
