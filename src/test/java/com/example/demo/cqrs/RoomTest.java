package com.example.demo.cqrs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class RoomTest {

    @Test
    void equalsSameObject(){
        Room room = MockBookManager.createRoom();
        Room roomOther = MockBookManager.createRoom();
        assertEquals(room, room);
        assertNotEquals(room, new Object());
        assertEquals(roomOther, room);
        assertNotEquals(room, new Room("ROOM_OTHER"));

        assertEquals(roomOther.hashCode(), room.hashCode());

        assertEquals("Room(name=roomName)",room.toString());
    }

}
