package com.example.demo.cqrs;

import java.time.LocalDate;

public class MockBookManager {
    static final String CLIENT_ID = "clientID";
    static final String ROOM_NAME = "roomName";
    static final LocalDate ARRIVE_DATE = LocalDate.of(2022, 12, 8);
    static final LocalDate DEPARTURE_DATE = LocalDate.of(2022, 12, 9);

    private MockBookManager(){}

    public static BookWrite createBookWrite() {
        return new BookWrite(CLIENT_ID, ROOM_NAME, ARRIVE_DATE, DEPARTURE_DATE);
    }

    public static RoomWrite createRoomWrite() {
        return new RoomWrite(ROOM_NAME);
    }

    public static Book createBook() {
        return new Book(CLIENT_ID, ROOM_NAME, ARRIVE_DATE, DEPARTURE_DATE);
    }

    public static Room createRoom() {
        return new Room(ROOM_NAME);
    }

    public static RoomRead createRoomRead() {
        return new RoomRead(ROOM_NAME);
    }
}
