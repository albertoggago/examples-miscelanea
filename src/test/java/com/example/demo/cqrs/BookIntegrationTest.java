package com.example.demo.cqrs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;



class BookIntegrationTest {

    private static final String FIRST_ROOM = "FirstRoom";
    private static final String SECOND_ROOM = "SecondRoom";
    private static final String CLIENT1 = "CLIENT1";
    private static final String CLIENT2 = "CLIENT2";
    private final LocalDate firstArriveDate = LocalDate.of(2022, 1, 1);
    private final LocalDate firstDepartureDate = LocalDate.of(2022, 1, 2);


    private RoomReadRepository readRepository;
    private RoomReadService readService;
    private BookWriteService writeService;
    private BookProjector projector;


    @BeforeEach
    void init() {
        readRepository = new RoomReadRepository();
        readService = new RoomReadService(readRepository);

        BookWriteRepository writeRepository = new BookWriteRepository();
        writeService = new BookWriteService(writeRepository);

        projector = new BookProjector(readRepository);
    }

    @Test
    void standardWork() {

        writeService.addRoom(new Room(FIRST_ROOM));
        writeService.addRoom(new Room(SECOND_ROOM));
        writeService.runAsync(projector); // simulate Transfer

        assertEquals(2, readRepository.getStore().size());

        List<Room> freeRooms1 = readService.freeRooms(firstArriveDate, firstDepartureDate);
        assertEquals(2, freeRooms1.size());

        writeService.addBook(CLIENT1, freeRooms1.getFirst().getName(), firstArriveDate, firstDepartureDate);
        writeService.runAsync(projector);

        assertEquals(1, readRepository.getStore().get(freeRooms1.getFirst().getName()).getBooks().size());

        List<Room> freeRooms2 = readService.freeRooms(firstArriveDate, firstDepartureDate);
        assertEquals(1, freeRooms2.size());

        writeService.addBook(CLIENT2, freeRooms2.getFirst().getName(), firstArriveDate, firstDepartureDate);
        writeService.runAsync(projector);

        assertEquals(1, readRepository.getStore().get(freeRooms1.getFirst().getName()).getBooks().size());

        List<Room> freeRooms3 = readService.freeRooms(firstArriveDate, firstDepartureDate);
        assertEquals(0, freeRooms3.size());
    }

}
