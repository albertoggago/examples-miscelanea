package com.example.demo.cqrs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RoomReadRepositoryTest {

    public static final String OTHER_ROOM = "OTHER ROOM";
    RoomReadRepository roomReadRepository;

    @BeforeEach
    void init(){
        roomReadRepository = new RoomReadRepository();
    }

    @Test
    void addRoomNull(){
        boolean result = roomReadRepository.addRoom(new Room(null));
        assertFalse(result);
    }

    @Test
    void addRoomFirst(){

        boolean result = roomReadRepository.addRoom(MockBookManager.createRoom());

        List<RoomRead> roomRead = roomReadRepository.getStore().values().stream().toList();

        assertTrue(result);
        assertEquals(1, roomRead.size());
        assertEquals(MockBookManager.ROOM_NAME, roomRead.getFirst().getName());
    }

    @Test
    void addRoomFailed(){
        roomReadRepository.addRoom(MockBookManager.createRoom());
        boolean result = roomReadRepository.addRoom(MockBookManager.createRoom());

        List<RoomRead> roomRead = roomReadRepository.getStore().values().stream().toList();

        assertFalse(result);
        assertEquals(1, roomRead.size());
        assertEquals(MockBookManager.ROOM_NAME, roomRead.getFirst().getName());
    }

    @Test
    void addTwoRooms(){
        boolean result1 = roomReadRepository.addRoom(MockBookManager.createRoom());
        boolean result2 = roomReadRepository.addRoom(new Room(OTHER_ROOM));

        List<RoomRead> roomRead = roomReadRepository.getStore().values().stream().toList();
        List<String> roomReadName = roomRead.stream().map(RoomRead::getName).toList();

        assertTrue(result1);
        assertTrue(result2);
        assertEquals(2, roomReadRepository.getStore().size());
        assertTrue(roomReadName.contains(OTHER_ROOM));
        assertTrue(roomReadName.contains(MockBookManager.ROOM_NAME));

    }

    @Test
    void addBookNull(){
        boolean result1 = roomReadRepository.addBook(new Book(null,MockBookManager.ROOM_NAME, MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE));
        assertFalse(result1);
        boolean result2 = roomReadRepository.addBook(new Book(MockBookManager.CLIENT_ID, null, MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE));
        assertFalse(result2);
        boolean result3 = roomReadRepository.addBook(new Book(MockBookManager.CLIENT_ID,  MockBookManager.ROOM_NAME, null, MockBookManager.DEPARTURE_DATE));
        assertFalse(result3);
        boolean result4 = roomReadRepository.addBook(new Book(MockBookManager.CLIENT_ID,  MockBookManager.ROOM_NAME, MockBookManager.ARRIVE_DATE, null));
        assertFalse(result4);
        boolean result5 = roomReadRepository.addBook(new Book(MockBookManager.CLIENT_ID,  MockBookManager.ROOM_NAME
                                                              , LocalDate.of(2022,1,1)
                                                              , LocalDate.of(2022,1,1)));
        assertFalse(result5);

    }

    @Test
    void addBookNotRooms() {
        boolean result = roomReadRepository.addBook(MockBookManager.createBook());

        assertFalse(result);
        assertEquals(0, roomReadRepository.getStore().size());
    }

    @Test
    void addBookOk() {
        roomReadRepository.addRoom(MockBookManager.createRoom());
        boolean result = roomReadRepository.addBook(MockBookManager.createBook());

        assertTrue(result);
        assertEquals(1, roomReadRepository.getStore().size());
        assertEquals(MockBookManager.CLIENT_ID, roomReadRepository.getStore()
                .get(MockBookManager.ROOM_NAME).getBooks().getFirst().getClientID());
    }

    @Test
    void addBookSelectedOk() {
        roomReadRepository.addRoom(MockBookManager.createRoom());
        boolean result1 = roomReadRepository.addBook(
                new Book(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, LocalDate.of(2022,1,1),LocalDate.of(2022,1,2)));
        boolean result2 = roomReadRepository.addBook(
                new Book(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, LocalDate.of(2022,1,5),LocalDate.of(2022,1,6)));
        boolean result3 = roomReadRepository.addBook(
                new Book(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, LocalDate.of(2022,1,3),LocalDate.of(2022,1,4)));
        boolean result4 = roomReadRepository.addBook(
                new Book(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, LocalDate.of(2022,1,3),LocalDate.of(2022,1,4)));

        assertTrue(result1);
        assertTrue(result2);
        assertTrue(result3);
        assertFalse(result4);
        assertEquals(1, roomReadRepository.getStore().size());
        assertEquals(3, roomReadRepository.getStore().get(MockBookManager.ROOM_NAME).getBooks().size());
    }

    @Test
    void testFreeRooms(){
        roomReadRepository.addRoom(MockBookManager.createRoom());
        roomReadRepository.addRoom(new Room(OTHER_ROOM));

        List<Room> result = roomReadRepository.freeRooms(LocalDate.now(), LocalDate.now().plus(Period.ofDays(1)));

        assertEquals(2, result.size());
    }

}
