package com.example.demo.cqrs;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class RoomReadTest {

    @Test
    void equalsSameObject(){
        RoomRead room = MockBookManager.createRoomRead();
        RoomRead roomOther = MockBookManager.createRoomRead();
        assertEquals(room, room);
        assertEquals(roomOther, room);
        assertNotEquals(room, new RoomRead("ROOM_OTHER"));


        assertEquals(roomOther.hashCode(), room.hashCode());

        roomOther.addBooks(MockBookManager.createBook());
        assertNotEquals(roomOther, room);

        assertEquals("RoomRead(name=roomName, books=[])",  room.toString());

        room.addBooks(MockBookManager.createBook());
        room.addBooks(new Book("clientIDNew",MockBookManager.ROOM_NAME, LocalDate.of(2022,1,1), LocalDate.of(2022,1,2)));
        assertEquals("RoomRead(name=roomName, books=[Book(clientID=clientID, roomName=roomName, " +
                        "arrivalDate=2022-12-08, departureDate=2022-12-09), Book(clientID=clientIDNew, " +
                        "roomName=roomName, arrivalDate=2022-01-01, departureDate=2022-01-02)])",
                room.toString());
    }

}
