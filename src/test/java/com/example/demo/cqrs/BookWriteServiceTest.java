package com.example.demo.cqrs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.mockito.Mockito.*;

class BookWriteServiceTest {
    BookWriteRepository bookWriteRepository;
    BookWriteService bookWriteService;

    @BeforeEach
    void setup(){
        bookWriteRepository = mock(BookWriteRepository.class);
        bookWriteService = new BookWriteService(bookWriteRepository);
    }

    @Test
    void addBookString(){

        bookWriteService.addBook(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE);

        verify(bookWriteRepository, times(1)).addBook(MockBookManager.createBookWrite());
    }

    @Test
    void addRoomString(){

        bookWriteService.addRoom(("roomName"));

        verify(bookWriteRepository, times(1)).addRoom(new RoomWrite(MockBookManager.ROOM_NAME));
    }

    @Test
    void runAsync(){

        BookProjector projector = mock(BookProjector.class);
        bookWriteService.addBook(MockBookManager.CLIENT_ID, MockBookManager.ROOM_NAME, MockBookManager.ARRIVE_DATE, MockBookManager.DEPARTURE_DATE);
        bookWriteService.addRoom(MockBookManager.ROOM_NAME);

        Map<String, BookWrite> bookWriteMap = Map.of(MockBookManager.CLIENT_ID, MockBookManager.createBookWrite());
        Map<String, RoomWrite> roomWriteMap = Map.of(MockBookManager.ROOM_NAME, MockBookManager.createRoomWrite());

        when(bookWriteRepository.getStoreBook()).thenReturn(bookWriteMap);
        when(bookWriteRepository.getStoreRoom()).thenReturn(roomWriteMap);


        BookWrite bookWriteCompare = MockBookManager.createBookWrite();
        bookWriteCompare.setMessageSent(true);
        RoomWrite roomWriteCompare = MockBookManager.createRoomWrite();
        roomWriteCompare.setMessageSent(true);

        bookWriteService.runAsync(projector);

        verify(projector, times(1)).projector(bookWriteCompare);
        verify(projector, times(1)).projector(roomWriteCompare);

        bookWriteService.runAsync(projector);

        verify(projector, times(1)).projector(bookWriteCompare);
        verify(projector, times(1)).projector(roomWriteCompare);
    }
}
