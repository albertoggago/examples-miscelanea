package com.example.demo.bank.ocr;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DigitTransformKataOfficialTest {

    private static final byte[] ONE_FOUR = {0,1,1,0,0,0,1};
    private static final byte[] ALMOST_ZERO = {0,1,1,1,1,1,0};
    private static final byte[] ALMOST_FIRE = {1,0,1,1,0,0,1};

    @Test
    void verify000_000_000(){
        byte[][] nineDigits = {DigitTransform.getZero(), DigitTransform.getZero(), DigitTransform.getZero(),
                DigitTransform.getZero(), DigitTransform.getZero(), DigitTransform.getZero(),
                DigitTransform.getZero(), DigitTransform.getZero(), DigitTransform.getZero(),
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("000000000",verify);
    }

    @Test
    void verify111_111_111(){
        byte[][] nineDigits = {DigitTransform.getOne(), DigitTransform.getOne(), DigitTransform.getOne(),
                DigitTransform.getOne(), DigitTransform.getOne(), DigitTransform.getOne(),
                DigitTransform.getOne(), DigitTransform.getOne(), DigitTransform.getOne(),
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("711111111",verify);
    }

    @Test
    void verify777_777_777(){
        byte[][] nineDigits = {DigitTransform.getSeven(), DigitTransform.getSeven(), DigitTransform.getSeven(),
                DigitTransform.getSeven(), DigitTransform.getSeven(), DigitTransform.getSeven(),
                DigitTransform.getSeven(), DigitTransform.getSeven(), DigitTransform.getSeven(),
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("777777177",verify);
    }

    @Test
    void verify200_000_000(){
        byte[][] nineDigits = {DigitTransform.getTwo(), DigitTransform.getZero(), DigitTransform.getZero(),
                DigitTransform.getZero(), DigitTransform.getZero(), DigitTransform.getZero(),
                DigitTransform.getZero(), DigitTransform.getZero(), DigitTransform.getZero()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("200800000",verify);
    }

    @Test
    void verify333_333_333(){
        byte[][] nineDigits = {DigitTransform.getThree(), DigitTransform.getThree(), DigitTransform.getThree(),
                DigitTransform.getThree(), DigitTransform.getThree(), DigitTransform.getThree(),
                DigitTransform.getThree(), DigitTransform.getThree(), DigitTransform.getThree()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("333393333",verify);
    }

    @Test
    void verify888_888_888(){
        byte[][] nineDigits = {DigitTransform.getEight(), DigitTransform.getEight(), DigitTransform.getEight(),
                DigitTransform.getEight(), DigitTransform.getEight(), DigitTransform.getEight(),
                DigitTransform.getEight(), DigitTransform.getEight(), DigitTransform.getEight()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        // "888888888 AMB [888886888, 888888988, 888888880]",
        assertTrue(verify.contains("888888888 AMB ["));
        assertTrue(verify.contains("888886888"));
        assertTrue(verify.contains("888888988"));
        assertTrue(verify.contains("888888880"));
        assertEquals(47, verify.length());

    }

    @Test
    void verify555_555_555(){
        byte[][] nineDigits = {DigitTransform.getFive(), DigitTransform.getFive(), DigitTransform.getFive(),
                DigitTransform.getFive(), DigitTransform.getFive(), DigitTransform.getFive(),
                DigitTransform.getFive(), DigitTransform.getFive(), DigitTransform.getFive()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        // "555555555 AMB [559555555, 555655555]"
        assertTrue(verify.contains("555555555 AMB ["));
        assertTrue(verify.contains("559555555"));
        assertTrue(verify.contains("555655555"));
        assertEquals(36, verify.length());
    }

    @Test
    void verify666_666_666(){
        byte[][] nineDigits = {DigitTransform.getSix(), DigitTransform.getSix(), DigitTransform.getSix(),
                DigitTransform.getSix(), DigitTransform.getSix(), DigitTransform.getSix(),
                DigitTransform.getSix(), DigitTransform.getSix(), DigitTransform.getSix()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        // "666666666 AMB [666566666, 686666666]"
        assertTrue(verify.contains("666666666 AMB ["));
        assertTrue(verify.contains("666566666"));
        assertTrue(verify.contains("686666666"));
        assertEquals(36, verify.length());
    }

    @Test
    void verify999_999_999(){
        byte[][] nineDigits = {DigitTransform.getNine(), DigitTransform.getNine(), DigitTransform.getNine(),
                DigitTransform.getNine(), DigitTransform.getNine(), DigitTransform.getNine(),
                DigitTransform.getNine(), DigitTransform.getNine(), DigitTransform.getNine()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        // "999999999 AMB [899999999, 993999999, 999959999]"
        assertTrue(verify.contains("999999999 AMB ["));
        assertTrue(verify.contains("899999999"));
        assertTrue(verify.contains("993999999"));
        assertTrue(verify.contains("999959999"));
        assertEquals(47, verify.length());
    }

    @Test
    void verify490_067_715(){
        byte[][] nineDigits = {DigitTransform.getFour(), DigitTransform.getNine(), DigitTransform.getZero(),
                DigitTransform.getZero(), DigitTransform.getSix(), DigitTransform.getSeven(),
                DigitTransform.getSeven(), DigitTransform.getOne(), DigitTransform.getFive()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        // "490067715 AMB [490067115, 490067719, 490867715]"
        assertTrue(verify.contains("490067715 AMB ["));
        assertTrue(verify.contains("490067115"));
        assertTrue(verify.contains("490067719"));
        assertTrue(verify.contains("490867715"));
        assertEquals(47, verify.length());
    }

    @Test
    void verifyX23_456_789(){
        byte[][] nineDigits = {ONE_FOUR, DigitTransform.getTwo(), DigitTransform.getThree(),
                DigitTransform.getFour(), DigitTransform.getFive(), DigitTransform.getSix(),
                DigitTransform.getSeven(), DigitTransform.getEight(), DigitTransform.getNine()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("123456789",verify);
    }

    @Test
    void verify0X0_000_051(){
        byte[][] nineDigits = {DigitTransform.getZero(), ALMOST_ZERO, DigitTransform.getZero(),
                DigitTransform.getZero(), DigitTransform.getZero(), DigitTransform.getZero(),
                DigitTransform.getZero(), DigitTransform.getFive(), DigitTransform.getOne()
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("000000051",verify);
    }

    @Test
    void verify490_867_71X(){
        byte[][] nineDigits = {DigitTransform.getFour(), DigitTransform.getNine(), DigitTransform.getZero(),
                DigitTransform.getEight(), DigitTransform.getSix(), DigitTransform.getSeven(),
                DigitTransform.getSeven(), DigitTransform.getOne(), ALMOST_FIRE
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("490867715",verify);
    }
}
