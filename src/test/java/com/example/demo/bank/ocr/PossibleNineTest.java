package com.example.demo.bank.ocr;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PossibleNineTest {

    @Test
    void toStringTest(){
        PossibleNine possibleNine = new PossibleNine();
        possibleNine.addOne(new PossibleByte((byte)1,0.9));
        possibleNine.addOne(new PossibleByte((byte)2,1));

        String result = possibleNine.toString();
        assertEquals("PossibleNine(nineList=[1, 2], percent=0.9)",result);
    }

}
