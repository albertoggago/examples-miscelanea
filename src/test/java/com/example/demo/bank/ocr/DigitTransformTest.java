package com.example.demo.bank.ocr;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DigitTransformTest {

    byte [] odd = {0,1,0,0,0,0,0};
    byte[] oddLength={0,0};


    @Test
    void buildClassTest() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Constructor<DigitTransform> constructor = DigitTransform.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    void equalsBytesOddTest(){
        boolean result = DigitTransform.equalsBytes(oddLength, odd);
        assertFalse(result);
    }

    @Test
    void equalsBytesOdd2Test(){
        boolean result = DigitTransform.equalsBytes(new byte[0], odd);
        assertFalse(result);
    }

    @Test
    void verifyDigitControlOddTest(){
        boolean result = DigitTransform.verifyDigitControl(new int[2]);
        assertFalse(result);
    }

    @Test
    void zeroToByteTest() {
        int zeroReceived = DigitTransform.transformDigitToInt(DigitTransform.getZero());
        assertEquals(0, zeroReceived);
    }

    @Test
    void oneToByteTest() {
        int oneReceived = DigitTransform.transformDigitToInt(DigitTransform.getOne());
        assertEquals(1, oneReceived);
    }

    @Test
    void twoToByteTest() {
        int twoReceived = DigitTransform.transformDigitToInt(DigitTransform.getTwo());
        assertEquals(2, twoReceived);
    }

    @Test
    void threeToByteTest() {
        int threeReceived = DigitTransform.transformDigitToInt(DigitTransform.getThree());
        assertEquals(3, threeReceived);
    }

    @Test
    void fourToByteTest() {
        int fourReceived = DigitTransform.transformDigitToInt(DigitTransform.getFour());
        assertEquals(4, fourReceived);
    }

    @Test
    void fiveToByteTest() {
        int fiveReceived = DigitTransform.transformDigitToInt(DigitTransform.getFive());
        assertEquals(5, fiveReceived);
    }

    @Test
    void sixToByteTest() {
        int sixReceived = DigitTransform.transformDigitToInt(DigitTransform.getSix());
        assertEquals(6, sixReceived);
    }

    @Test
    void sevenToByteTest() {
        int sevenReceived = DigitTransform.transformDigitToInt(DigitTransform.getSeven());
        assertEquals(7, sevenReceived);
    }

    @Test
    void eightToByteTest() {
        int eightReceived = DigitTransform.transformDigitToInt(DigitTransform.getEight());
        assertEquals(8, eightReceived);
    }

    @Test
    void nineToByteTest() {
        int nineReceived = DigitTransform.transformDigitToInt(DigitTransform.getNine());
        assertEquals(9, nineReceived);
    }

    @Test
    void oddToByteTest(){
        int oddReceived = DigitTransform.transformDigitToInt(odd);
        assertEquals(DigitTransform.BYTE_NOT_FOUND, oddReceived);
    }

    @Test
    void oneApproxToByte(){
        List<PossibleByte> possibleByteList = DigitTransform.transformDigitToIntClose(DigitTransform.getOne());
        System.out.println(DigitTransform.draw(DigitTransform.getOne()));
        possibleByteList.forEach(System.out::println);
        assertEquals(1,possibleByteList.getFirst().value());
        assertEquals(1.0,possibleByteList.getFirst().percent());
    }

    @Test
    void oddApproxToByte(){
        List<PossibleByte> possibleByteList = DigitTransform.transformDigitToIntClose(odd);
        System.out.println(DigitTransform.draw(odd));
        possibleByteList.forEach(System.out::println);
        assertEquals(1,possibleByteList.getFirst().value());
        assertNotEquals(1.0,possibleByteList.getFirst().percent());
    }

    @Test
    void verify123456789(){
        byte[][] nineDigits = {DigitTransform.getOne(), DigitTransform.getTwo(), DigitTransform.getThree(),
                               DigitTransform.getFour(), DigitTransform.getFive(), DigitTransform.getSix(),
                               DigitTransform.getSeven(), DigitTransform.getEight(), DigitTransform.getNine(),
                                };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("123456789",verify);
    }

    @Test
    void verify664371495(){
        byte[][] nineDigits = {DigitTransform.getSix(), DigitTransform.getSix(), DigitTransform.getFour(),
                DigitTransform.getThree(), DigitTransform.getSeven(), DigitTransform.getOne(),
                DigitTransform.getFour(), DigitTransform.getNine(), DigitTransform.getFive(),
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("664371485",verify);
    }

    @Test
    void verify457508000(){
        byte[][] nineDigits = {DigitTransform.getFour(), DigitTransform.getFive(), DigitTransform.getSeven(),
                DigitTransform.getFive(), DigitTransform.getZero(), DigitTransform.getEight(),
                DigitTransform.getZero(), DigitTransform.getZero(), DigitTransform.getZero(),
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("457508000",verify);
    }

    @Test
    void verify86110XX36(){
        byte[][] nineDigits = {DigitTransform.getEight(), DigitTransform.getSix(), DigitTransform.getOne(),
                DigitTransform.getOne(), DigitTransform.getZero(), odd,
                odd, DigitTransform.getThree(), DigitTransform.getSix(),
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        // "861101136 AMB [861101138, 861107136]"
        assertTrue(verify.contains("861101136 AMB ["));
        assertTrue(verify.contains("861101138"));
        assertTrue(verify.contains("861107136"));

    }

    @Test
    void verifyOddDigit(){
        byte[][] nineDigits = {DigitTransform.getEight(), DigitTransform.getSix(), DigitTransform.getOne(),
                DigitTransform.getOne(), DigitTransform.getZero(), odd,
                odd, DigitTransform.getOne(), DigitTransform.getTwo(),
        };
        System.out.println(DigitTransform.draw(nineDigits));
        String verify = DigitTransform.getSolution(nineDigits);
        System.out.println(verify);
        assertEquals("861101112 ERR",verify);
    }


}
