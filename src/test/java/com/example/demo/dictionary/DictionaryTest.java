package com.example.demo.dictionary;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DictionaryTest {

    public static final String TEMPORARY = "temporary";

    @Test
    void emptyTest(){
        Dictionary dictionary = new Dictionary();

        Object output = dictionary.transform("");

        assertEquals("", output);
    }

    @Test
    void simpleTransformTest(){
        Dictionary dictionary = new Dictionary();
        dictionary.addDict("temp", TEMPORARY);

        Object output = dictionary.transform("$temp$");

        assertEquals(TEMPORARY, output);
    }

    @Test
    void complexTransformTest(){
        Dictionary dictionary = new Dictionary();
        dictionary.addDict("temp", TEMPORARY);
        dictionary.addDict("name","John Doe");

        Object output = dictionary.transform("$temp$ here comes the name $name$");

        assertEquals("temporary here comes the name John Doe", output);
    }

    @Test
    void complexTransformNotIndicTest(){
        Dictionary dictionary = new Dictionary();
        dictionary.addDict("temp", TEMPORARY);
        dictionary.addDict("name","John Doe");

        Object output = dictionary.transform("$temp$ here comes $notInDic$ the name $name$");

        assertEquals("temporary here comes $notInDic$ the name John Doe", output);
    }
}
