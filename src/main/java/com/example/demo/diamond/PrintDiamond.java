package com.example.demo.diamond;

import java.util.stream.IntStream;

public class PrintDiamond {

    private PrintDiamond(){}
    public static String printDiamond(Character character) {
        if (character == null || character.compareTo('A') < 0 ||  character.compareTo('Z') > 0) return null;
        return IntStream.rangeClosed(0, character.compareTo('A')*2)
                 .mapToObj(cycleRow ->
                    IntStream.rangeClosed(0, character.compareTo('A'))
                             .mapToObj(cycleColum -> (cycleColum == cycleRow ||
                                                      cycleColum == (character.compareTo('A') * 2 - cycleRow))
                                                     ? String.valueOf((char) ('A' + cycleColum)) : " ")
                             .reduce("", (strA, strB) -> strB + ((strA.isEmpty()) ? "" : strA + strB)))
                .reduce("",(rowA,rowB)-> ((!rowA.isEmpty()) ? (rowA+"\n") : "") + rowB);
    }
}
