package com.example.demo.santa;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Present {
    private final String name;
    private final List<String> historicActions;
    private String family;

    Present(SantaActor who, String name) {
        this.name = name;
        this.historicActions = new ArrayList<>();
        addAction(who,null);
        this.family = "";
    }

    Present(SantaActor who, String name, String family) {
        this(who, name);
        this.family = family;
    }

    public void addAction(SantaActor who, String action) {
        if (action !=null){
            this.historicActions.add("(" + action+ ")");
        }
        this.historicActions.add(who.getName());
    }

    @Override
    public String toString() {
        return historicActions.stream().reduce("",(a,b)-> a + (a.isEmpty()?"":" --> ") + b);
    }
}
