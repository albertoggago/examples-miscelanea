package com.example.demo.santa;


import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class MrsClaus implements SantaActor{
    List<Present> pendingPresents = new ArrayList<>();

    public Present getNextPresent() {
        return (pendingPresents.isEmpty())? null:pendingPresents.removeFirst();
    }

    public void addNewPendingPresent(Present present) {
        pendingPresents.add(present);
        present.addAction(this, "Gives Present");
    }

    @Override
    public String getName() {
        return "MrsClaus";
    }
}
