package com.example.demo.santa;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

public class SantaManagerStoryTree extends SantaManagerStoryTwo {

    @Override
    void phaseTwoAssignToMrsClaus(List<Present> presents) {
        presents.stream().sorted(Comparator.comparing(Present::getFamily)).forEach(getMrsClaus()::addNewPendingPresent);
    }

    @Override
    List<Present> phaseOneCreateListPresents() {
        List<Present> presents = new ArrayList<>();
        IntStream.rangeClosed(1,NUMBER_PRESENTS)
                .boxed()
                .map(num-> getToyMachines()
                        .stream()
                        .map(toyMachine -> toyMachine.createPresentWithFamily("New Present "+num))
                        .toList())
                .flatMap(List::stream)
                .forEach(presents::add);
        return presents;
    }
}
