package com.example.demo.santa;

import java.util.ArrayList;
import java.util.List;

public class SantaSleighImpl implements SantaSleigh, SantaActor {

    List<Present> presentList;

    SantaSleighImpl(){
        this.presentList = new ArrayList<>();
    }


    @Override
    public List<Present> getPresents() {
        return presentList;
    }

    @Override
    public void pack(Present present) {
        this.presentList.add(present);
        present.addAction(this, "Packs onto");

    }

    @Override
    public String getName() {
        return "SantaSleigh";
    }
}
