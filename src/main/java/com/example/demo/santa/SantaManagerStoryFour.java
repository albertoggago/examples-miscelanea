package com.example.demo.santa;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class SantaManagerStoryFour extends SantaManagerStoryTree {
    private List<String> badFamilies = new ArrayList<>();
    @Override
    void phaseTwoAssignToMrsClaus(List<Present> presents) {

        presents.stream().filter(present->!getBadFamilies().contains(present.getFamily())).forEach(getMrsClaus()::addNewPendingPresent);
    }


}
