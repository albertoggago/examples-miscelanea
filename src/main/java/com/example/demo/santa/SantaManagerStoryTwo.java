package com.example.demo.santa;


import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Getter
public class SantaManagerStoryTwo {
    private static final int NUM_ELVES = 10;
    static final int NUMBER_PRESENTS = 100;
    static final int NUM_TOY_MACHINES = 5;
    private final List<ToyMachine> toyMachines;
    private final List<Elf> elves;
    private final MrsClaus mrsClaus;
    private final SantaSleigh santaSleigh;

    SantaManagerStoryTwo(){
        this.mrsClaus = new MrsClaus();
        this.toyMachines = IntStream.rangeClosed(1,NUM_TOY_MACHINES).boxed().map(n -> new ToyMachine("Toy Machine "+n)).toList();
        this.elves = IntStream.rangeClosed(1, NUM_ELVES).boxed().map(n -> new Elf("Elf "+n)).toList();
        this.santaSleigh = new SantaSleighImpl();
    }

    synchronized void run()  {
        List<Present> presents = phaseOneCreateListPresents();
        phaseTwoAssignToMrsClaus(presents);
        while(!(getMrsClaus().getPendingPresents().isEmpty() && getElves().stream().allMatch(Elf::readyToPacked))) {
            phaseTreeGivesPresents();
            phaseFourCheckElfFinished();
        }
    }

    List<Present> phaseOneCreateListPresents() {
        List<Present> presents = new ArrayList<>();
        IntStream.rangeClosed(1,NUMBER_PRESENTS)
                 .boxed()
                 .map(num->toyMachines
                         .stream()
                         .map(toyMachine -> toyMachine.createPresent("New Present "+num))
                         .toList())
                 .flatMap(List::stream)
                 .forEach(presents::add);
        return presents;
    }

    void phaseTwoAssignToMrsClaus(List<Present> presents) {
        presents.forEach(mrsClaus::addNewPendingPresent);
    }

    void phaseTreeGivesPresents() {
        elves.stream()
                .filter(Elf::readyToPacked)
                .limit(mrsClaus.getPendingPresents().size())
                .forEach(elf-> elf.packing(mrsClaus.getNextPresent()));
    }

    void phaseFourCheckElfFinished() {
        elves.stream().filter(Elf::packed).forEach(elf -> santaSleigh.pack(elf.unpacking()));
    }
}
