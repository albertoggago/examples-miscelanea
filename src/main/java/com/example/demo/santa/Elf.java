package com.example.demo.santa;

import lombok.Getter;

import java.security.SecureRandom;

@Getter
public class Elf implements SantaActor {
    private static final int TOTAL_STATS = 100;
    private static final int TOTAL_PACKED = 90;
    private final String name;
    private static final SecureRandom random= new SecureRandom();
    private Present present;

    Elf(String name){
        this.name = name;
    }
    public boolean readyToPacked() {
        return present == null;
    }

    public boolean packed()  {
        if (present == null) {
            return false;
        }
        return random.nextInt(TOTAL_STATS)> TOTAL_PACKED ;
    }

    public void packing(Present present) {
        this.present = present;
        present.addAction(this, "Gives Present");
    }

    public Present unpacking() {
        Present presentFree = this.present;
        this.present = null;
        return  presentFree;
    }
}
