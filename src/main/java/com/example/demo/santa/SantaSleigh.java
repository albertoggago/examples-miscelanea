package com.example.demo.santa;


import java.util.List;

public interface SantaSleigh

{


    List<Present> getPresents();

    void pack(Present present);
}
