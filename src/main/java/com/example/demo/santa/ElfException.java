package com.example.demo.santa;

import java.io.Serial;

public class ElfException extends RuntimeException {
    /**
	 * 
	 */
	@Serial
	private static final long serialVersionUID = 1234448588L;

	public ElfException(String err) {
        super(err);
    }
}
