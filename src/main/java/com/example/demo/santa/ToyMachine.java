package com.example.demo.santa;

import lombok.Getter;

import java.security.SecureRandom;

@Getter
public class ToyMachine implements SantaActor{

    private final String name;

    private static final SecureRandom random= new SecureRandom();
    private static final String[] families = {"Family1", "Family2", "Family3", "Family4", "Family5"};

    public ToyMachine(String name) {
        this.name = name;

    }

    public Present createPresent(String namePresent){
        return new Present( this, namePresent);
    }

    public Present createPresentWithFamily(String name) {
        String family = families[random.nextInt(5)];
        return new Present( this, name, family);
    }
}
