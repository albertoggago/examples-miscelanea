package com.example.demo.santa;


import lombok.Getter;

@Getter
public class SantaManagerStoryOne {
    private final ToyMachine toyMachine;
    private final Elf elf;
    private final SantaSleigh santaSleigh;

    SantaManagerStoryOne(){
        this.toyMachine = new ToyMachine("ToyMachine");
        this.elf = new Elf("Sole Elf");
        this.santaSleigh = new SantaSleighImpl();
    }

    synchronized void run()  {
        Present present = phaseOneCreatePresent();
        phaseTwoAssignToElf(present);
        while(elf.getPresent() != null) {
            phaseTreeCheckPacked();
        }
    }

    private Present phaseOneCreatePresent() {
        return toyMachine.createPresent("New Present");
    }

    private void phaseTwoAssignToElf(Present present) {
        if (elf.readyToPacked()) {
            elf.packing(present);
        } else {
            throw new ElfException("Error the Elf is busy");
        }
    }

    private void phaseTreeCheckPacked()  {
        if (elf.packed()) {
            Present present =  elf.unpacking();
            santaSleigh.pack(present);
        }
    }
}
