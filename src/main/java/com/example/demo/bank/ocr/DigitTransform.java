package com.example.demo.bank.ocr;

import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class DigitTransform {

    @Getter
    protected static final byte [] zero = {1,1,1,1,1,1,0};
    @Getter
    protected static final byte [] one = {0,1,1,0,0,0,0};
    @Getter
    protected static final byte [] two = {1,1,0,1,1,0,1};
    @Getter
    protected static final byte [] three = {1,1,1,1,0,0,1};
    @Getter
    protected static final byte [] four = {0,1,1,0,0,1,1};
    @Getter
    protected static final byte [] five = {1,0,1,1,0,1,1};
    @Getter
    protected static final byte [] six = {1,0,1,1,1,1,1};
    @Getter
    protected static final byte [] seven = {1,1,1,0,0,0,0};
    @Getter
    protected static final byte [] eight = {1,1,1,1,1,1,1};
    @Getter
    protected static final byte [] nine = {1,1,1,1,0,1,1};
    public static final byte BYTE_NOT_FOUND = (byte) -128;
    public static final String OVER_LINE = "===";
    private static final double MARGIN_ERROR = 0.0001;


    private static final byte[][] coreTable = {zero, one, two, three, four, five, six, seven, eight, nine};

    private DigitTransform(){}

    public static int transformDigitToInt(byte[] digit) {
        for (byte i = 0; i < coreTable.length; i++) {
           if (equalsBytes(coreTable[i],digit)) {return i;}
        }
        return BYTE_NOT_FOUND;
    }

    public static boolean equalsBytes(byte[] bytes, byte[] digit) {
        if (bytes.length == 0 ) {return false;}
        if (bytes.length != digit.length) {return false;}
        for (byte i=0;i< bytes.length; i++) {
            if (bytes[i] != digit[i]) {return false;}
        }
        return true;
    }


    public static String draw(byte[] digit) {
        return (headLine(digit) + "\n" + firstLine(digit) + "\n" + secondLine(digit) + "\n" + thirdLine(digit) + "\n" + lastLine() + "\n");
    }
    public static String draw(byte[][] digit) {
        return Stream.of(digit).map(DigitTransform::headLine).collect(Collectors.joining())+"\n"
                + Stream.of(digit).map(DigitTransform::firstLine).collect(Collectors.joining())+"\n"
                + Stream.of(digit).map(DigitTransform::secondLine).collect(Collectors.joining())+"\n"
                + Stream.of(digit).map(DigitTransform::thirdLine).collect(Collectors.joining())+"\n"
                + Stream.of(digit).map(i ->lastLine()).collect(Collectors.joining())+"\n";
    }

    private static String headLine(byte[] digit) {
        var value = DigitTransform.transformDigitToInt(digit);
        return " "+(value==DigitTransform.BYTE_NOT_FOUND ?"?":value)+" ";
    }

    private static String firstLine(byte[] digit) {
        return digit[0]==1?" _ ":"   ";
    }

    private static String secondLine(byte[] digit) {
        return (digit[5]==1?"|":" ")+(digit[6]==1?"_":" ")+(digit[1]==1?"|":" ");
    }

    private static String thirdLine(byte[] digit) {
        return (digit[4]==1?"|":" ")+(digit[3]==1?"_":" ")+(digit[2]==1?"|":" ");
    }

    private static String lastLine() {
        return OVER_LINE;
    }

    public static boolean verifyDigitControl(int[] result) {
        if (result.length != 9 ) {return false;}
        int sums = IntStream.rangeClosed(0, 8).reduce(0, (partialInt, i) -> (partialInt + (i + 1) * result[8 - i]));
        return Math.floorMod(sums,11)==0;
    }

    public static String getSolution(byte[][] nineDigits) {

        List<List<PossibleByte>> data = Arrays.stream(nineDigits).map(DigitTransform::transformDigitToIntClose).toList();
        List<PossibleNine> possibleNines = new ArrayList<>();
        var firstPossibleNine = new PossibleNine();
        data.stream().map(List::getFirst).forEach(firstPossibleNine::addOne);
        possibleNines.add(firstPossibleNine);

        for (var a = 0; a < data.size(); a++) {
            for (var deep = 1; deep < data.get(a).size(); deep++) {
                if (Math.abs(data.get(a).get(1).percent() - data.get(a).get(deep).percent()) < MARGIN_ERROR ) {
                    possibleNines.add(createPossibleNine(data, a, deep));
                }
            }
        }
        List<PossibleNine> potentialNines = possibleNines.stream().filter(PossibleNine::verify).sorted(Comparator.comparing(PossibleNine::getPercent).reversed()).toList();
        if (potentialNines.isEmpty()) {
            return firstPossibleNine.transformString() + " ERR";
        } else if (firstPossibleNine.transformString().equals(potentialNines.getFirst().transformString())) {
                return firstPossibleNine.transformString();
        } else if (potentialNines.size()==1){
                return potentialNines.getFirst().transformString();
        } else {
            return firstPossibleNine.transformString()+" AMB ["+potentialNines.stream().map(PossibleNine::transformString).collect(Collectors.joining(", "))+"]";
        }
    }

    private static PossibleNine createPossibleNine(List<List<PossibleByte>> data, int a, int deep) {
        var possibleNine = new PossibleNine();
        for (var i = 0; i < data.size(); i++) {
            possibleNine.addOne(data.get(i).get((a == i) ? deep : 0));
        }
        return possibleNine;
    }

    public static List<PossibleByte> transformDigitToIntClose(byte[] digit) {
        List<PossibleByte> possibleBytes = new ArrayList<>();
        for (byte i = 0; i < coreTable.length; i++) {
            var probability = 0;
            for (byte j = 0; j < coreTable[i].length; j++) {
                probability=probability+(coreTable[i][j]==digit[j]?1:0);
            }
            possibleBytes.add(new PossibleByte(i,probability/7.0));

        }
        return possibleBytes.stream().sorted(Comparator.comparing(PossibleByte::percent).reversed()).toList();
    }

}

