package com.example.demo.bank.ocr;


public record PossibleByte (byte value, double percent){}

