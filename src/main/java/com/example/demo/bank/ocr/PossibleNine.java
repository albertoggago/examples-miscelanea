package com.example.demo.bank.ocr;

import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class PossibleNine {
    List<Byte> nineList;
    @Getter
    double percent;

    public PossibleNine(){
        nineList = new ArrayList<>();
        percent = 1;
    }

    public void addOne(PossibleByte possibleByte) {
        this.nineList.add(possibleByte.value());
        this.percent = this.percent* possibleByte.percent();
    }


    public int[] transformInt() {
        return nineList.stream().mapToInt(Integer::valueOf).toArray();
    }

    public String transformString() {
        return nineList.stream().map(Integer::toString).collect(Collectors.joining());
    }


    public boolean verify() {
        return DigitTransform.verifyDigitControl(this.transformInt());
    }
}
