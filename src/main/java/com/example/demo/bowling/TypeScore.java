package com.example.demo.bowling;

public enum TypeScore {
    SPARE, STRIKE, FAIL
}
