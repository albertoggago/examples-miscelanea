package com.example.demo.bowling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BowlsManager {

    public static final int NUMBER_LINES = 10;
    public static final int NUMBER_TRIES = 2;
    public static final int FIRST_BOWL = 1;
    public static final int LAST_BOWL = 10;
    public static final int TOTAL_BOWLS = LAST_BOWL - FIRST_BOWL + 1;

    private BowlsManager(){}

    protected static Integer[] clean(Integer[] bowls) {
        return Arrays.stream(bowls).filter(a->a >=BowlsManager.FIRST_BOWL).filter(a->a <=BowlsManager.LAST_BOWL).sorted().distinct().toArray(Integer[]::new);
    }

    protected static  boolean validate(Integer[] bowls) {
        Integer[] bowlsCleaned = clean(bowls);
        return (Arrays.equals(bowlsCleaned, bowls));
    }

    public static Integer[] cleanWithPrev(Integer[] bowls, Integer[] prevBowls) {
        List<Integer> prevBowlsArray = Arrays.asList(prevBowls);
        return Arrays.stream(clean(bowls)).filter(bowl -> !prevBowlsArray.contains(bowl)).toArray(Integer[]::new);

    }

    public static Score score(Integer[] firstTry, Integer[] secondTry) {

        int downBowls = firstTry.length + secondTry.length;
        if (downBowls == BowlsManager.TOTAL_BOWLS ) {
            if (firstTry.length == BowlsManager.TOTAL_BOWLS) {
                return new Score(TypeScore.STRIKE, firstTry.length, secondTry.length);
            } else {
                return new Score(TypeScore.SPARE, firstTry.length, secondTry.length);
            }
        } else {
            return new Score(TypeScore.FAIL, firstTry.length, secondTry.length);
        }
    }

    public static int score(Score scorePrev, int nextScore, int next2Score) {

        if (scorePrev.getType() == TypeScore.SPARE) {
            return nextScore;
        } else if (scorePrev.getType() == TypeScore.STRIKE) {
            return nextScore+next2Score;
        }
        return 0;
    }

    public static Score clone(Score score) {
        return new Score(score.getType(), score.getFirstPoints(), score.getSecondPoints(), score.getExtraPoints());
    }

    public static TurnBowling clone(TurnBowling turnBowling) {
        return new TurnBowling(BowlsManager.clone(turnBowling.getTries()), BowlsManager.clone(turnBowling.getScore()));
    }

    private static List<Integer[]> clone(List<Integer[]> tries) {
        return new ArrayList<>(tries);
    }

    public static LineBowling clone(LineBowling lineBowling) {
        return new LineBowling(lineBowling.getTurns().stream().map(BowlsManager::clone).toList());
    }
}
