package com.example.demo.bowling;

import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Data
@Getter
public class LineBowling {
    private List<TurnBowling> turns;

    LineBowling(){
        turns = Stream.generate(TurnBowling::new).limit(10).toList();
    }

    public LineBowling(List<TurnBowling> turns) {
        setTurns(turns);
        IntStream.rangeClosed(2,turns.size()).boxed().sorted(Collections.reverseOrder())
                .forEach(i -> turns.get(i-2)
                        .scoringExtra(turns.subList(i-1,Math.min(i+1,turns.size()))));
    }

    public void setTurns(List<TurnBowling> turns){
        if (turns.size() == BowlsManager.NUMBER_LINES ) {
            this.turns = turns;
        } else if (turns.size() == BowlsManager.NUMBER_LINES+1 &&
                   turns.get(BowlsManager.NUMBER_LINES-1).getScore().getType() != TypeScore.FAIL) {
            this.turns = turns;
            this.turns.get(turns.size()-1).setExtra(true);
        } else {
            this.turns = new ArrayList<>();
        }
    }

    public boolean validate() {
        boolean validateStatus = false;
        if (turns.size() >= BowlsManager.NUMBER_LINES) {
            validateStatus = turns.stream().allMatch(TurnBowling::validate);
            if(turns.size() > BowlsManager.NUMBER_LINES &&
               turns.get(turns.size()-2).getScore().getType().equals(TypeScore.FAIL)) {
                validateStatus = false;
            }
        }
        return validateStatus;
    }

    public int totalScore(){
        return turns.stream().filter(a->!a.isExtra()).map(TurnBowling::getScore).map(Score::getPoints).reduce(0, Integer::sum);
    }

    @Override
    public String toString() {
        String textTurns = turns.stream().map(TurnBowling::toString).reduce("", (prevS, newS) -> prevS + " " + newS);
        return textTurns.trim() + "\n" + "Total Points: " + totalScore() + "\n";
    }
}
