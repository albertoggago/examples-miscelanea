package com.example.demo.bowling;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
public class Score {
    private final TypeScore type;
    private final int firstPoints;
    private final int secondPoints;
    @Setter
    private int extraPoints;

    public Score(TypeScore type, int firstPoints, int secondPoints, int extraPoints) {
        this.type = type;
        this.firstPoints = firstPoints;
        this.secondPoints = secondPoints;
        this.extraPoints = extraPoints;
    }

    public int getPoints() {
        return this.firstPoints+this.secondPoints+this.extraPoints;
    }
    Score(TypeScore type, int firstPoints, int secondPoints ){
        this.type = type;
        this.firstPoints = firstPoints;
        this.secondPoints = secondPoints;
        this.extraPoints = 0;
    }

    public Score() {
        this(TypeScore.FAIL,0,0);
    }


}
