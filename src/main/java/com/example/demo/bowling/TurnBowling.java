package com.example.demo.bowling;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

@Data
@Getter
public class TurnBowling {
    List<Integer[]> tries;
    Score score;
    @Setter
    boolean isExtra;

    TurnBowling(){
        tries = new ArrayList<>();
        IntStream.rangeClosed(1,BowlsManager.NUMBER_TRIES).forEach(i->tries.add(new Integer[]{}));
        this.scoring();
        this.isExtra = false;
    }
    TurnBowling(Integer[] first, Integer[] second) {
        tries = new ArrayList<>();
        tries.add(BowlsManager.clean(first));
        tries.add(BowlsManager.cleanWithPrev(second, tries.getFirst()));
        this.scoring();
        this.isExtra = false;
    }

    public TurnBowling(List<Integer[]> tries, Score score) {
        this.tries = tries;
        this.score = score;
    }

    public boolean validate(){
        return (tries.size() == BowlsManager.NUMBER_TRIES) &&
                BowlsManager.validate(tries.get(0)) &&
                BowlsManager.validate(tries.get(1));
    }
    public void scoring(){
        this.score = BowlsManager.score(getTries().get(0),getTries().get(1));
    }

    public void scoringExtra(List<TurnBowling> turns ) {
        List<Integer[]> nextTurns = turns.stream().map(TurnBowling::getTries).flatMap(List::stream).toList();
        int nextPoints = nextTurns.get(0).length;
        int next2Points = nextTurns.get(1).length;
        if (nextPoints == BowlsManager.TOTAL_BOWLS) {
            if (nextTurns.size() > 2) {
                  next2Points = nextTurns.get(2).length;
              } else {
                  next2Points = BowlsManager.TOTAL_BOWLS;
              }
        }
        this.score.setExtraPoints(BowlsManager.score(this.getScore(), nextPoints, next2Points));
    }

    public void setTries(List<Integer[]> tries) {
        this.tries = tries;
        if(this.validate()) {
            this.scoring();
        }
    }

    @Override
    public String toString() {
        if (!validate()) {
            return "ERROR";
        }
        if (isExtra()){
            return switch (this.getScore().getType()) {
                case STRIKE -> "X";
                case SPARE -> score.getFirstPoints() + "/";
                default -> String.valueOf(score.getPoints());
            };
        }
        return switch (this.getScore().getType()) {
            case STRIKE -> "X";
            case SPARE -> score.getFirstPoints() + "/";
            default -> score.getPoints() + "-";
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null ) return false;
        if (getClass() != o.getClass()) return false;
        TurnBowling turnBowling = (TurnBowling) o;
        return score.equals(turnBowling.score) &&
                Arrays.equals(tries.get(0), turnBowling.tries.get(0)) &&
                Arrays.equals(tries.get(1), turnBowling.tries.get(1));
    }

    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(tries.get(0)), Arrays.hashCode(tries.get(1)), score, isExtra);
    }
}
