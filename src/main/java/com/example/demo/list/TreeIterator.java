package com.example.demo.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


public class TreeIterator<T extends Comparable<T>> implements Iterator<T> {
    private final List<TreeList<T>> treeListInternal;
    private final List<T> treeListPrev;
    public TreeIterator(TreeList<T> treeList) {

        this.treeListInternal = new ArrayList<>();
        if(treeList.getElement() != null) this.treeListInternal.addFirst( treeList);
        this.treeListPrev = new ArrayList<>();
    }

    @Override
    public boolean hasNext() {
        return !treeListInternal.isEmpty();
    }

    @Override
    public T next() {
        if (hasNext()) {
            TreeList<T> treeListToAnalyse = treeListInternal.removeFirst();
            if (treeListToAnalyse.getRightElement() != null) treeListInternal.addFirst( treeListToAnalyse.getRightElement());
            if (treeListToAnalyse.getLeftElement() == null) {
                treeListPrev.addFirst( treeListToAnalyse.getElement());
                return treeListToAnalyse.getElement();
            } else {
                treeListInternal.addFirst( new TreeList<>(treeListToAnalyse.getElement()));
                treeListInternal.addFirst( treeListToAnalyse.getLeftElement());
                return next();
            }
        }
        throw new NoSuchElementException("Not element in QueueList");
    }

    public boolean hasPrev() {
        return !treeListPrev.isEmpty() ;
    }

    public T prev() {
        if (hasPrev()) {
            T treeListToAnalyse = treeListPrev.removeFirst();
            treeListInternal.addFirst( new TreeList<>(treeListToAnalyse));
            return treeListToAnalyse;
        }
        throw new NoSuchElementException("Not element in prev QueueList");
    }
}
