package com.example.demo.list;

import static com.example.demo.list.TreeBuilder.*;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.Iterator;
import java.util.Objects;

import jakarta.validation.constraints.NotNull;

import static java.lang.Math.max;

@Getter
public class TreeList<T extends Comparable<T>> implements ListDiff<T>, Iterable<T> {
    private TreeList<T> leftElement;
    private TreeList<T> rightElement;
    @Setter
    private T element;
    @Setter
    private long deep;
    @Setter
    private long length;


    public TreeList() {

        this.deep = 1;
        this.length = 0;
    }

    public TreeList(TreeList<T> left, TreeList<T> right, T element) {
        this.leftElement = left;
        this.rightElement = right;
        this.element = element;
        this.deep = calculateDeep();
        this.length= (getElement()==null) ? 0 : 1 + lengthBranch(getLeftElement())+lengthBranch(getRightElement());
    }

    public TreeList(T element) {
        this( null, null, element);
    }

    protected long calculateDeep(){
        return (1+max(deepBranch(getLeftElement()), deepBranch(getRightElement())));
    }


    @Override
    public void setRightElement(ListDiff<T> rightElement) {
        this.rightElement = (TreeList<T>) rightElement;
    }

    @Override
    public void setLeftElement(ListDiff<T> leftElement) {
        this.leftElement = (TreeList<T>) leftElement;
    }

    @Override
    public Typist getType() {
        return Typist.TREE_LIST;
    }

    @Override
    public boolean isEmpty() {
        return (this.getElement() == null) && (this.getLeftElement() == null)&& (this.getRightElement() == null);
    }


    protected T removeHigher(TreeList<T> returnElement, TypeBranch type) {
        if (this.getRightElement() == null ) {
            if (TypeBranch.RIGHT.equals(type)) {
                returnElement.setRightElement(this.getLeftElement());
            } else {
                returnElement.setLeftElement(this.getLeftElement());
            }
            return this.getElement();
        } else {
            this.setLength(this.getLength() - 1);
            var newElement = this.getRightElement().removeHigher(this,TypeBranch.RIGHT);
            this.setDeep(this.calculateDeep());
            return newElement;
        }
    }

    protected T removeLower(TreeList<T> returnElement, TypeBranch type) {
        if (this.getLeftElement() == null ) {
            if (TypeBranch.RIGHT.equals(type)) {
                returnElement.setRightElement(this.getRightElement());
            } else {
                returnElement.setLeftElement(this.getRightElement());
            }
            return this.getElement();
        } else {
            this.setLength(this.getLength() - 1);
            var newElement = this.getLeftElement().removeLower(this, TypeBranch.LEFT);
            this.setDeep(this.calculateDeep());
            return newElement;
        }
    }

    long deepBranch(TreeList<T> treeList) {
        return (treeList == null)?0: treeList.getDeep();
    }

    @Override
    public TypeQuality verifyQuality() {
        var success = (this.getElement() != null
                                   || this.getLeftElement() == null && this.getRightElement() == null)
                ? TypeQuality.OK:TypeQuality.ERROR_ROOT;
        if (TypeQuality.OK.equals(success) && !this.verifySumLengths()) { success = TypeQuality.ERROR_LENGTH;}
        if (TypeQuality.OK.equals(success) && !this.verifyDeeps()) { success = TypeQuality.ERROR_DEEP;}
        if (TypeQuality.OK.equals(success) && !this.verifyOrder())    {success = TypeQuality.ERROR_ORDER;}
        if (TypeQuality.OK.equals(success) && this.getLeftElement()!=null
             && !(TypeQuality.OK.equals(this.getLeftElement().verifyQuality()))) {
            success = TypeQuality.ERROR_LEFT_BRANCH;
        }
        if (TypeQuality.OK.equals(success) && this.getRightElement()!=null
             && !(TypeQuality.OK.equals(this.getRightElement().verifyQuality()))) {
            success = TypeQuality.ERROR_RIGHT_BRANCH;
        }
        return success;
    }

    private boolean verifyOrder() {
        if (this.getLeftElement() != null) {
            var leftElementLast = getLast(this.getLeftElement());
            if (leftElementLast != null && leftElementLast.compareTo(this.getElement()) > 0) {
                    return false;
            }
        }
        if (this.getRightElement() != null) {
            var rightElementFirst = getFirst(this.getRightElement());
            return rightElementFirst == null || this.getElement().compareTo(rightElementFirst) <= 0;
        }
        return true;
    }


    private boolean verifySumLengths(){
        return this.getLength() == ( lengthBranch(this.getLeftElement())
                                     + lengthBranch(this.getRightElement())
                                     + ((this.getElement()==null)?0:1) );
    }
    boolean verifyDeeps(){
        return this.getDeep() == this.calculateDeep();
    }



    @Override
    public String toString() {
        return "// " + ((this.getLeftElement() == null)?"*":this.getLeftElement())
                + " (" + this.getDeep() + ")" + this.getElement() + "(" + this.getLength() +    ") "
                + ((this.getRightElement()==null)?"*":this.getRightElement()) + " \\\\";
    }

    public ListDiff<T> createVoidElement(T newObject) {
        return new TreeList<>(null, null, newObject);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null ) return false;
        if (getClass() != o.getClass()) return false;
        @SuppressWarnings("unchecked")
		TreeList<T> treeList = (TreeList<T>) o;
        if (treeList.getElement() != null && this.getElement() == null ) return false;
        return this.getLength() == treeList.getLength() &&
                this.getDeep() == treeList.getDeep() &&
                (this.getElement() == null || this.getElement().equals(treeList.getElement())) &&
                (this.getLeftElement() == null || this.getLeftElement().equals(treeList.getLeftElement())) &&
                (this.getRightElement() == null || this.getRightElement().equals(treeList.getRightElement()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getElement(), this.getLeftElement(), this.getRightElement());
    }

    @NonNull
    @Override
    public Iterator<T> iterator() {
        return new TreeIterator<>(this);
    }

    @NotNull
    public Iterator<T> iteratorSoft() {
        return new TreeIteratorSoft<>(this);
    }
}



