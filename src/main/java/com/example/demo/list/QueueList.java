package com.example.demo.list;

import jakarta.annotation.Nonnull;
import lombok.Getter;
import lombok.Setter;

import java.util.Iterator;
import java.util.Objects;

@Getter
public class QueueList<T extends Comparable<T>> implements ListDiff<T>, Iterable<T> {
    private QueueList<T> nextElement;
    @Setter
    private T element;
    @Setter
    private long deep;

    QueueList(){
        this.deep = 0L;
    }

    QueueList(QueueList<T> queueList, T obj){
        this.nextElement = queueList;
        this.element = obj;
        if (obj == null) {
            this.deep = 0;
        } else {
            this.deep = (queueList == null) ? 1 : queueList.getLength() + 1;
        }
    }

    @Override
    public void setRightElement(ListDiff<T> rightElement) {
        this.setNextElement(rightElement);
    }

    @Override
    public void setLeftElement(ListDiff<T> leftElement) {
        this.setNextElement(leftElement);

    }

    @Override
    public Typist getType() {
        return Typist.QUEUE_LIST;
    }

    @Override
    public long getLength() {
        return getDeep();
    }

    @Override
    public boolean isEmpty() {
        return getElement() == null && getNextElement() == null;
    }


    @Override
    public TypeQuality verifyQuality() {
        if (this.getNextElement() == null) {
            if (this.getDeep() == (this.getElement() == null?0:1)) {
                return TypeQuality.OK;
            } else {
                return TypeQuality.ERROR_DEEP;
            }
        } else {
            var result = ((this.getNextElement().getLength()+1) == this.getDeep())?TypeQuality.OK:TypeQuality.ERROR_DEEP;

            if ((TypeQuality.OK.equals(result)) && (this.getElement() != null && this.getNextElement().getElement() != null)) {
                    result = this.getNextElement().verifyQuality();
                }
            return result;
        }
    }

    T removeLast(QueueList<T> prev){
        if (this.getNextElement() == null) {
                var oldElement = this.getElement();
                prev.setNextElement( null);
                return oldElement;
        } else {
            this.setDeep(this.getDeep()-1);
            return this.getNextElement().removeLast(this);
        }
    }
    public String toString() {
        return "Number " +
                this.getDeep() +
                (this.getElement() != null ? ", Element " +this.getElement() : ", No Element") +
                (this.getNextElement() != null ? "; " + this.getNextElement() : "");

    }

    public boolean removeObject(T object) {
        if (object == this.getElement()) {
            QueueBuilder.removeFirst(this);
            return true;
        } else if (this.getNextElement() == null) {
            return false;
        } else {
            var result = this.getNextElement().removeObject(object);
            if (result){this.setDeep(this.getDeep()-1);}
            return result;
        }
    }

    public void setNextElement(ListDiff<T> t1) {
        this.nextElement = (QueueList<T>) t1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null ) return false;
        if (getClass() != o.getClass()) return false;
        @SuppressWarnings("unchecked")
		QueueList<T> queueList = (QueueList<T>) o;
        if (queueList.getElement() != null && this.getElement() == null ) return false;
        return this.getDeep() == queueList.getDeep() &&
               (this.getElement() == null || this.getElement().equals(queueList.getElement())) &&
               (this.getNextElement() == null || this.getNextElement().equals(queueList.getNextElement()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getElement(), this.getNextElement());
    }

    @Nonnull
    @Override
    public Iterator<T> iterator() {
        return new QueueIterator<>(this);
    }
}