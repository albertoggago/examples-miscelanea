package com.example.demo.list;

class BalancedTreeBuilder extends TreeBuilder {
    private BalancedTreeBuilder() {
        super();
    }

    public static <T extends Comparable<T>> BalancedTreeList<T> addLast(BalancedTreeList<T> balancedTreeList, T element) {
        TreeBuilder.addLast(balancedTreeList, element);
        balancer(balancedTreeList);
        return balancedTreeList;
    }

    public static <T extends Comparable<T>> BalancedTreeList<T> addFirst(BalancedTreeList<T> balancedTreeList, T element) {
        TreeBuilder.addFirst(balancedTreeList, element);
        balancer(balancedTreeList);
        return balancedTreeList;
    }

    public static <T extends Comparable<T>> BalancedTreeList<T> addOrdered(BalancedTreeList<T> balancedTreeList, T element) {
        TreeBuilder.addOrdered(balancedTreeList, element);
        return balancer(balancedTreeList);
    }

    public static <T extends Comparable<T>> BalancedTreeList<T> balancer(BalancedTreeList<T> balancedTreeList) {
        while (!ListDiff.TypeQuality.OK.equals(balancedTreeList.verifyQuality())) {
            while (ListDiff.TypeQuality.ERROR_LEFT_BRANCH.equals(balancedTreeList.verifyQuality())) {
                balancedTreeList.setLeftElement(balancer(balancedTreeList.getLeftElement()));
                balancedTreeList.setDeep(balancedTreeList.calculateDeep());
            }
            while (ListDiff.TypeQuality.ERROR_RIGHT_BRANCH.equals(balancedTreeList.verifyQuality())) {
                balancedTreeList.setRightElement(balancer(balancedTreeList.getRightElement()));
                balancedTreeList.setDeep(balancedTreeList.calculateDeep());
            }
            while (balancedTreeList.verifyErrorBalance()) {
                if (balancedTreeList.deepBranch(balancedTreeList.getLeftElement()) >
                        balancedTreeList.deepBranch(balancedTreeList.getRightElement())) {
                    moveToRight(balancedTreeList);
                    balancer(balancedTreeList);
                } else {
                    moveToLeft(balancedTreeList);
                    balancer(balancedTreeList);
                }
            }
        }
        return balancedTreeList;
    }


    public static <T extends Comparable<T>> void moveToRight(BalancedTreeList<T> balancedTreeList) {

        if (balancedTreeList.getRightElement() == null) {
            balancedTreeList.setRightElement(balancedTreeList.createVoidElement(balancedTreeList.getElement()));
        } else {
            addOrdered(balancedTreeList.getRightElement(), balancedTreeList.getElement());
        }
        balancedTreeList.setElement(balancedTreeList.getLeftElement()
                                    .removeHigher(balancedTreeList, TypeBranch.LEFT));
        balancer(balancedTreeList.getLeftElement());
        balancedTreeList.setDeep(balancedTreeList.calculateDeep());
    }

    public static <T extends Comparable<T>> void moveToLeft(BalancedTreeList<T> balancedTreeList) {
        if (balancedTreeList.getLeftElement() == null) {
            balancedTreeList.setLeftElement(balancedTreeList.createVoidElement(balancedTreeList.getElement()));
        } else {
            addOrdered(balancedTreeList.getLeftElement(), balancedTreeList.getElement());
        }
        balancedTreeList.setElement(balancedTreeList.getRightElement()
                .removeLower(balancedTreeList, TypeBranch.RIGHT));
        balancer(balancedTreeList.getRightElement());
        balancedTreeList.setDeep(balancedTreeList.calculateDeep());
    }

    public static <T extends Comparable<T>> T removeLast(BalancedTreeList<T> balancedTreeList) {
        var removed = TreeBuilder.removeLast(balancedTreeList);
        BalancedTreeBuilder.balancer(balancedTreeList);
        return removed;
    }

    public static <T extends Comparable<T>> T removeFirst(BalancedTreeList<T> balancedTreeList) {
        var removed = TreeBuilder.removeFirst(balancedTreeList);
        BalancedTreeBuilder.balancer(balancedTreeList);
        return removed;
    }

    public static <T extends Comparable<T>> boolean removeObject(BalancedTreeList<T> balancedTreeList, T t1) {
        var result = TreeBuilder.removeObject(balancedTreeList, t1);
        BalancedTreeBuilder.balancer(balancedTreeList);
        return result;
    }
}

