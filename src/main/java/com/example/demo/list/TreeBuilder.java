package com.example.demo.list;


public class TreeBuilder {

    TreeBuilder() {}

    public static <T extends Comparable<T>> T getFirst(TreeList<T> treeList) {
        return (treeList.getLeftElement() == null)? treeList.getElement() : getFirst(treeList.getLeftElement());
    }

    public static <T extends Comparable<T>> T getLast(TreeList<T> treeList) {
        return (treeList.getRightElement() == null) ? treeList.getElement() : getLast(treeList.getRightElement());
    }

    public static <T extends Comparable<T>> T get(TreeList<T> treeList, long i) {
        var total = 0L;
        if (treeList.getLeftElement() != null) {
            if (treeList.getLeftElement().getLength() >= i ) {
                return get(treeList.getLeftElement(), i);
            } else {
                total = treeList.getLeftElement().getLength();
            }
        }
        if ( i == (total +1)) {
            return treeList.getElement();
        }
        if (treeList.getRightElement() != null) {
            return get(treeList.getRightElement(), i - total -1);
        }
        return null;
    }
    public static <T extends Comparable<T>> TreeList<T> addLast(TreeList<T> treeList, T t1) {
        return addOrdered(treeList, t1);
    }
    public static <T extends Comparable<T>> TreeList<T> addFirst(TreeList<T> treeList, T t1) {
        return addOrdered(treeList, t1);
    }

    public static <T extends Comparable<T>> TreeList<T> addOrdered(TreeList<T> treeList, T newObject) {
        treeList.setLength(treeList.getLength()+1);
        if (treeList.getElement() == null ) {
            treeList.setElement(newObject);
        } else {
            if (treeList.getElement().compareTo(newObject) > 0) {
                if (treeList.getLeftElement() == null) {
                    treeList.setLeftElement(treeList.createVoidElement(newObject));
                } else {
                    addOrdered(treeList.getLeftElement(), newObject);
                }
            }  else {
                if (treeList.getRightElement() == null) {
                    treeList.setRightElement(treeList.createVoidElement(newObject));
                } else {
                    addOrdered(treeList.getRightElement(), newObject);
                }
            }
            treeList.setDeep(treeList.calculateDeep());
        }
        return treeList;
    }

    public static <T extends Comparable<T>> T removeLast(TreeList<T> treeList) {
        if (treeList.getElement() == null ) {
            return null;
        } else if (treeList.getRightElement() == null) {
            var removed = treeList.getElement();
            if (treeList.getLeftElement() == null) {
                treeList.setElement(null);
            } else {
                treeList.setElement(treeList.getLeftElement().getElement());
                treeList.setRightElement(treeList.getLeftElement().getRightElement());
                treeList.setLeftElement(treeList.getLeftElement().getLeftElement());
                treeList.setDeep(treeList.getDeep() - 1);
            }
            treeList.setLength(treeList.getLength() - 1);
            treeList.setDeep(treeList.calculateDeep());
            return removed;
        } else {
            var removed = treeList.getRightElement().removeHigher(treeList, TypeBranch.RIGHT);
            treeList.setLength(treeList.getLength() - 1);
            treeList.setDeep(treeList.calculateDeep());
            return removed;
        }
    }

    public static <T extends Comparable<T>> T removeFirst(TreeList<T> treeList) {
        if (treeList.getElement() == null ) {
            return null;
        } else if (treeList.getLeftElement() == null) {
            var removed = treeList.getElement();
            if (treeList.getRightElement() == null) {
                treeList.setElement(null);
            } else {
                treeList.setElement(treeList.getRightElement().getElement());
                treeList.setLeftElement(treeList.getRightElement().getLeftElement());
                treeList.setRightElement(treeList.getRightElement().getRightElement());
                treeList.setDeep(treeList.getDeep() - 1 );
            }
            treeList.setLength(treeList.getLength() - 1);
            treeList.setDeep(treeList.calculateDeep());
            return removed;
        } else {
            var removed = treeList.getLeftElement().removeLower(treeList, TypeBranch.LEFT);
            treeList.setLength(treeList.getLength() - 1);
            treeList.setDeep(treeList.calculateDeep());
            return removed;
        }
    }
    public static <T extends Comparable<T>> boolean removeObject(TreeList<T> treeList, T t1) {
        if (treeList.getElement() == null){return false;}
        if (t1.compareTo(treeList.getElement())==0) {
            matchedToRemove(treeList);
            return true;
        }
        if (t1.compareTo(treeList.getElement())<0) {
            return checkInLeftBranch(treeList, t1);
        } else {
            return checkInRightBranch(treeList, t1);
        }
    }

    private static <T extends Comparable<T>> void matchedToRemove(TreeList<T> treeList) {
        treeList.setLength(treeList.getLength() - 1);
        if (treeList.getLeftElement() == null) {
            if (treeList.getRightElement() == null ) {
                treeList.setElement(null);
            } else {
                treeList.setElement(treeList.getRightElement().getElement());
                treeList.setLeftElement(treeList.getRightElement().getLeftElement());
                treeList.setRightElement(treeList.getRightElement().getRightElement());
                treeList.setDeep(treeList.getDeep()-1);
            }
        } else {
            var branchToMerge = treeList.getRightElement();
            treeList.setDeep(treeList.getDeep()-1);
            treeList.setLength(treeList.getLength() - lengthBranch(branchToMerge));
            treeList.setElement(treeList.getLeftElement().getElement());
            treeList.setRightElement(treeList.getLeftElement().getRightElement());
            treeList.setLeftElement(treeList.getLeftElement().getLeftElement());
            for (long i = lengthBranch(branchToMerge);i >0; i--){
                addOrdered(treeList, get(branchToMerge, i));
            }
        }
    }

    private static <T extends Comparable<T>> boolean checkInLeftBranch(TreeList<T> treeList, T t1) {
        if(treeList.getLeftElement() == null) {
            return false;
        }
        else {
            var hasRemoved = removeObject(treeList.getLeftElement(), t1);
            if (hasRemoved) {
                treeList.setLength(treeList.getLength() - 1);
                treeList.setDeep(treeList.calculateDeep());
            }
            return hasRemoved;
        }
    }

    private static <T extends Comparable<T>> boolean checkInRightBranch(TreeList<T> treeList, T t1) {
        if(treeList.getRightElement() == null) {
            return false;
        }
        else {
            var hasRemoved = removeObject(treeList.getRightElement(), t1);
            if (hasRemoved) {
                treeList.setLength(treeList.getLength() - 1);
                treeList.setDeep(treeList.calculateDeep());
            }
            return hasRemoved;
        }
    }

    static <T extends Comparable<T>> long lengthBranch(TreeList<T> treeList) {
        return (treeList==null)?0:treeList.getLength();
    }
}
