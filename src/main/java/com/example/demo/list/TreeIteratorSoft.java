package com.example.demo.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


public class TreeIteratorSoft<T extends Comparable<T>> implements Iterator<T> {
    private List<TreeList<T>> currentTrees;
    private List<TreeList<T>> nextTrees;
    private final List<T> prevElements;
    public TreeIteratorSoft(TreeList<T> treeList) {

        this.currentTrees = new ArrayList<>();
        if(treeList.getElement() != null) this.currentTrees.add(treeList);
        this.nextTrees = new ArrayList<>();
        this.prevElements = new ArrayList<>();
    }

    @Override
    public boolean hasNext() {
        return !(currentTrees.isEmpty() && nextTrees.isEmpty()) ;
    }

    @Override
    public T next() {
        if (hasNext()) {
            if(this.currentTrees.isEmpty()) {
                this.currentTrees = this.nextTrees;
                this.nextTrees = new ArrayList<>();
            }
            TreeList<T> treeListToAnalyse = this.currentTrees.removeFirst();
            if (treeListToAnalyse.getLeftElement() != null) nextTrees.add(treeListToAnalyse.getLeftElement());
            if (treeListToAnalyse.getRightElement() != null) nextTrees.add(treeListToAnalyse.getRightElement());

            this.prevElements.addFirst(treeListToAnalyse.getElement());
            return treeListToAnalyse.getElement();
        }
        throw new NoSuchElementException("Not element in QueueList");
    }

    public boolean hasPrev() {
        return !prevElements.isEmpty() ;
    }

    public T prev() {
        if (hasPrev()) {
            T treeListToAnalyse = prevElements.removeFirst();
            this.currentTrees.addFirst( new TreeList<>(treeListToAnalyse));
            return treeListToAnalyse;
        }
        throw new NoSuchElementException("Not element in prev QueueList");
    }
}
