package com.example.demo.list;

class OrderBuilder extends QueueBuilder {
    private OrderBuilder(){
        super();
    }

    public static <T extends Comparable<T>> OrderList<T> addLast(OrderList<T> orderList, T newElement) {
        return addOrdered(orderList, newElement);
    }

    public static <T extends Comparable<T>> OrderList<T> addFirst(OrderList<T> orderList, T newElement) {
        return addOrdered(orderList, newElement);
    }

    public static <T extends Comparable<T>> OrderList<T> addOrdered(OrderList<T> orderList, T newElement) {
        if (orderList.getElement() == null) {
            orderList.setElement(newElement);
        } else {
            T objectLeft = (orderList.getElement().compareTo(newElement) >= 0) ? newElement : orderList.getElement();
            T objectRight  = (orderList.getElement().compareTo(newElement) >= 0) ? orderList.getElement() : newElement;

            orderList.setElement(objectLeft);
            if (orderList.getNextElement() == null) {
                orderList.setNextElement(new OrderList<>());
                orderList.getNextElement().setElement(objectRight);
                orderList.getNextElement().setDeep(1);
            } else {
                addOrdered(orderList.getNextElement(), objectRight);
            }
        }
        orderList.setDeep(orderList.getDeep()+1);
        return orderList;
    }
}
