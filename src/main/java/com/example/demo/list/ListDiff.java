package com.example.demo.list;

public interface ListDiff<T extends Comparable<T>> {
    Typist getType();
    long getLength();
    long getDeep();
    boolean isEmpty();

    TypeQuality verifyQuality();
    void setRightElement(ListDiff<T> leftElement);
    void setLeftElement(ListDiff<T> leftElement);

    enum TypeQuality {
        OK,
        ERROR_ROOT,
        ERROR_LENGTH, ERROR_DEEP, ERROR_BALANCED, ERROR_ORDER, ERROR_LEFT_BRANCH, ERROR_RIGHT_BRANCH
    }

    enum Typist {
        ORDERED_LIST, QUEUE_LIST, TREE_LIST, BALANCED_TREE_LIST, ND
    }
}
