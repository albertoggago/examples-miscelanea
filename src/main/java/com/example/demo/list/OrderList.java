package com.example.demo.list;

import lombok.Getter;
import lombok.Setter;


@Getter
public class OrderList<T extends Comparable<T>> extends QueueList<T> implements ListDiff<T>, Iterable<T> {
    private OrderList<T> nextElement;
    @Setter
    private T element;
    @Setter
    private long deep;

    OrderList(){
        this.deep = 0L;
    }

    OrderList(OrderList<T> orderList, T obj){
        this.nextElement = orderList;
        this.element = obj;
        if (obj == null) {
            this.deep = 0;
        } else {
            this.deep = (orderList == null) ? 1 : orderList.getLength() + 1;
        }
    }

    @Override
    public Typist getType() {
        return Typist.ORDERED_LIST;
    }

    @Override
    public TypeQuality verifyQuality() {
        var error = super.verifyQuality();
        if (!((this.getDeep() <=1 )  || (this.getElement().compareTo(this.getNextElement().getElement())<=0))) {
            error = TypeQuality.ERROR_ORDER;
        }
        return error;
    }

    @Override
    public void setNextElement(ListDiff<T> t1) {
        this.nextElement = (OrderList<T>) t1;
    }

    @Override
    public boolean equals(Object o) { return super.equals(o); }

    @Override
    public int hashCode() { return super.hashCode(); }
}
