package com.example.demo.list;

public class ListBuilder {

    private ListBuilder() {
    }

    public static <T extends Comparable<T>> ListDiff<T> clone(ListDiff<T> listInput) {
        if (listInput != null) {
            switch (listInput.getType()) {
                case QUEUE_LIST -> {
                    QueueList<T> queueList = (QueueList<T>) listInput;

                    QueueList<T> queueListNew = new QueueList<>();
                    queueListNew.setElement(queueList.getElement());
                    queueListNew.setDeep(queueList.getDeep());
                    queueListNew.setNextElement(clone(queueList.getNextElement()));
                    return queueListNew;
                }
                case ORDERED_LIST -> {
                    OrderList<T> orderList = (OrderList<T>) listInput;

                    OrderList<T> orderListNew = new OrderList<>();
                    orderListNew.setElement(orderList.getElement());
                    orderListNew.setDeep(orderList.getDeep());
                    orderListNew.setNextElement(clone(orderList.getNextElement()));
                    return orderListNew;
                }
                case TREE_LIST -> {
                    TreeList<T> treeList = (TreeList<T>) listInput;

                    TreeList<T> treeListNew = new TreeList<>();
                    treeListNew.setElement(treeList.getElement());
                    treeListNew.setLength(treeList.getLength());
                    treeListNew.setDeep(treeList.getDeep());
                    treeListNew.setLeftElement(clone(treeList.getLeftElement()));
                    treeListNew.setRightElement(clone(treeList.getRightElement()));
                    return treeListNew;
                }
                case BALANCED_TREE_LIST -> {
                    BalancedTreeList<T> balancedTreeList = (BalancedTreeList<T>) listInput;

                    BalancedTreeList<T> balancedTreeListNew = new BalancedTreeList<>();
                    balancedTreeListNew.setLength(balancedTreeList.getLength());
                    balancedTreeListNew.setDeep(balancedTreeList.getDeep());
                    balancedTreeListNew.setElement(balancedTreeList.getElement());
                    balancedTreeListNew.setLeftElement(clone(balancedTreeList.getLeftElement()));
                    balancedTreeListNew.setRightElement(clone(balancedTreeList.getRightElement()));
                    return balancedTreeListNew;
                }
                default -> {
                	return null;
                }
            }
        }
        return null;
    }
}
