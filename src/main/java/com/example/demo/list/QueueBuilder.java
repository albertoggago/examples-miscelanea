package com.example.demo.list;

class QueueBuilder {
    QueueBuilder(){}

    public static <T extends Comparable<T>> T getFirst(QueueList<T> queueList) {
        return QueueBuilder.get(queueList, 1L);
    }
    public static <T extends Comparable<T>> T getLast(QueueList<T> queueList) {
        return (queueList.getNextElement() == null) ? queueList.getElement() : getLast(queueList.getNextElement());
    }
    public static <T extends Comparable<T>> T get(QueueList<T> queueList, long i) {
        if (i<1) return null;
        if (i==1) {
            return queueList.getElement();
        } else if (queueList.getNextElement() != null){
            return get(queueList.getNextElement(),--i);
        }  else {
            return null;
        }
    }

    public static <T extends Comparable<T>> QueueList<T> addLast(QueueList<T> queueList, T object) {
        if (queueList.getElement() == null) {
            queueList.setElement(object);
        } else if (queueList.getNextElement() == null) {
            queueList.setNextElement(new QueueList<>(null, object));
        } else {
            addLast(queueList.getNextElement(), object);
        }
        queueList.setDeep(queueList.getDeep()+1);
        return queueList;
    }

    public static <T extends Comparable<T>> QueueList<T> addFirst(QueueList<T> queueList, T object) {
        if (queueList.getElement() != null) {
            var interQueueList = new QueueList<T>();
            interQueueList.setElement(queueList.getElement());
            interQueueList.setNextElement(queueList.getNextElement());
            interQueueList.setDeep(queueList.getDeep());

            queueList.setNextElement(interQueueList);
        }
        queueList.setElement(object);
        queueList.setDeep(queueList.getDeep() + 1);
        return queueList;
    }

    public static <T extends Comparable<T>> QueueList<T> addOrdered(QueueList<T> queueList, T t1) {
        return addLast(queueList, t1);
    }

    public static <T extends Comparable<T>> T removeLast(QueueList<T> queueList) {
        if (queueList.getNextElement() == null) {
            var oldElement = queueList.getElement();
            queueList.setElement(null);
            queueList.setDeep(0);
            return oldElement;
        } else {
            queueList.setDeep(queueList.getDeep()-1);
            return queueList.getNextElement().removeLast(queueList);
        }
    }

    public static <T extends Comparable<T>> T removeFirst(QueueList<T> queueList) {
        if (queueList.getElement() == null) {
            return null;
        } else {
            var oldElement = queueList.getElement();
            if (queueList.getNextElement() == null) {
                queueList.setElement(null);
            } else {
                queueList.setElement(queueList.getNextElement().getElement());
                queueList.setNextElement(queueList.getNextElement().getNextElement());
            }
            queueList.setDeep(queueList.getDeep()-1);
            return oldElement;
        }
    }
}