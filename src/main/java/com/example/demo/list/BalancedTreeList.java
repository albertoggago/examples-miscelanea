package com.example.demo.list;

import lombok.Getter;
import lombok.Setter;

@Getter
public class BalancedTreeList<T extends Comparable<T>> extends TreeList<T> implements ListDiff<T>, Iterable<T> {

    @Setter
    private T element;
    @Setter
    private long deep;
    @Setter
    private long length;
    private BalancedTreeList<T> leftElement;
    private BalancedTreeList<T> rightElement;

    public BalancedTreeList() {

        this.deep = 1;
        this.length = 0;
    }

    public BalancedTreeList(BalancedTreeList<T> left, BalancedTreeList<T> right, T element) {
        this.leftElement = left;
        this.rightElement = right;
        this.element = element;
        this.deep = this.calculateDeep();
        this.length= (this.getElement() ==null) ? 0 : 1
                    + TreeBuilder.lengthBranch(this.getLeftElement()) + TreeBuilder.lengthBranch(this.getRightElement());
    }

    public BalancedTreeList(T element) {
        this(null, null, element);
    }

    @Override
    public Typist getType(){
        return Typist.BALANCED_TREE_LIST;
    }

    @Override
    public TypeQuality verifyQuality() {
        var success = super.verifyQuality();
        if (TypeQuality.OK.equals(success) && this.verifyErrorBalance()) {
            success = TypeQuality.ERROR_BALANCED;
        }
        return success;
    }

    boolean verifyErrorBalance() {
        return (Math.abs(deepBranch(this.getLeftElement()) - deepBranch(this.getRightElement())) > 1) ;
    }

    @Override
    public void setRightElement(ListDiff<T> rightElement) {
        this.rightElement = (BalancedTreeList<T>) rightElement;
    }
    @Override
    public void setLeftElement(ListDiff<T> leftElement) {
        this.leftElement = (BalancedTreeList<T>) leftElement;
    }

    @Override
    public ListDiff<T> createVoidElement(T newObject) {
        return new BalancedTreeList<>(null, null, newObject);
    }

    @Override
    public boolean equals(Object o) { return super.equals(o); }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}