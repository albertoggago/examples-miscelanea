package com.example.demo.list;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class QueueIterator <T extends Comparable<T>> implements Iterator<T> {
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    private QueueList<T> queueListInternal;
    @Getter(AccessLevel.PRIVATE)
    private final List<QueueList<T>> queueListPrev;

    public QueueIterator(QueueList<T> queueList) {
        this.queueListInternal = queueList;
        this.queueListPrev = new ArrayList<>();
    }

    @Override
    public boolean hasNext() {
        return getQueueListInternal() != null && getQueueListInternal().getElement() != null;
    }

    @Override
    public T next() {
        if (hasNext()) {
            T element = getQueueListInternal().getElement();
            getQueueListPrev().addFirst(getQueueListInternal());
            setQueueListInternal(getQueueListInternal().getNextElement());
            return element;
        }
        throw new NoSuchElementException("Not element in QueueList");
    }

    public boolean hasPrev() {
        return !getQueueListPrev().isEmpty();
    }

    public T prev() {
        if (hasPrev()) {
            T element = getQueueListPrev().getFirst().getElement();
            setQueueListInternal(getQueueListPrev().getFirst());
            getQueueListPrev().removeFirst();
            return element;
        }
        throw new NoSuchElementException("Not element in prev QueueList");
    }
}
