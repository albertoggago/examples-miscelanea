package com.example.demo.javacoding;

import java.util.*;
import java.util.stream.IntStream;

public class StringExamples {

    private StringExamples(){

    }

    public static String reverseString(String s) {
        if (s == null) {
            return null;
        }
        byte[] sInBytes = s.getBytes();
        byte[] result = new byte[s.length()];
        IntStream.rangeClosed(0, s.length() -1).forEach(i -> result[i] = sInBytes[s.length() -i - 1]);
        return new String(result);
    }

    public static boolean isAnagram(String left, String right) {
        if (left == null || right == null) return false;

        int[] leftB = left.chars().sorted().toArray();
        int[] rightB = right.chars().sorted().toArray();
        if (leftB.length != rightB.length) return false;
        int i = 0;
        while (i < leftB.length && leftB[i] == rightB[i]) {
            i = i + 1;
        }
         return (i == leftB.length);
    }

    public static boolean allIsUnique(String text) {
        if (text == null) return false;

        int result = text.chars()
                .sorted()
                .reduce(Integer.MAX_VALUE, (partialResult, element) ->
                        (element == partialResult || partialResult == Integer.MIN_VALUE) ? Integer.MIN_VALUE : element);
        return result != Integer.MIN_VALUE;
    }

    public static Map<String, Integer> hasDuplicated(String text) {
        if (text == null) return Collections.emptyMap();
        Map<String, Integer> found = new HashMap<>();
        int result = text.chars().sorted()
                .reduce(Integer.MAX_VALUE, (partialResult, element) ->
                        {
                            if (partialResult != Integer.MAX_VALUE && partialResult == element) {
                                String strElement = Character.toString(element);
                                if (found.containsKey(strElement)) {
                                    found.put(strElement, found.get(strElement) + 1);
                                } else {
                                    found.put(strElement, 2);
                                }
                            }
                            return element;
                        }
                );
        if (result == Integer.MAX_VALUE) {
            return new HashMap<>();
        }
        return found;
    }

    public static String findFirstNoDuplicated(String text) {
        if (text == null) return null;

        Map<String, Integer> duplicates = StringExamples.hasDuplicated(text);
        OptionalInt result = text.chars().filter(element -> !duplicates.containsKey(Character.toString(element))).findFirst();
        return result.isPresent()? Character.toString(result.getAsInt()) : null;
    }

    public static List<String> allSubstrings(String text){
        if (text == null ) return Collections.emptyList();

        return IntStream.rangeClosed(1, text.length() - 1 )
                .mapToObj(size -> StringExamples.allSubstrings(text, size))
                .flatMap(Collection::stream).toList();
    }
    private static List<String> allSubstrings(String text, int size) {
        return IntStream.rangeClosed(0,text.length() - size ).mapToObj(i -> text.substring(i,i + size)).toList();
    }

    public static long getLength(String text) {
        if (text == null) return 0;
        return text.chars().count();
    }


    public static List<String> getPermutations(String text) {
        if (text == null ) return Collections.emptyList();
        return getPermutations(Collections.emptyList(), text.chars().toArray());
    }

    private static List<String> getPermutations(List<String> leftString, int[] toPermute) {
        if (toPermute.length == 0) return leftString;
        return IntStream.rangeClosed(0, toPermute.length -1)
                .mapToObj(i -> getPermutations(adToLeft(leftString, toPermute[i]),
                        IntStream.rangeClosed(0,toPermute.length -1)
                                .filter(i2 -> i2 != i)
                                .map(i3 -> toPermute[i3]).toArray()))
                .flatMap(Collection::stream)
                .toList();
    }

    private static List<String> adToLeft(List<String> leftString, int charNew) {
        if (leftString.isEmpty()) return Collections.singletonList(Character.toString(charNew));
        return leftString.stream().map(str -> str + Character.toString(charNew)).toList();
    }

}
