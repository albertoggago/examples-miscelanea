package com.example.demo.javacoding;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public class ArrayExamples {

    private ArrayExamples(){

    }

    public static <E extends Comparable<E>> List<E> finMaxAndMin(E[] list){
        class MaxMin <T extends Comparable<T>> {
            private T min;
            private T max;
            public MaxMin(T min, T max) {
                this.min = min;
                this.max = max;
            }
            public MaxMin() {
                this.min = null;
                this.max = null;
            }
        }
        BinaryOperator<MaxMin<E>> binaryOperator = (x1, x2) -> {
            if (x1.max == null || x2.max.compareTo(x1.max) > 0) {
                x1.max = x2.max;
            }
            if (x1.min == null || x2.min.compareTo(x1.min) < 0 ) {
                x1.min = x2.min;
            }
            return x1;
        };

        if (list == null) return Arrays.asList(null, null);
        MaxMin<E> result = Stream.of(list)
                .map(element -> new MaxMin<>(element, element))
                .reduce(new MaxMin<>(), binaryOperator);
        return Arrays.asList(result.min, result.max);

    }

    public static Integer findMissingNumber (int[] list) {
        return findMissingNumber(list, Arrays.stream(list).min().orElse(0), Arrays.stream(list).min().orElse(0) + list.length);
    }

    public static Integer findMissingNumber (int[] list, int minValue, int maxValue) {
        if (Arrays.stream(list).min().orElse(0) < minValue) {return null;}
        if (Arrays.stream(list).max().orElse(0) > maxValue) {return null;}
        int sum = Arrays.stream(list).sum();
        float sumExpected = (float) ((maxValue + minValue) * (list.length + 1) / 2.0);

        float result = sumExpected - sum;

        return (Math.round(result) == result) ? (int) result : null;
    }

}
