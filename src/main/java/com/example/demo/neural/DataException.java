package com.example.demo.neural;

import java.io.Serial;

public class DataException extends Exception{

    /**
	 * 
	 */
	@Serial
	private static final long serialVersionUID = 123456888L;

	DataException(String information) {
        super(information);
    }
}
