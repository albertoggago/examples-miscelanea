package com.example.demo.neural;

import java.security.SecureRandom;
import java.util.List;

public class NeuralNetwork {
    private double lambdaRate = 0.01;
    private final Matrix weightsIh;
    private final Matrix weightsHo;
    private final Matrix biasH;
    private final Matrix biasO;

    public NeuralNetwork(int input,int hidden,int output) throws DataException {
        this.weightsIh = new Matrix(hidden,input);
        this.weightsHo = new Matrix(output,hidden);

        this.biasH = new Matrix(hidden,1);
        this.biasO = new Matrix(output,1);
    }

    public NeuralNetwork(int input,int hidden,int output, double rate) throws DataException {
        this(input, hidden, output);
        this.lambdaRate = rate;
    }

    public List<Double> predict(double[] inputList) throws DataException {
        return Matrix.multiply(this.weightsHo,Matrix.multiply(this.weightsIh, Matrix.fromArray(inputList))
                                                    .add(this.biasH).sigmoid())
                     .add(this.biasO).sigmoid().toArray();
    }

    public void train(double [] inputTrain,double [] resultHope) throws DataException {
        var input   = Matrix.fromArray(inputTrain);
        var hidden  = Matrix.multiply(this.weightsIh, input).add(this.biasH).sigmoid();
        var output  = Matrix.multiply(this.weightsHo,hidden).add(this.biasO).sigmoid();

        var error   = Matrix.subtract(Matrix.fromArray(resultHope), output);
        var gradient= output.dSigmoid().multiply(error).multiply(lambdaRate);

        this.weightsHo.add(Matrix.multiply(gradient, Matrix.transpose(hidden)));
        this.biasO.add(gradient);

        var hGradient = hidden.dSigmoid()
                                      .multiply(Matrix.multiply(Matrix.transpose(this.weightsHo), error))
                                      .multiply(lambdaRate);

        this.weightsIh.add(Matrix.multiply(hGradient, Matrix.transpose(input)));
        this.biasH.add(hGradient);
    }

    public void fit(double[][] samplesInput, double[][] samplesOutput, int epochs, double delta) throws DataException {
        for(var i=0;i<epochs;i++)
        {
            int sampleN =  (int) ((new SecureRandom()).nextDouble() * samplesInput.length );
            this.train(samplesInput[sampleN], samplesOutput[sampleN]);
        //check predict

            var elementsOK = 0;
            for(var j = 0; j<samplesInput.length;j++) {
                List<Double> result = this.predict(samplesInput[j]);
                var deviationE = 0.0;
                for (var k = 0; k<samplesOutput[j].length; k++) {
                    deviationE+= Math.pow(samplesOutput[j][k]-result.get(k),2.0);
                    if (deviationE < delta){elementsOK++;}
                }
            }
            if (elementsOK==samplesInput.length){break;}
        }
    }
}