package com.example.demo.neural;

import lombok.Getter;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Getter
public class Matrix {

    private static final String SUBTRACT = "Subtract";
    private static final String ADD = "Add";
    private static final String MULTIPLY = "Multiply";
    private static final String MULTIPLY_NO_MATRIX = "Multiply-no-matrix";

    private final double[][] data;
    private final int rows;
    private final int cols;

    public Matrix(int rows, int cols) throws DataException {
        if (rows<1 || cols<1) throw new DataException("Error creation of Matrix rows: " +rows+", cols: "+cols);
        this.data = new double[rows][cols];
        this.rows = rows;
        this.cols = cols;
        for(var i=0;i<rows;i++){
            for(var j=0;j<cols;j++){
                Random random = new SecureRandom(); // Sensitive use of Random
                data[i][j]=random.nextDouble()*2-1;
            }
        }
    }

    public Matrix (Matrix matrix)  {
        this.rows = matrix.getRows();
        this.cols = matrix.getCols();
        this.data = new double[rows][cols];
        for(var i=0;i<rows;i++){
            System.arraycopy(matrix.getData()[i], 0, this.data[i], 0, cols);
        }
    }

    public Matrix add(double scalar) {
        for(var i=0;i<this.rows;i++){
            for(var j=0;j<this.cols;j++){
                this.data[i][j]+=scalar;
            }
        }
        return this;
    }
    public Matrix add(Matrix m) throws DataException{
        checkSameSize(m, Matrix.ADD);
        for(var i=0;i<this.rows;i++){
            for(var j=0;j<this.cols;j++){
                this.data[i][j]+=m.data[i][j];
            }
        }
        return this;
    }

    private void checkSameSize(Matrix m, String operation) throws DataException {
        if (this.cols!=m.getCols() || this.rows !=m.getRows()) {
            throw new DataException("Error Checking Size of two Matrix, operation: "+operation);
        }
    }

    public static Matrix add(Matrix m, Matrix n) throws DataException {
        m.checkSameSize(n,Matrix.ADD);
        var temp=new Matrix(m);
        temp.add(n);
        return temp;
    }

    public void subtract(Matrix m) throws DataException{
        checkSameSize(m, Matrix.SUBTRACT);
        for(var i=0;i<this.rows;i++){
            for(var j=0;j<this.cols;j++){
                this.data[i][j]-=m.data[i][j];
            }
        }
    }
    public static Matrix subtract(Matrix m, Matrix n) throws DataException {
        m.checkSameSize(n,Matrix.SUBTRACT);
        var temp=new Matrix(m);
        temp.subtract(n);
        return temp;
    }

    public void subtract(double v) {
        this.add(-v);
    }


    public static Matrix transpose(Matrix m) throws DataException {
        var temp = new Matrix(m.getCols(), m.getRows());
        for(var i=0;i<m.getRows();i++){
            for(var j=0;j<m.getCols();j++){
                temp.data[j][i] = m.getData()[i][j];
            }
        }
        return temp;
    }

    public static Matrix multiply(Matrix a, Matrix b) throws DataException {
        if (a.getCols()!= b.getRows()) { throw new DataException("Error Checking Size of two Matrix, operation: "+Matrix.MULTIPLY); }
        var temp=new Matrix(a.getRows(),b.getCols());
        for(var i=0;i<temp.getRows();i++)
        {
            for(var j=0;j<temp.getCols();j++)
            {
                double sum=0;
                for(var k=0;k<a.getCols();k++)
                {
                    sum+=a.data[i][k]*b.data[k][j];
                }
                temp.data[i][j]=sum;
            }
        }
        return temp;
    }

    public Matrix multiply(Matrix a) throws DataException {
        checkSameSize(a,Matrix.MULTIPLY_NO_MATRIX);
        for(var i=0;i<a.rows;i++)
        {
            for(var j=0;j<a.cols;j++)
            {
                this.data[i][j]*=a.data[i][j];
            }
        }
        return this;
    }

    public Matrix multiply(double a) {
        for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.cols; j++) {
                this.data[i][j] *= a;
            }
        }
        return this;
    }

    public Matrix sigmoid() {
        for(var i=0;i<this.rows;i++)
        {
            for(var j=0;j<this.cols;j++)
                this.data[i][j] = 1/(1+Math.exp(-this.data[i][j]));
        }
        return this;
    }

    public Matrix dSigmoid() throws DataException {
        var temp = new Matrix(rows, cols);
        for (var i = 0; i < rows; i++) {
            for (var j = 0; j < cols; j++)
                temp.data[i][j] = this.data[i][j] * (1 - this.data[i][j]);
        }
        return temp;
    }

    public static Matrix fromArray(double[]x) throws DataException {
        var temp = new Matrix(x.length,1);
        for(var i =0;i<x.length;i++)
            temp.data[i][0]=x[i];
        return temp;
    }

    public List<Double> toArray() {
        List<Double> temp= new ArrayList<>()  ;
        for(var i=0;i<rows;i++)
        {
            for(var j=0;j<cols;j++) { temp.add(data[i][j]); }
        }
        return temp;
    }
}
