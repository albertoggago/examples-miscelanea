package com.example.demo.dictionary;

import lombok.AccessLevel;
import lombok.Getter;

import java.util.*;
import java.util.stream.IntStream;

public class Dictionary {

    private static final String BORDER_TERMINAL = "######";
    private static final String DELIMITER = "$";

    @Getter(AccessLevel.PRIVATE)
    private final Map<String, String> dicRules = new HashMap<>();

    public Object transform(String input) {
        // get tokens
        List<String> tokens = new ArrayList<>(Collections.list(new StringTokenizer(BORDER_TERMINAL + input + BORDER_TERMINAL, DELIMITER))
                                                         .stream()
                                                         .map(String.class::cast).toList());
        IntStream.rangeClosed(1, tokens.size()-2)
                 .filter(ind->Math.floorMod(ind,2)==1)
                 .forEach(ind->tokens.set(ind, (dicRules.containsKey(tokens.get(ind))) ? dicRules.get(tokens.get(ind)) : DELIMITER + tokens.get(ind) + DELIMITER));
        return tokens.stream().reduce("", String::concat).replace(BORDER_TERMINAL,"");
    }

    public void addDict(String temp, String temporary) {
        getDicRules().put(temp, temporary);
    }
}
