package com.example.demo.heritance;

import lombok.Getter;

@Getter
public class Son extends Father {
    private final String name;

    Son(){
        this.name = "Son";
    }


}
