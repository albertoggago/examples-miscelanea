package com.example.demo.heritance;

import lombok.Getter;

@Getter
public class Father {


    private final String name;

    Father(){
        this.name = "Father";
    }

    public String withoutThis(){
        return getName();
    }
    public String withThis(){
        return this.getName();
    }

    public String directAccess(){
        return this.name;
    }
}
