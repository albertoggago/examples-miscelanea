package com.example.demo.magicbox;

import lombok.Getter;

import java.util.Arrays;
import java.util.stream.IntStream;

public class MagicBoxTransform {

    public enum TYPE_TRANSPOSE {
        VERTICAL, HORIZONTAL, TOTAL
    }

    @Getter
    private final int size;
    private final int[] horizontalMatrix;
    private final int[] verticalMatrix;

    MagicBoxTransform(int size) {
        this.size = size;
        this.horizontalMatrix = IntStream.rangeClosed(0, size*size-1)
                .map(i -> Math.floorMod(size*size-i-1,size)+Math.toIntExact(i/size)*size).toArray();
        this.verticalMatrix = IntStream.rangeClosed(0, size*size-1)
                .map(i -> Math.floorMod(i,size)+(size-Math.toIntExact(i/size)-1)*size).toArray();
    }


    MagicBox transpose(MagicBox magicBox, TYPE_TRANSPOSE type) {
        MagicBox magicBoxNew = MagicBoxBuilder.clone(magicBox);
        if (type.equals(TYPE_TRANSPOSE.HORIZONTAL) || type.equals(TYPE_TRANSPOSE.TOTAL)) {
            magicBoxNew.setMatrix(Arrays.stream(horizontalMatrix).map(i->magicBoxNew.getMatrix()[i]).toArray());
        }
        if (type.equals(TYPE_TRANSPOSE.VERTICAL) || type.equals(TYPE_TRANSPOSE.TOTAL)) {
            magicBoxNew.setMatrix(Arrays.stream(verticalMatrix).map(i->magicBoxNew.getMatrix()[i]).toArray());
        }
        return magicBoxNew;
    }

}
