package com.example.demo.magicbox;

import jakarta.annotation.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;

@Getter
public class MagicBox implements Comparable<MagicBox>{
    @Setter
    private int[] matrix;
    private final int size;
    private final int magicValue;

    public MagicBox(int size) {
        if (size > 0) {
            this.size = size;
            this.matrix = new int[size * size];
            this.magicValue = IntStream.rangeClosed(1, size*size).sum()/size;
        } else {
            throw new IllegalStateException("size wrong");
        }
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("[");
        for (int i : IntStream.rangeClosed(0,getSize()-1).toArray()) {
            result.append("[");
            for (int j : IntStream.rangeClosed(0,getSize()-1).toArray()) {
                result.append(this.getMatrix()[i*getSize()+j]);
                if (j < (getSize()-1)) result.append(",");
            }
            result.append("]");
            if (i < (getSize()-1)) result.append(",");
        }
        result.append("]");
        return result.toString();
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null ) return false;
        if (getClass() != o.getClass()) return false;
        MagicBox magicBox = (MagicBox) o;
        if (this.getMatrix() == null) return false;
        return this.getSize() == magicBox.getSize() &&
                Arrays.compare(this.getMatrix(), magicBox.getMatrix()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getSize(), Arrays.hashCode(this.getMatrix()));
    }

    @Override
    public int compareTo(@Nullable MagicBox magicBoxNew) {
        if (magicBoxNew == null) {return 0;}
        return IntStream.rangeClosed(0, size * size - 1)
                .filter(i ->matrix[i]!=magicBoxNew.getMatrix()[i])
                .map(i ->(matrix[i]<magicBoxNew.getMatrix()[i])?-1:1).findFirst().orElse(0);
    }
}
