package com.example.demo.magicbox;

import java.util.*;
import java.util.stream.IntStream;


public class MagicBoxBuilder {
    private static MagicBoxTransform magicBoxTransform;

    private MagicBoxBuilder(){}

    public static MagicBoxTransform getMagicBoxTransform(int size ) {
        if (magicBoxTransform == null || magicBoxTransform.getSize() != size) {
            magicBoxTransform = new MagicBoxTransform(size);
        }
        return magicBoxTransform;
    }

    public static void nextBox(MagicBox magicBox) {
        if (hasNext(magicBox)) {
            int size = magicBox.getSize();
            int[] matrix = magicBox.getMatrix();
            long valueMagic = magicBox.getMagicValue();

            int valuePosMagic = IntStream.rangeClosed(0, size-1)
                    .filter(i -> IntStream.rangeClosed(i*size, (i+1)*size-1).map(a->matrix[a]).sum() != valueMagic)
                    .map(i -> (i+1)*size-1).findFirst().orElse(Integer.MAX_VALUE);

            int valueRow = IntStream.rangeClosed(0, size * size - 2)
                    .boxed()
                    .sorted(Collections.reverseOrder())
                    .filter(i -> matrix[i] < matrix[i + 1])
                    .findFirst().orElse(size * size - 1);
            int valuePrev = Math.min(valueRow, valuePosMagic);
            int value = IntStream.rangeClosed(valuePrev, size*size-2)
                    .filter(val -> !Arrays.stream(Arrays.copyOfRange(matrix, val+1, matrix.length)).allMatch(a-> a< matrix[val]))
                    .findFirst().orElse(Integer.MAX_VALUE);

            int[] matrixFix = Arrays.copyOfRange(matrix, 0, value);
            int[] matrixToChange = Arrays.copyOfRange(matrix, value+1, matrix.length);
            int valueToChange = matrix[value];
            int newValue = Arrays.stream(matrixToChange).filter(a -> valueToChange <a).sorted().findFirst().orElse(0);
            int[] matrixNew = Arrays.stream(matrixToChange).map(a-> (a==newValue)? valueToChange : a).sorted().toArray();
            int[][] mergeArrays = new int[][]{matrixFix, new int[]{newValue}, matrixNew};
            int[] completedMatrix = Arrays.stream(mergeArrays).flatMapToInt(Arrays::stream).toArray();
            magicBox.setMatrix(completedMatrix);
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public static MagicBox createFirstBox(int size) {
        MagicBox magicBox = new MagicBox(size);
        magicBox.setMatrix(IntStream.rangeClosed(1, size * size).toArray());
        return magicBox;
    }

    public static boolean hasNext(MagicBox magicBox) {
        if (magicBox.getSize() > 1) {
            return IntStream.rangeClosed(0, magicBox.getSize() * magicBox.getSize() - 2)
                    .mapToObj(i -> magicBox.getMatrix()[i] < magicBox.getMatrix()[i + 1])
                    .anyMatch(a -> a);
        }
        return false;
    }

    public static boolean isMagicBox(MagicBox magicBox) {
        if (magicBox.getSize()==1) return true;
        if (!isOriginal(magicBox)) return false;
        int size = magicBox.getSize();
        int[] matrix = magicBox.getMatrix();
        long valueMagic = magicBox.getMagicValue();
        // evaluate rows
        for (int i: IntStream.rangeClosed(0, size-1).toArray()) {
            long value1 = IntStream.rangeClosed(i*size, (i+1)*size-1)
                    .map(a->matrix[a]).reduce(0, Integer::sum);
            if (value1 != valueMagic) return false;
        }
        for (int i: IntStream.rangeClosed(0, size-1).toArray()) {
            long value2 = IntStream.rangeClosed(0, size-1)
                    .map(a-> a*size+i)
                    .map(a->matrix[a]).reduce(0, Integer::sum);
            if (value2 != valueMagic) return false;
        }
        long value3 = IntStream.rangeClosed(0, size-1).map(a->a*(size+1)).map(a->matrix[a]).reduce(0, Integer::sum);
        if (value3 != valueMagic) return false;
        long value4 = IntStream.rangeClosed(0, size-1).map(a->(a+1)*(size-1)).map(a->matrix[a]).reduce(0, Integer::sum);
        return value4 == valueMagic;
    }

    public static List<MagicBox> printAllMagicBoxed(int size){
        return printAllMagicBoxed(size,Integer.MAX_VALUE, false);
    }

    public static List<MagicBox> printAllMagicBoxed(int size, int limit, boolean withClones) {
        List<MagicBox> result = new ArrayList<>();
        MagicBox magicBox = createFirstBox(size);
        while(hasNext(magicBox) && result.size() < limit) {
            if(isMagicBox(magicBox)) {
                result.add(clone(magicBox));
                if (withClones) {
                    result.add(transpose(clone(magicBox), MagicBoxTransform.TYPE_TRANSPOSE.HORIZONTAL));
                    result.add(transpose(clone(magicBox), MagicBoxTransform.TYPE_TRANSPOSE.VERTICAL));
                    result.add(transpose(clone(magicBox), MagicBoxTransform.TYPE_TRANSPOSE.TOTAL));
                }
            }
            nextBox(magicBox);
        }
        return result;
    }

    public static MagicBox clone(MagicBox magicBox) {
        MagicBox magicBoxNew = createFirstBox(magicBox.getSize());
        magicBoxNew.setMatrix(magicBox.getMatrix().clone());
        return magicBoxNew;
    }

    public static boolean isOriginal(MagicBox magicBox) {
        return magicBox.compareTo(transpose(magicBox, MagicBoxTransform.TYPE_TRANSPOSE.HORIZONTAL)) < 0 &&
               magicBox.compareTo(transpose(magicBox, MagicBoxTransform.TYPE_TRANSPOSE.VERTICAL)) < 0 &&
                magicBox.compareTo(transpose(magicBox, MagicBoxTransform.TYPE_TRANSPOSE.TOTAL)) < 0;

    }

    public static MagicBox transpose(MagicBox magicBox, MagicBoxTransform.TYPE_TRANSPOSE type) {
        return getMagicBoxTransform(magicBox.getSize()).transpose(magicBox,type);
    }
}
