package com.example.demo.cqrs;

import lombok.*;

import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
public class BookWrite extends Book{
    private boolean messageSent;

    public BookWrite(String clientID, String roomName, LocalDate arrivalDate, LocalDate departureDate) {
        super(clientID, roomName, arrivalDate, departureDate);
        messageSent = false;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null ) return false;
        if (getClass() != o.getClass()) return false;
        BookWrite bookWrite = (BookWrite) o;
        return  this.getClientID().equals(bookWrite.getClientID()) &&
                this.getRoomName().equals(bookWrite.getRoomName()) &&
                this.getArrivalDate().equals(bookWrite.getArrivalDate()) &&
                this.getDepartureDate().equals(bookWrite.getDepartureDate()) &&
                this.isMessageSent() == bookWrite.isMessageSent();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getClientID(), this.getRoomName(), this.getArrivalDate(), this.getDepartureDate(), this.isMessageSent());
    }

    @Override
    public String toString(){
        String prev =super.toString();
        return prev.substring(0, prev.length()-1)+", messageSent: "+ this.isMessageSent()+")";
    }
}
