package com.example.demo.cqrs;

public class BookProjector {
    RoomReadRepository roomReadRepository;

    BookProjector(RoomReadRepository roomReadRepository) {
        this.roomReadRepository = roomReadRepository;
    }

    public boolean projector(BookWrite bookWrite) {
        return roomReadRepository.addBook(bookWrite);
    }

    public boolean projector(RoomWrite roomWrite) {
        return roomReadRepository.addRoom(roomWrite);
    }

}
