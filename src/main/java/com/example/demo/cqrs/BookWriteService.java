package com.example.demo.cqrs;

import java.time.LocalDate;

public class BookWriteService {
    BookWriteRepository bookWriteRepository;

    BookWriteService(BookWriteRepository bookWriteRepository) {
        this.bookWriteRepository = bookWriteRepository;
    }

    public void addBook(String clientID, String roomName, LocalDate arrivalDate, LocalDate departureDate ) {
        addBook(new Book(clientID, roomName, arrivalDate, departureDate));
    }

    public void addBook(Book book) {
        bookWriteRepository.addBook(BookManager.transformWrite(book));
    }

    public void addRoom(String name) {
        addRoom(new Room(name));
    }

    public void addRoom(Room room) {
        bookWriteRepository.addRoom(BookManager.transformWrite(room));
    }

    public void runAsync(BookProjector projector) {
        bookWriteRepository.getStoreBook().values().stream()
                .filter(bookWrite -> !bookWrite.isMessageSent())
                .forEach(bookWrite -> {bookWrite.setMessageSent(true);projector.projector(bookWrite);});
        bookWriteRepository.getStoreRoom().values().stream()
                .filter(roomWrite -> !roomWrite.isMessageSent())
                .forEach(roomWrite -> {roomWrite.setMessageSent(true);projector.projector(roomWrite);});
    }

}
