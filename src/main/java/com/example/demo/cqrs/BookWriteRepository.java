package com.example.demo.cqrs;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class BookWriteRepository {
    private final Map<String, BookWrite> storeBook = new HashMap<>();
    private final Map<String, RoomWrite> storeRoom = new HashMap<>();

    public void addBook(BookWrite book){
           storeBook.putIfAbsent(book.getClientID(), book);
    }

    public void addRoom(RoomWrite room){
        storeRoom.putIfAbsent(room.getName(), room);
    }

}
