package com.example.demo.cqrs;

import java.time.LocalDate;
import java.util.List;

public class RoomReadService {
    RoomReadRepository roomReadRepository;

    RoomReadService(RoomReadRepository roomReadRepository) {
        this.roomReadRepository = roomReadRepository;
    }


    public List<Room> freeRooms(LocalDate arriveDate, LocalDate departureDate) {
        return roomReadRepository.freeRooms(arriveDate, departureDate);
    }

}
