package com.example.demo.cqrs;

import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Data
@Getter
public class RoomRead {
    private final String name;
    private final List<Book> books = new ArrayList<>();

    RoomRead(String name) {
        this.name = name;
    }

    public void addBooks(Book book) {
        books.add(book);
    }
}
