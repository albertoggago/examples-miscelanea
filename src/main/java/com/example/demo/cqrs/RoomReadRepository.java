package com.example.demo.cqrs;

import lombok.Getter;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class RoomReadRepository {
    private final Map<String, RoomRead> store = new HashMap<>();

    public boolean addRoom(Room room) {
        if(room.getName() == null) {
            return false;
        }
        if(!store.containsKey(room.getName())) {
            store.put(room.getName(), new RoomRead(room.getName()));
            return true;
        }
        return false;
    }

    public boolean addBook(Book bookNew) {
        if (bookNew.getClientID() == null || bookNew.getRoomName() == null ||
            bookNew.getArrivalDate() == null ||  bookNew.getDepartureDate() == null) {
            return false;
        }
        if (!bookNew.getArrivalDate().isBefore(bookNew.getDepartureDate())) { // !a<d
            return false;
        }
        if(store.containsKey(bookNew.getRoomName())) {
                boolean canStore = checkCanStore(bookNew.getArrivalDate()
                                                 , bookNew.getDepartureDate()
                                                 , store.get(bookNew.getRoomName()).getBooks());
                if (canStore) {
                    store.get(bookNew.getRoomName()).getBooks().add(bookNew);
                    return true;
                }
            }
        return false;
    }

    private boolean checkCanStore(LocalDate arrivalDate, LocalDate departureDate, List<Book> books) {
        boolean cannotStore = books.stream()
                .filter(bookStored -> bookStored.getDepartureDate().isAfter(arrivalDate)) // !dx<=a dx>a
                .anyMatch(bookStored -> departureDate.isAfter(bookStored.getArrivalDate())); // !d<=ax d>ax
        return !cannotStore;
    }

    public List<Room> freeRooms(LocalDate arriveDate, LocalDate departureDate) {
         return store.entrySet().stream()
                .filter(entryRoom -> this.checkCanStore(arriveDate, departureDate, entryRoom.getValue().getBooks()))
                .map(Map.Entry::getKey).map(Room::new).toList();
    }
}