package com.example.demo.cqrs;

import lombok.Data;
import lombok.Getter;

import java.time.LocalDate;

@Data
@Getter
public class Book {
    private String clientID;
    private String roomName;
    private LocalDate arrivalDate;
    private LocalDate departureDate;

    public Book(String clientID, String roomName, LocalDate arrivalDate, LocalDate departureDate) {
        this.clientID = clientID;
        this.roomName = roomName;
        this.arrivalDate = arrivalDate;
        this.departureDate = departureDate;
    }
}
