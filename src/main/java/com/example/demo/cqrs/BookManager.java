package com.example.demo.cqrs;

public class BookManager {

    private BookManager() {
    }

    public static RoomWrite transformWrite(Room room) {
        return new RoomWrite(room.getName());
    }

    public static BookWrite transformWrite(Book book) {
        return new BookWrite(book.getClientID(), book.getRoomName(), book.getArrivalDate(), book.getDepartureDate());
    }
}
