package com.example.demo.cqrs;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class RoomWrite extends Room {
    private boolean messageSent = false;

    public RoomWrite(String name) {
        super(name);
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null ) return false;
        if (getClass() != o.getClass()) return false;
        RoomWrite roomWrite = (RoomWrite) o;
        return  this.getName().equals(roomWrite.getName()) &&
                this.isMessageSent() == roomWrite.isMessageSent();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.isMessageSent());
    }

    @Override
    public String toString(){
        String prev = super.toString();
        return prev.substring(0, prev.length()-1)+", messageSent: "+ this.isMessageSent() + ")";
    }
}
