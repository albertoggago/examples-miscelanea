package com.example.demo.cqrs;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;


@Data
@Getter
public class Room {

    private final String name;

    @Builder
    public Room(String name) {
        this.name = name;
    }
}
