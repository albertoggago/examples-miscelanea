package com.example.demo.foo_bar_qix;

public class FooBarQix {
    private static final String FOO = "Foo";
    private static final String BAR = "Bar";
    private static final String QIX = "Qix";
    private static final String ZERO = "*";


    private FooBarQix() {
        throw new IllegalStateException("Utility class");
    }

    public static String compute(int value){
        var initString = (Integer.valueOf(value)).toString();
        var resultMod = "";
        if (Math.floorMod(value,3)==0) {resultMod=resultMod+ FOO;}
        if (Math.floorMod(value,5)==0) {resultMod=resultMod+ BAR;}
        if (Math.floorMod(value,7)==0) {resultMod=resultMod+ QIX;}
        String result = initString.chars().mapToObj(Character::toString)
                .map(FooBarQix::changeRuleFooBarQix)
                .reduce(resultMod,(partialString, element)-> partialString+element);

        return eqBlankOrZeroes(result)? streamChangeZeroes(initString):result;
    }

    private static String streamChangeZeroes(String initString) {
        return initString.chars().mapToObj(Character::toString).map(FooBarQix::changeZeroes).reduce("", (partialString, element)-> partialString+element);
    }

    private static boolean eqBlankOrZeroes(String result) {
        return "".equals(result)||result.chars().allMatch(c->c=='*');
    }

    private static String changeRuleFooBarQix(String s) {
        return switch (s) {
            case "3":
                yield FOO;
            case "5":
                yield  BAR;
            case "7":
                yield  QIX;
            case "0":
                yield  ZERO;
            default:
                yield  "";
        };
    }

    private static String changeZeroes(String s) {
        if (s.equals("0")) {return ZERO;}
        return s;
    }

}
